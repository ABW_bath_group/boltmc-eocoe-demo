from setuptools import setup, find_packages

long_description = (
    """BoltMC is an implementation of a drift-scatter model implemented using PPMD."""
)

install_requires = []
with open("requirements.txt") as fh:
    for l in fh:
        if len(l) > 0:
            install_requires.append(l)

setup(
    name="boltmc",
    version="1.0",
    description="",
    license="",
    long_description=long_description,
    author="Lewis Irvine",
    author_email="",
    url="",
    packages=find_packages(),
    install_requires=install_requires,
    scripts=["scripts/boltmc"],
    include_package_data=True,
)
