import numpy as np
import matplotlib.pyplot as plt
import lmfit
from boltmc.utilities.boltmc_constants import *


class FitMaxwellBoltzmann:
    def __init__(
        self,
        a,
        fit_func,
        fit_mode,
        min_energy=None,
        max_energy=None,
        top_percent=None,
        colour="blue",
        allow_scaling=False,
        label=None,
        fitting_method="leastsq",
    ):
        """Fit a Maxwell-Boltzmann distribution to the BoltMC energy distributions.

        :param a: A DistributionAnimator class instance
        :type a: DistributionAnimator
        :param fit_mode: The fitting mode to use. If "energy", fits within an energy range defined by ``min_energy`` and ``max_energy``. If "top_percent", fits to the top proportion of particles, with the proportion given by ``top_percent``.
        :type fit_mode: str
        :param min_energy: The minimum energy (in J) to fit to. Used if ``fit_mode = energy``. If None, fits above 0. Default is None.
        :type min_energy: float, optional
        :param max_energy: The maximum energy (in J) to fit to. Used if ``fit_mode = energy``. If None, to maximum of distribution. Default is None.
        :type max_energy: float, optional
        :param top_percent: The percentage to use when fitting to the highest energy particles. Should be between 0 and 1. For example, ``top_percent = 0.2`` will fit to the top 20% of particles. Defaults to None.
        :type top_percent: float, optional
        :param colour: The colour to plot with, defaults to "blue"
        :type colour: str, optional
        :param allow_scaling: Whether or not to allow an arbitrary scaling constant to improve fitting at the cost of neglecting normalisation, defaults to False
        :type allow_scaling: bool, optional
        :param label: The label to use in axis legends. If None, a method is called to try and automatically generate one based on the vcalue of ``fit_mode``. Defaults to None.
        :type label: str, optional
        """
        self.a = a
        self.fit_mode = fit_mode
        self.min_energy = min_energy
        self.max_energy = max_energy
        self.colour = colour
        self.allow_scaling = allow_scaling
        self.top_percent = top_percent
        self.label = label
        self.fitting_method = fitting_method

        if fit_func == "maxwell_boltzmann":
            self.fitting_func = self.maxwell_boltzmann
        elif fit_func == "exponential":
            self.fitting_func = self.exponential
        else:
            raise RuntimeError(
                "fit_func must be maxwell_boltzmann or exponential, I was given {}".format(
                    fit_func
                )
            )

        if not self.label:
            self.make_label()
        if self.fit_mode == "energy":
            self.selection_ids = (self.a.energies_j > min_energy) & (
                self.a.energies_j < max_energy
            )

    def maxwell_boltzmann(self, ens, params):
        """
        Generates a Maxwell-Boltzmann distribution, given a numpy array of energies (in J) and a temperature (in K).
        """
        kt = k_B * params["temperature"]
        scale = params["scale"]
        return scale * 2 * np.sqrt(ens / pi) * kt ** -1.5 * np.exp(-ens / kt)

    def exponential(self, ens, params):
        kt = k_B * params["temperature"]
        scale = params["scale"]
        return scale * np.exp(-ens / kt)

    def residual(self, params, x, data, model_func):
        """
        The function to be minimised. Takes the function model_func as a function of x and returns the difference between it and the data. params should completely describe the function.
        """
        model = model_func(x, params)
        return abs(model - data)

    def extract_temperature(self, energies, distribution, model_func, allow_scaling):
        """Fit a temperature to an energy distribution function.

        :param energies: The energy series to fit (in Joules)
        :type energies: ndarray
        :param distribution: The (normalised) distribution as a function of energy. Should be the same length as energies.
        :type distribution: ndarray
        :param model_func: The fitting function that should be used. Should take a temperature and scale from a params object and return an array of the same length as energies.
        :type model_func: function
        :param allow_scaling: Whether or not to allow an arbitrary scaling constant to improve fitting at the cost of neglecting normalisation
        :type allow_scaling: bool
        :return: an lmfit Parameters object containing the fitting parameters. Temperature can be extracted from ["temperature"].value element, scaling factor from "scale"].value.
        :rtype: lmift Parameters object
        """
        params = lmfit.Parameters()
        params.add("temperature", value=1000, min=1, max=4000)
        params.add("scale", value=1, min=0, max=1e30, vary=allow_scaling)

        mini = lmfit.Minimizer(
            self.residual, params, fcn_args=(energies, distribution, model_func)
        )
        result = mini.minimize(method=self.fitting_method)
        return result.params

    def percent_fit_energies(self, full_energies, distribution):
        """Find the energy and distribution range that corresponds to a proportion of particles, set by self.top_percent.

        :param full_energies: The full energy range of the distribution.
        :type full_energies: ndarray
        :param distribution: The full (normalised) distribution.
        :type distribution: ndarray
        :return:
        - The energy range corresponding to the top proportion of particles.
        - The distribution of the top proportion of particles.
        :rtype: ndarray, ndarray
        """
        cumsum_reverse = np.cumsum(distribution[::-1])[::-1]
        max_cumsum = cumsum_reverse[0]
        min_idx = (np.abs(cumsum_reverse - self.top_percent * max_cumsum)).argmin()
        fit_ens = full_energies[min_idx:]
        fit_dist = distribution[min_idx:]
        return fit_ens, fit_dist

    def initialise(self):
        """For every timestep, fit a Maxwell-Boltzmann distribution function to the current timestep's distribution. The fitting procedure is chosen according to the value of self.fit_mode.

        Also saves the fitted temperatures.
        """
        self.temperatures = np.zeros(self.a.n_times)
        self.distribution = np.zeros((self.a.n_times, len(self.a.energies_j)))
        self.fit_distributions = []
        self.fit_energies = []
        self.results = []
        for it in range(self.a.n_times):
            if self.fit_mode == "energy":
                self.fit_energies.append(self.a.energies_j[self.selection_ids])
                self.fit_distributions.append(
                    self.a.distribution[it, self.selection_ids]
                )
            if self.fit_mode == "top_percent":
                self.fit_ens, self.fit_dists = self.percent_fit_energies(
                    self.a.energies_j, self.a.distribution[it]
                )
                self.fit_energies.append(self.fit_ens)
                self.fit_distributions.append(self.fit_dists)

            self.results.append(
                self.extract_temperature(
                    self.fit_energies[it],
                    self.fit_distributions[it],
                    self.fitting_func,
                    self.allow_scaling,
                )
            )
            self.temperatures[it] = self.results[it]["temperature"].value
            self.distribution[it] = self.fitting_func(
                self.a.energies_j, self.results[it]
            )

    def execute(self):
        """Plot the fitted temperature, expressed as an effective mean energy, as a vertical line on the main animation. Also fill under the distribution to show the fitting range.

        The 'effective mean energy' is found simply by using E = 3/2 * k_B * T, where k_B is the Boltzmann constant and T is the fitted temperature.
        """
        eff_mean_ke = 3 / 2 * k_B * self.temperatures[self.a.frame_idx]
        self.a.ax.plot(
            self.a.energies_mev,
            self.distribution[self.a.frame_idx],
            color=self.colour,
        )
        self.a.ax.fill_between(
            self.fit_energies[self.a.frame_idx] * 1e3 / q,
            self.fit_distributions[self.a.frame_idx],
            color=self.colour,
            alpha=0.5,
        )
        self.a.ax.axvline(eff_mean_ke * 1e3 / q, color=self.colour)

    def make_label(self):
        """Make the label for plots, depending on the fitting mode."""
        if self.fit_mode == "energy":
            self.label = "between {} - {} meV".format(
                self.min_energy * 1e3 / q, self.max_energy * 1e3 / q
            )
        elif self.fit_mode == "top_percent":
            self.label = "top {}%".format(self.top_percent * 100)


def hellinger_distance_solo(p, q):
    assert np.shape(p) == np.shape(q), "shape of p {} and q {} do not match".format(
        np.shape(p), np.shape(q)
    )
    assert not np.isnan(p).any()
    assert not np.isnan(q).any()
    p_norm = p / np.sum(p)
    q_norm = q / np.sum(q)
    hellinger = (
        1 / np.sqrt(2) * np.sqrt(np.sum(np.square(np.sqrt(p_norm) - np.sqrt(q_norm))))
    )
    assert not np.isnan(hellinger).any()
    return hellinger
