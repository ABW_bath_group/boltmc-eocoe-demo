import os
import numpy as np
from scipy.signal import savgol_filter
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from boltmc.utilities.boltmc_constants import *
from plotting.boltmc_plotter import BoltMCPlotter
from plotting.distributions.distribution_extensions import hellinger_distance_solo

mpl.use("Agg")

default_inputs = {
    "workfolder": os.getcwd(),
    "distributionfile": "energydists.npz",
    "simulationfile": "simulation.json",
    "meanenergyfile": "meanenergy.json",
    "savefile": "distribution.gif",
    "verbose": False,
    "steptime": None,
    "maxtime": None,
    "distribution_extensions": [],
    "make_animation": True,
    "plot_temperatures": True,
    "plot_hellinger": False,
    "plot_distribution": True,
    "plot_smooth_distribution": False,
    "plot_thermal": True,
    "plot_mean_energy": True,
    "show_time": True,
    "dpi": 200,
    "animation_interval": 100,
    "gif_writer": "imagemagick",
    "xmin": None,
    "xmax": None,
    "ymin": None,
    "ymax": None,
    "xlabel": "Energy (meV)",
    "ylabel": "Population",
    "distribution_colour": "black",
    "mean_energy_colour": "black",
    "thermal_colour": "black",
    "smooth_distribution_colour": "black",
    "smooth_polyorder": 3,
    "smooth_window_length": 21,
}


class DistributionAnimator:
    def __init__(self, inputs=None, external_animator=None):
        self.external_animator = external_animator
        self.parse_inputs(inputs)
        self.setup_animator()

    def parse_inputs(self, inputs):
        """Take the input dict and save all entries as attributes of the DistributionAnimator class.

        :param inputs: A dict containing everything needed for the animator.
        :type inputs: dict
        """
        all_inputs = default_inputs.copy()
        if inputs:
            all_inputs.update(inputs)
        for key, value in all_inputs.items():
            if all_inputs["verbose"]:
                print("{}: {}".format(key, value))
            setattr(self, key, value)
        if self.verbose:
            print("Inputs have been parsed.")

    def setup_animator(self):
        """Set up the figure, time series, energies and extensions that the animator needs."""
        # axes that distribution will be plotted on
        if self.external_animator:
            self.fig = self.external_animator.fig
            self.ax = self.external_animator.ax
        else:
            self.fig, self.ax = plt.subplots()

        for fstr in ["distributionfile", "simulationfile", "meanenergyfile"]:
            setattr(self, fstr, os.path.join(self.workfolder, getattr(self, fstr)))

        # import simulation data
        self.plotfiles = [self.distributionfile, self.simulationfile]
        if self.plot_mean_energy:
            self.plotfiles.append(self.meanenergyfile)
        self.p = BoltMCPlotter(self.plotfiles, verbose=self.verbose)

        # Sort out time series
        if not self.maxtime:
            # default: set self.maxtime to length of simulation
            self.maxtime = self.p.savetimes[-1]
        if not self.steptime:
            self.steptime = self.p.savetimes[1] - self.p.savetimes[0]
        self.max_time_idx = (np.abs(self.p.savetimes - self.maxtime)).argmin()
        self.plot_times = np.arange(0, self.maxtime, self.steptime)
        self.n_times = len(self.plot_times)
        self.plot_idxs = np.array(
            [(np.abs(self.p.savetimes - t)).argmin() for t in self.plot_times]
        )

        # Sort out energies
        if hasattr(self.p, "distribution_energy_step"):
            self.de = self.p.distribution_energy_step * q
        else:
            self.de = self.p.rate_table_energy_step * q
        self.energies_j = self.p.distribution_energies[:-1]
        self.energies_mev = self.energies_j * 1e3 / q
        if self.plot_mean_energy:
            if len(np.shape(self.p.distribution)) == 3:  # more than one species
                self.meanenergies_j = self.p.mean_energy_per_index[
                    self.plot_idxs, 0, self.ptype, 0
                ]
            else:
                self.meanenergies_j = self.p.mean_energy[self.plot_idxs]
            self.meanenergies_mev = self.meanenergies_j * 1e3 / q
            self.meanenergy_temperatures = self.meanenergies_j * 2 / 3 / k_B

        # Sort out distribution function
        print(len(np.shape(self.p.distribution)))
        # import pdb; pdb.set_trace()
        if len(np.shape(self.p.distribution)) == 2:  # just one species
            self.distribution = (
                self.p.distribution[self.plot_idxs] / self.de / self.p.n_particles
            )
        if len(np.shape(self.p.distribution)) == 3:  # more than one species
            distribution_ptx = self.p.distribution[self.plot_idxs, self.ptype, :]
            n_particles_ptx = np.sum(distribution_ptx, axis=-1)
            self.distribution = distribution_ptx / self.de / n_particles_ptx[:, None]

        self.make_smooth_distributions()

        self.calculate_mean_energy_mb()

        # Initialise extensions list (empty for now, can be added to via self.add_extenstions()).
        self.extensions = []

        if self.verbose:
            print("Setup completed.")

    def run(self):
        # Initialise any function handles
        self._initialise_extensions()
        # Calculate Hellinger distances
        if self.plot_hellinger:
            self.calculate_hellinger_distances()
        # Animate
        if self.make_animation:
            self.animate()
        # Plot temperature v time
        if self.plot_temperatures:
            self.plot_temperature_v_time()
        if self.plot_hellinger:
            self.plot_hellinger_v_time()
        # Finalise any function handles
        self._finalise_extensions()

    def init(self):
        """Dummy function that gets called at the start of the animation process."""
        self.ax.plot()

    def animation_loop(self, frame_idx):
        """The core loop that gets called by self.animate().

        Plots the relevant energy distribution plus anything else defined by inputs, and executes any extensions that have an execute() method.

        :param frame_idx: The current time index to plot
        :type frame_idx: int
        """
        self.frame_idx = frame_idx
        if self.verbose:
            print(
                "Animating {} / {}".format(self.frame_idx, self.maxtime / self.steptime)
            )

        self.ax.clear()
        self.axtext = ""

        self.add_plots(self.frame_idx)

        if self.show_time:
            self.add_time_text()
        if self.plot_mean_energy:
            self.add_mean_energy_text()
        self.add_text()

        self.config_axes()

    def add_plots(self, frame_idx):
        """Execute all the various plotting functions."""
        self.frame_idx = frame_idx
        self.current_time = self.p.savetimes[self.plot_idxs[self.frame_idx]]

        if self.plot_distribution:
            self.add_plot_distribution()
        if self.plot_smooth_distribution:
            self.add_plot_smooth_distribution()
        if self.plot_mean_energy:
            self.add_plot_mean_energy()
        if self.plot_thermal:
            self.add_plot_thermal()
        # Execute any function handles
        self._execute_extensions()

    def animate(self):
        """The main function that handles the execution, writing and saving of the animation.

        Also executes any extensions with an initialise() or finalise() method.
        """

        if self.verbose:
            print("Animating...")

        ani = FuncAnimation(
            self.fig,
            self.animation_loop,
            init_func=self.init,
            frames=self.n_times,
            interval=self.animation_interval,
        )
        ani.save(self.savefile, writer=self.gif_writer, dpi=self.dpi)
        print("Animation saved as {}".format(self.savefile))

    def plot_temperature_v_time(self):
        if self.external_animator:
            self.temperature_fig = self.external_animator.temperature_fig
            self.temperature_ax = self.external_animator.temperature_ax
        else:
            self.temperature_fig, self.temperature_ax = plt.subplots()
        for ext in self.extensions:
            if hasattr(ext, "temperatures"):
                label = ext.label if hasattr(ext, "label") else None
                self.temperature_ax.plot(
                    self.plot_times / 1e-12,
                    ext.temperatures,
                    color=ext.colour,
                    label=label,
                )
        # if not self.external_animator:
        self.temperature_ax.set_ylabel("Temperature (K)")
        self.temperature_ax.set_xlabel("Time (ps)")
        self.temperature_ax.legend()
        self.temperature_fig.savefig("fit_temps.png", dpi=1000)

    def plot_hellinger_v_time(self):
        """Plot the Hellinger statistical distance for all extensions that have a 'distribution' attribute. The Hellinger distances are calculated beforehand using the calculate_hellinger_distance() method."""
        if self.external_animator:
            self.hellinger_fig = self.external_animator.hellinger_fig
            self.hellinger_ax = self.external_animator.hellinger_ax
        else:
            self.hellinger_fig, self.hellinger_ax = plt.subplots()
        for ext in self.extensions:
            if hasattr(ext, "hellinger"):
                label = ext.label if hasattr(ext, "label") else None
                self.hellinger_ax.plot(
                    self.plot_times / 1e-12,
                    ext.hellinger,
                    color=ext.colour,
                    label=label,
                )
        if not self.external_animator:
            self.hellinger_ax.set_ylabel("Hellinger distance")
            self.hellinger_ax.set_xlabel("Time (ps)")
            self.hellinger_ax.legend()
            self.hellinger_fig.savefig("hellinger.png", dpi=1000)

    def calculate_hellinger_distances(self):
        """Calculate the Hellinger statistical data between an extension's fitted distribution and the raw data from the simulation. Only calls if the extension has an attribute called 'distribution'."""
        for ext in self.extensions:
            if hasattr(ext, "distribution"):
                ext.hellinger = np.zeros(self.n_times)
                for it in range(self.n_times):
                    ext.hellinger[it] = hellinger_distance_solo(
                        self.distribution[it], ext.distribution[it]
                    )

    def add_plot_distribution(self):
        """Plot the energetic distribution of the ensemble."""
        self.ax.plot(
            self.energies_mev,
            self.distribution[self.frame_idx],
            color=self.distribution_colour,
        )

    def add_plot_smooth_distribution(self):
        """Plot the smoothed energetic distribution of the ensemble."""
        self.ax.plot(
            self.energies_mev,
            self.smooth_distributions[self.frame_idx],
            color=self.smooth_distribution_colour,
        )

    def add_plot_mean_energy(self):
        """Plot the mean energy as a big vertical line."""
        self.ax.axvline(
            self.meanenergies_mev[self.frame_idx], color=self.mean_energy_colour
        )

    def calculate_mean_energy_mb(self):
        self.mean_energy_mbs = np.zeros((len(self.plot_times), len(self.energies_j)))
        mb_ens = self.energies_j + 0.5 * self.de
        for it in range(len(self.plot_times)):
            self.mean_energy_mbs[it, :] = self.p.maxwell_boltzmann_en(
                mb_ens, self.meanenergy_temperatures[it]
            )

    def add_plot_thermal(self):
        """Plot a Maxwell-Boltzmann distribution, using the mean energy to calculate an effective temperature."""
        self.ax.plot(
            self.energies_mev,
            self.mean_energy_mbs[self.frame_idx],
            color=self.thermal_colour,
        )

    def add_plot_extensions(self):
        """Go through all the extensions and plot their distributions."""
        return

    def add_time_text(self):
        """Add some text telling the current time (in ps)."""
        self.axtext = self.axtext + "{:2f} ps".format(
            self.plot_times[self.frame_idx] / 1e-12
        )

    def add_mean_energy_text(self):
        """Add some text showing the current mean energy (in meV)."""
        self.axtext = self.axtext + ", {:2f} meV".format(
            self.meanenergies_mev[self.frame_idx]
        )

    def add_text(self):
        """Show any text in bottom left of plot."""
        self.ax.text(0, 0, self.axtext)

    def config_axes(self):
        """Update any axis properties according to inputs.

        Note: can be overrided / extneded with other properties by writing an extension.
        """
        self.ax.set_xlim(left=self.xmin, right=self.xmax)
        self.ax.set_xlabel(self.xlabel)
        self.ax.set_ylim(bottom=self.ymin, top=self.ymax)
        self.ax.set_ylabel(self.ylabel)

    def make_smooth_distributions(self):
        """[summary]"""
        self.smooth_distributions = self.smooth(self.distribution)

    def smooth(self, y):
        """Smooth a function using the scipy Savitzky-Golay filter.

        :param y: The data to be smoothed.
        :type y: array_like
        :return: The smoothed data
        :rtype: array_like
        """
        return np.abs(
            savgol_filter(
                y, self.smooth_window_length, self.smooth_polyorder, mode="constant"
            )
        )

    def add_extensions(self, extensions):
        """Add extension class instances to the list of extensions to be triggered before, during and after the animation.

        An extension class instance should contain one or more of the following methods:
        - initialise: to be executed before the animation begins
        - execute: to be executed during each animation step
        - finalise: to be executed after the animation finishes

        :param extensions: A list of extension class instances.
        :type extensions: list
        """
        for ext in extensions:
            self.extensions.append(ext)

    def _initialise_extensions(self):
        """
        Run initialise() for all functions defined in the list self.extensions that have that method. Gets called just before animation loop starts.
        """
        for ext in self.extensions:
            if callable(getattr(ext, "initialise", None)):
                ext.initialise()

    def _execute_extensions(self):
        """
        Run execute() for all functions defined in the list self.extensions that have that method. Gets called every timestep in main loop.
        """
        for ext in self.extensions:
            if callable(getattr(ext, "execute", None)):
                ext.execute()

    def _finalise_extensions(self):
        """
        Run finalise() for all functions defined in the list self.extensions that have that method. Gets called just after animation loop ends.
        """
        for ext in self.extensions:
            if callable(getattr(ext, "finalise", None)):
                ext.finalise()

    def initialise(self):
        if self.external_animator:
            # Initialise any function handles
            self._initialise_extensions()
            # Calculate Hellinger distances
            if self.plot_hellinger:
                self.calculate_hellinger_distances()

    def execute(self):
        """Useful for running a DistributionAnimator instance as an extension to another DistributionAnimator instance."""
        if self.external_animator:
            self.add_plots(self.external_animator.frame_idx)
        else:
            raise RuntimeError(
                "Running as an extension, but external_animator not specified."
            )

    def finalise(self):
        if self.external_animator:
            # Plot temperature v time
            if self.plot_temperatures:
                self.plot_temperature_v_time()
            if self.plot_hellinger:
                self.plot_hellinger_v_time()
            self._finalise_extensions()


if __name__ == "__main__":
    a = DistributionAnimator()
    a.animate()