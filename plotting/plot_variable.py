import numpy as np
import matplotlib as mpl

# mpl.use('Agg')
import matplotlib.pyplot as plt
import argparse

from plotting.boltmc_plotter import BoltMCPlotter


def plot_variable_vs_time(
    variable, varfile, simfile, timefile, figfile, fig=None, ax=None, savefig=True
):
    filenames = [simfile, varfile, timefile]
    p = BoltMCPlotter(filenames, verbose=False)

    if fig is None and ax is None:
        fig, ax = plt.subplots()

    plot_var = getattr(p, variable)
    ax.plot(p.savetimes, plot_var)

    plt.savefig(figfile)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot something as a function of time")
    parser.add_argument(
        "-f", "--vfile", type=str, help="File containing variable data to be plotted"
    )
    parser.add_argument("-v", "--var", type=str, help="Name of variable to be plotted")
    parser.add_argument(
        "-p", "--plotfile", type=str, help="Name of plot file (include suffix)"
    )
    args = parser.parse_args()

    plot_variable_vs_time(
        args.var, args.vfile, "saved_inputs.yaml", "savetimes.json", args.plotfile
    )
