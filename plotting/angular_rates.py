"""
A plotting script that plots the angular dependence of recorded scattering events.
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import seaborn as sns
import json
import yaml
from boltmc.utilities.boltmc_constants import *
from plotting.boltmc_plotter import BoltMCPlotter

mpl.use("Agg")


def plot_events_v_rates(simfile, eventfile, figfile):
    colours = sns.color_palette("Paired")

    ie = 100

    filenames = [simfile, eventfile]
    p = BoltMCPlotter(filenames)

    energies_mev = 1e3 / q * p.energies
    n_mats = len(p.matfilenames)
    t_sim = p.n_timesteps * p.timestep
    cpol = np.linspace(-2, 2, p.tracking_n_cpol)

    if not p.events.any():
        raise RuntimeError("No events found!")

    for m in range(n_mats):
        for pt in range(p.max_ptypes):
            # returns true or false for each valley; true only if at least one event occurred in the valley
            check_vals = [p.events[m, pt, v].any() for v in range(p.max_valleys)]
            # returns list of valleys that passed the last test
            valid_vals = [i for i, x in enumerate(check_vals) if x]

            n_axes = sum(check_vals)
            if n_axes > 0:
                # for rates:
                fig, axes = plt.subplots(1, n_axes, figsize=(n_axes * 5, 5))
                # for events:

                for vx, v in enumerate(valid_vals):
                    ax = axes[vx] if n_axes > 1 else axes

                    events_red = np.sum(p.events[m, pt, v], axis=-3)

                    for s in range(len(p.rate_labels)):
                        ax.plot(cpol, events_red[:, s], color=colours[s])

                    ax.set_yscale("log")
                    ax.set_xlabel(r"$\cos\theta$")
                    ax.set_xlim(left=-2, right=2)
                    ax.set_title(p.gaas_valley_dict[str(v)] + " Valley")

                    ax.set_ylim(bottom=9e-1)
                    ax.set_ylabel("Events (per bin)")

                labels = [
                    p.gaas_nice_label(p.rate_labels[s])
                    for s in range(len(p.rate_labels) - 2)
                ]

                fig.legend(
                    labels, loc="upper center", frameon=False, ncol=5, markerscale=6
                )
                fig.subplots_adjust(
                    wspace=0.2, top=0.82, bottom=0.15, left=0.05, right=0.95
                )

                fig.savefig(figfile + "_mat_" + str(m) + "_pt_" + str(pt) + ".pdf")


if __name__ == "__main__":
    plot_events_v_rates("saved_inputs.yaml", "events.json", "cpol")
