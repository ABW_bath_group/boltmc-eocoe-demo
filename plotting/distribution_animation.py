import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from boltmc.utilities.boltmc_constants import *
from plotting.boltmc_plotter import BoltMCPlotter


mpl.use("Agg")


def distribution_animation(
    workfolder=None,
    maxtime=None,
    steptime=1,
    ax=None,
    fig=None,
    meanenergyfile=None,
    distributionfile=None,
    simulationfile=None,
    verbose=False,
    plot_thermal=False,
    plot_mean_energy=False,
    dpi=200,
    savefile="distribution.gif",
    func_handles=[None],
    **plot_kwargs
):

    if not workfolder:
        workfolder = os.getcwd()
    if not ax and not fig:
        fig, ax = plt.subplots()
    elif ax and fig:
        pass
    else:
        raise RuntimeError("Must specify ax and fig or neither")
    if not distributionfile:
        distributionfile = os.path.join(workfolder, "energydists.npz")
    if not simulationfile:
        simulationfile = os.path.join(workfolder, "simulation.json")

    # import simulation data
    plotfiles = [distributionfile, simulationfile]

    if plot_mean_energy:
        if not meanenergyfile:
            meanenergyfile = os.path.join(workfolder, "meanenergy.json")
        plotfiles.append(meanenergyfile)

    p = BoltMCPlotter(plotfiles, verbose=verbose)

    if not maxtime:  # default: set maxtime to length of simulation
        maxtime = len(p.timesteps)

    plottimes = np.arange(0, maxtime, steptime)

    de = p.distribution_energies[1] - p.distribution_energies[0]
    energies_j = p.distribution_energies[:-1]
    energies_mev = energies_j * 1e3 / q
    if plot_mean_energy:
        meanenergies_j = p.meanenergyhistory
        meanenergies_mev = meanenergies_j * 1e3 / q

    def init():
        ax.plot()

    def animate(i):
        if verbose:
            print("Animating {} / {}".format(i, maxtime / steptime))

        dist_idx = plottimes[i]
        ax.clear()
        ax.plot(
            energies_mev, p.distribution[dist_idx] / de / p.n_particles, **plot_kwargs
        )

        axtext = "{:2f} ps".format(p.savetimes[dist_idx] / 1e-12)

        if plot_mean_energy:
            meanenergy_mev = meanenergies_mev[dist_idx]
            axtext = axtext + ", {:2f} meV".format(meanenergy_mev)
            ax.axvline(meanenergy_mev, color="r", alpha=0.5)

        if plot_thermal:
            est_temp = meanenergies_j[dist_idx] * 2 / 3 / k_B
            ax.plot(energies_mev, p.maxwell_boltzmann_en(energies_j, est_temp))

        ax.text(0, 0, axtext)

        # Execute any arbitrary functions, for example axis
        for func in func_handles:
            if func:
                func(ax)

    if verbose:
        print("Animating...")
    ani = FuncAnimation(
        fig, animate, init_func=init, frames=int(maxtime / steptime), interval=100
    )
    ani.save(savefile, writer="imagemagick", dpi=dpi)
    print("Animation saved as {}".format(savefile))
