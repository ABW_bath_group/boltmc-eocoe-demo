"""
Plotting the energy distributon of carriers and the mean energy as a function of time.
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from boltmc.utilities.boltmc_constants import *
from plotting.boltmc_plotter import BoltMCPlotter

mpl.use("Agg")


def plot_energy_distribution(plot_every, simfile, tfile, efile, mbfig, avefig):
    cmap = cm.get_cmap("coolwarm_r")

    filenames = [simfile, tfile, efile]
    p = BoltMCPlotter(filenames)

    energy_bins = 1e3 / q * p.energies[:-1]
    energy_dists = p.distribution
    mean_energies = p.mean_energies
    times = p.savetimes
    rate_table_energy_step = p.rate_table_energy_step * q
    n_particles = p.n_particles
    quasi_temps = np.zeros(len(times))

    fig, ax = plt.subplots()

    for it in range(len(times)):
        if it % plot_every == 0:
            color = "k" if it == 0 else cmap(times[it] / times[-1])
            ax.plot(
                energy_bins,
                energy_dists[it] / rate_table_energy_step / n_particles,
                color=color,
                label="{} ps".format(round(times[it] / 1e-12, 3)),
            )

    for it in range(len(times)):
        quasi_temps[it] = 2 / 3 * mean_energies[it] / k_B
        mb = p.maxwell_boltzmann_en(energy_bins * q / 1e3, quasi_temps[it])
        if it % plot_every == 0:
            color = "k" if it == 0 else cmap(times[it] / times[-1])
            ax.plot(energy_bins, mb, color=color, ls="--")

    ax.set_xlabel("Energy (meV)")
    ax.set_ylabel("Population")

    max_energy = energy_bins[np.max(np.nonzero(energy_dists)[1])]
    ax.set_xlim(left=0, right=max_energy)
    ax.set_ylim(bottom=0, top=1e21)

    ax.legend()

    plt.savefig(mbfig)

    # Temperature plot
    fig, ax = plt.subplots()
    ax.plot(times / 1e-12, quasi_temps)
    # ax.axhline(np.mean(quasi_temps))
    ax.axhline(p.lattice_temperature, ls="--")
    ax.set_xlabel("Time (ps)")
    ax.set_ylabel("Estimated temperature [K]")
    ax.set_xlim(left=0, right=max(times / 1e-12))

    plt.savefig(avefig)


if __name__ == "__main__":
    plot_energy_distribution(
        10,
        "saved_inputs.yaml",
        "savetimes.json",
        "energy_distribution.json",
        "energy_distribution.pdf",
        "mean_energies.pdf",
    )
