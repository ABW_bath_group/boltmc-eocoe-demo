"""
Plotting scripts that compare the events tracked in BoltMC and the rates that were used to generate the events.
"""
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from boltmc.utilities.boltmc_constants import *
from plotting import BoltMCPlotter

mpl.use("Agg")


def plot_events_v_rates(
    simfile, ratefile, eventfile, rfigfile, efigfile, plot_rates=True, plot_events=True
):
    if not plot_events and not plot_rates:
        raise RuntimeError(
            "plot_rates and/or plot_events must be set to True, or there will be nothing to plot."
        )

    colours = sns.color_palette("Paired")

    filenames = [simfile, ratefile, eventfile]
    p = BoltMCPlotter(filenames)

    energies_mev = 1e3 / q * p.energies
    n_mats = len(p.matfilenames)
    t_sim = p.n_timesteps * p.timestep

    print("Total number of events:", np.sum(p.events))

    if not p.events.any():
        raise RuntimeError("No events found!")

    if plot_rates:
        # Convert events to effective scattering rates
        effective_rates = convert_events_to_rates(
            p.events, p.n_particles, total_time=t_sim
        )
    if plot_events:
        # Convert rates to expected number of scattering events
        effective_events = convert_rates_to_events(
            p.rates, p.events, p.n_particles, total_time=t_sim
        )

    for m in range(n_mats):
        for pt in range(p.max_ptypes):
            # returns true or false for each valley; true only if at least one event occurred in the valley
            check_vals = [p.events[m, pt, v].any() for v in range(p.max_valleys)]
            # returns list of valleys that passed the last test
            valid_vals = [i for i, x in enumerate(check_vals) if x]

            n_axes = sum(check_vals)
            print("n_axes:", n_axes)
            if n_axes > 0:
                if plot_rates:
                    rfig, raxes = plt.subplots(1, n_axes, figsize=(n_axes * 5, 5))
                if plot_events:
                    efig, eaxes = plt.subplots(1, n_axes, figsize=(n_axes * 5, 5))

                for vx, v in enumerate(valid_vals):
                    if plot_rates:
                        # PLOT EFFECTIVE RATES
                        rax = raxes[vx] if n_axes > 1 else raxes

                        # Get rates vs energy for all scattering mechanisms at correct material / particle type / valley indices, summing across all final angles where appropriate
                        rates_at_index = p.rates[m, pt, v]
                        eff_rates_at_index = np.sum(effective_rates[m, pt, v], axis=-2)

                        for s in range(len(p.rate_labels)):
                            rax.plot(
                                energies_mev,
                                eff_rates_at_index[:, s],
                                color=colours[s],
                                ls="None",
                                marker=".",
                                ms="3",
                            )
                            rax.plot(
                                energies_mev,
                                rates_at_index[:, s],
                                color=colours[s],
                                ls="-",
                                marker="None",
                            )

                    if plot_events:
                        # PLOT EFFECTIVE EVENTS
                        eax = eaxes[vx] if n_axes > 1 else eaxes

                        # Get events vs energy for all scattering mechanisms at correct material / particle type / valley indices, summing across all final angles where appropriate
                        events_at_index = np.sum(p.events[m, pt, v], axis=-2)
                        eff_events_at_index = effective_events[m, pt, v]

                        for s in range(len(p.rate_labels)):
                            eax.plot(
                                energies_mev,
                                events_at_index[:, s],
                                color=colours[s],
                                ls="None",
                                marker=".",
                                ms="3",
                            )
                            eax.plot(
                                energies_mev,
                                eff_events_at_index[:, s],
                                color=colours[s],
                                ls="-",
                                marker="None",
                            )

                    axes = []
                    if plot_rates:
                        axes.append(rax)
                    if plot_events:
                        axes.append(eax)
                    for ax in axes:
                        ax.set_yscale("log")
                        ax.set_xlabel("Energy [meV]")
                        ax.set_xlim(left=0, right=700)
                        ax.set_title(p.gaas_valley_dict[str(v)] + " Valley")

                    if plot_rates:
                        rax.set_ylim(bottom=1e10, top=1e15)
                        rax.set_ylabel(r"Scattering rate [s$^{-1}$]")

                    if plot_events:
                        ymax = np.max(np.sum(p.events[:, :, :, :, :, :-1], axis=-2))
                        eax.set_ylim(bottom=1e-1, top=2 * ymax)
                        eax.set_ylabel("Events (per energy bin)")
                        eax.axhspan(1e-10, 1, color="red", alpha=0.1)

                labels = [
                    p.gaas_nice_label(p.rate_labels[s])
                    for s in range(len(p.rate_labels) - 2)
                ]

                figs = []
                if plot_rates:
                    figs.append(rfig)
                if plot_events:
                    figs.append(efig)
                for fig in figs:
                    fig.legend(
                        labels, loc="upper center", frameon=False, ncol=5, markerscale=6
                    )
                    fig.subplots_adjust(
                        wspace=0.2, top=0.82, bottom=0.15, left=0.05, right=0.95
                    )

                if plot_rates:
                    rfig.savefig(
                        "{}_mat_{}_pt_{}.png".format(rfigfile, m, pt), dpi=1000
                    )
                if plot_events:
                    efig.savefig(
                        "{}_mat_{}_pt_{}.png".format(efigfile, m, pt), dpi=1000
                    )


def convert_events_to_rates(
    events, n_particles, v_time=False, total_time=None, timestep=None
):
    events_shape = np.shape(events)

    if v_time:
        if not timestep:
            raise RuntimeError("If v_time is True, timestep must be specified.")

        effective_rates = np.zeros(events_shape)
        for tx in range(events_shape[0]):
            effective_rates[tx] = convert_events_to_rates(
                events[tx], n_particles, total_time=timestep, v_time=False
            )

    else:
        if not total_time:
            raise RuntimeError("If v_time is False, total_time must be specified.")

        total_events = np.sum(events)
        effective_rates = np.zeros(events_shape)

        for mx in range(events_shape[0]):  # material index
            for px in range(events_shape[1]):  # particle type index
                for vx in range(events_shape[2]):  # band valley index
                    available_events = np.sum(events[mx, px, vx], axis=-2)
                    events_all_mechs = np.sum(available_events, axis=-1)

                    effective_rates[mx, px, vx, :, 0, :] = (
                        available_events * total_events
                    ) / (events_all_mechs[:, None] * n_particles * total_time)

    effective_rates = np.nan_to_num(effective_rates)
    return effective_rates


def convert_rates_to_events(rates, events, n_particles, total_time):
    rates_shape = np.shape(rates)
    total_events = np.sum(events)
    effective_events = np.zeros(rates_shape)
    for mx in range(rates_shape[0]):  # material index
        for px in range(rates_shape[1]):  # particle type index
            for vx in range(rates_shape[2]):  # band valley index
                available_rates = rates[mx, px, vx]
                available_events = np.sum(events[mx, px, vx], axis=-2)
                events_all_mechs = np.sum(available_events, axis=-1)

                # effective_events[mx, px, vx, :, :] = (
                #     available_rates
                #     * events_all_mechs[:, None]
                #     * n_particles
                #     * total_time
                #     / total_events
                # )

                print(np.shape(available_rates), np.shape(events_all_mechs[:, None]))
                a = available_rates * events_all_mechs[:, None]

    effective_events = np.nan_to_num(effective_events)
    return effective_events


if __name__ == "__main__":
    plot_events_v_rates(
        "saved_inputs.yaml", "rates.json", "events.json", "rate", "events"
    )

    # total_events = np.sum(p.events)
    # available_rates = p.rates[m, pt, v]
    # available_events = np.sum(p.events[m, pt, v], axis=-2)
    # events_all_mechs = np.sum(available_events, axis=-1)

    #  # PLOTTING EFFECTIVE EVENTS
    #             rates_norm = (
    #                 available_rates
    #                 * events_all_mechs[:, None]
    #                 * n_particles
    #                 * total_time
    #                 / total_events
    #             )
