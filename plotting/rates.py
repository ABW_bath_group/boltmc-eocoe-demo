"""
A bunch of plotting scripts that compare the events tracked in BoltMC and the rates that were used to generate the events.
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import seaborn as sns
import json
import yaml
from boltmc.utilities.boltmc_constants import *
from plotting.boltmc_plotter import BoltMCPlotter

mpl.use("Agg")


def plot_rates(
    simfile,
    ratefile,
    rfigfile,
    rfig=None,
    raxes=None,
    ymax=None,
    xmax=None,
    ymin=None,
    xmin=None,
    yscale=None,
    colours=None,
    ls="-",
):
    if not colours:
        colours = sns.color_palette("Paired")

    filenames = [simfile, ratefile]
    p = BoltMCPlotter(filenames, verbose=False)

    energies_mev = 1e3 / q * p.energies
    n_mats = len(p.matfilenames)
    t_sim = p.n_timesteps * p.timestep

    for m in range(n_mats):
        for pt in range(p.max_ptypes):
            # returns true or false for each valley; true only if at least one event occurred in the valley
            check_vals = [p.rates[m, pt, v].any() for v in range(p.max_valleys)]
            # returns list of valleys that passed the last test
            valid_vals = [i for i, x in enumerate(check_vals) if x]

            n_axes = sum(check_vals)
            print("n_axes:", n_axes)
            if n_axes > 0:
                # for rates:
                if not rfig and not raxes:
                    rfig, raxes = plt.subplots(1, n_axes, figsize=(n_axes * 5, 5))

                for vx, v in enumerate(valid_vals):
                    rax = raxes[vx] if n_axes > 1 else raxes

                    rates_red = p.rates[m, pt, v]

                    for s in range(len(p.rate_labels)):
                        rax.plot(energies_mev, rates_red[:, s], color=colours[s], ls=ls)

                    for ax in [rax]:
                        ax.set_yscale(yscale if yscale else "log")
                        ax.set_xlabel("Energy [meV]")
                        ax.set_xlim(left=xmin, right=xmax)
                        ax.set_title(p.gaas_valley_dict[str(v)] + " Valley")

                    rax.set_ylim(bottom=ymin, top=ymax)
                    rax.set_ylabel(r"Scattering rate [s$^{-1}$]")

                labels = [
                    p.gaas_nice_label(p.rate_labels[s])
                    for s in range(len(p.rate_labels) - 2)
                ]

                for fig in [rfig]:
                    fig.legend(
                        labels, loc="upper center", frameon=False, ncol=5, markerscale=6
                    )
                    fig.subplots_adjust(
                        wspace=0.2, top=0.82, bottom=0.15, left=0.05, right=0.95
                    )

                rfig.savefig(rfigfile + "_mat_" + str(m) + "_pt_" + str(pt) + ".png")


if __name__ == "__main__":
    plot_rates("saved_inputs.yaml", "rates.json", "rates")
