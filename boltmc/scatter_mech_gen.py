"""
Module to create scatter functions that can be called from a PPMD kernel. Scatter functions should be implemented as
subclasses of the BaseScatterMech class. Scatter function calling based on a number line of relative rates is
implemented through the ScatterMechHandler class.
"""


import numpy as np
from ppmd.lib.build import write_header
from ctypes import c_double
from ppmd.data import ScalarArray
from ppmd.access import READ


class _BaseScatterMech:
    """
    Base class for scatter functions. Scatter functions should contain the `rate` and `c` attributes. The `rate`
    attribute should be a NumPy array of rates for this scatter function. The `c` attribute is a string literal
    containing a C function that implements the scattering function. e.g.
    ::
        int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {
            return 0;
        }

    The function defined must take a `gsl_rng` pointer followed by 3 `double` pointers. If a `NAME` string substitution
    key is provided it will be populated automatically.
    """

    def __call__(self):
        return self.rate

    def name(self):
        """
        Returns the name of the scattering function.
        """
        return "ScatterMech_" + self.__class__.__name__

    def __str__(self):
        c = self.c % {"NAME": self.name()}
        return c


def ScatterMechInterface(interface=(), header_src=""):
    if type(interface) == str:
        interface = (interface,)
    t = type(
        "BaseScatterMechInterface",
        (_BaseScatterMech,),
        {"interface": interface, "header_src": str(header_src)},
    )
    return t


BaseScatterMech = ScatterMechInterface(("double *", "double *", "double *"))
"""ScatterMechInterface that allows 3 doubles to be passed and modified"""

BaseScatterMechThreeInt = ScatterMechInterface(
    ("double *", "double *", "double *", "int *", "int *", "int *")
)
"""ScatterMechInterface that allows 3 doubles and 3 ints to be passed and modified"""

BaseScatterMechFourInt = ScatterMechInterface(
    ("double *", "double *", "double *", "int *", "int *", "int *", "int *")
)
"""ScatterMechInterface that allows 3 doubles and 4 ints to be passed and modified"""

BaseScatterMechStruct = ScatterMechInterface(
    "ScatterData *",
    """
    typedef struct ScatterData_ {
        // everything that rates may want to access should be defined here
        double *kx;                             // wavevector x component
        double *ky;                             // wavevector y component
        double *kz;                             // wavevector z component
        int *m;                                 // material index
        int *pt;                                // particle type index
        int *v;                                 // valley index
        int *icpol;                             // cos(theta) index (for tracking)
        const double * RESTRICT ALL_K;          // array of all particle wavevectors
        const double * RESTRICT ALL_PT;         // array of all particle types
        const double * RESTRICT SCREENLEN;      // screening length, beta
        const double * RESTRICT SCREENLEN_MIN;  // minimum screening length, beta_min
        int a_id;                               // particle index
        int *event_accepted;                     // whether or not the event was accepted (0 = no, 1 = yes)

    } ScatterData;
    """,
)
"""ScatterMechInterface that passes a pointer to a struct and defines the struct in a header."""


class Padding(BaseScatterMech):
    def __init__(self, interface):
        """
        Class that implements a NULL scattering function that does not modify any particle data.
        """

        self.rate = None  # This attribute is ignored for padding functions.
        self.c = r"""
            int %(NAME)s(gsl_rng * rng, {INTERFACE}) {{
                return 0;
            }}
            """.format(
            INTERFACE=",".join(
                [str(tx) + " I" + str(txi) for txi, tx in enumerate(interface)]
            )
        )


class ScatterMechHandler:
    def __init__(self, scatter_funcs, padding=True, padding_func=None):
        """
        Class to call scatter functions based on the rates. If `padding` is `True` (default) then a padding
        scatter function is called when the random number ends in the padding region. The scatter functions
        should be called with
        ::
            apply_scatter_function(SCATTER_RATES_ARRAY, I0, I1, I2, I3, rng, &kx, &ky, &kz, &sx);

        where `SCATTER_RATES_ARRAY` is provided by the `get_dat_dict` method of this class, `I0`, `I1`, `I2` and `I3`
        are integers that index into the rates of the scatter functions. The number of these indexes is determined by
        the number of dimensions of the rates array in the passed scatter functions. `rng` is a `gsl_rng` pointer and
        `&kx`, `&ky` and `&kz` are double pointers. Finally `&sx` is a pointer to an integer that will be populated
        with the index of the scatter function which was called.

        :arg scatter_funcs: Tuple of instances of subclasses of the `BaseScatterMech`.
        :arg padding: Bool (default True) of whether to pad to the maximum set of rates.
        :arg padding_func: Scattering function (subclass of `BaseScatterMech`) (default: a null padding mechanism).
        """

        self.scatter_funcs = [sx for sx in scatter_funcs]
        if len(scatter_funcs) < 1:
            raise RuntimeError("Require at least one scatter function.")

        # check the scatter func rates are consistent shape with each other
        Ndim = len(scatter_funcs[0].rate.shape)
        dim_shape = scatter_funcs[0].rate.shape
        for sxi, sx in enumerate(scatter_funcs):
            if sx.rate.shape != scatter_funcs[0].rate.shape:
                raise RuntimeError(
                    "Scatter function with index {} has inconsistent rate shape with rate function 0.".format(
                        sxi
                    )
                )
            if np.min(sx.rate.ravel()) < 0:
                raise RuntimeError(
                    "Scatter function with index {} has a negative rate.".format(sxi)
                )

        self.padding = padding
        if padding and (padding_func is not None):
            self.scatter_funcs.append(padding_func)
        elif padding:
            self.scatter_funcs.append(Padding(scatter_funcs[0].interface))

        # Add the function definitions to the header
        s = """
        #include <stdio.h>
        #include <vector>
        #include <functional>
        """
        s += self.scatter_funcs[0].header_src

        # get the block of c function definitions for the scatter functions
        s += "\n".join([str(sx) for sx in self.scatter_funcs])
        # create the array literal to use an rvalue for the vector of functions.
        c_funcs_literal = (
            "{" + ",".join([sx.name() for sx in self.scatter_funcs]) + "};"
        )
        # create the vector of functions
        s += r"""
        // vector of scatter functions
        std::vector<std::function<int(gsl_rng *, {INTERFACE})>> SCATTER_FUNCTIONS = {C_FUNCS_LITERAL}
        """.format(
            C_FUNCS_LITERAL=c_funcs_literal,
            INTERFACE=",".join(self.scatter_funcs[0].interface),
        )

        # check the indexing args the scatter func was called with from the kernel
        scatter_index_checking = "\n".join(
            [
                r"""
                if ((s{I} < 0) || (s{I} >= {MI})){{
                    printf("Error: scatter table lookup. Index s{I} is either < 0 or >= {MI} \n");
                    return 1;
                }}
                """.format(
                    I=str(dxi), MI=str(int(dx))
                )
                for dxi, dx in enumerate(dim_shape)
            ]
        )

        # generate the linear index starting point
        stride = len(self.scatter_funcs)
        l = []
        for dx in reversed(tuple(enumerate(dim_shape))):
            l.append("s{} * {}".format(dx[0], stride))
            stride *= dx[1]
        linind = "const int linind = {} ;".format("+".join(l))

        # wrapper interface
        wrapper_interface = ",".join(
            [
                tx + " ivar" + str(txi)
                for txi, tx in enumerate(self.scatter_funcs[0].interface)
            ]
        )
        # calling interface
        calling_interface = ",".join(
            [
                "ivar" + str(txi)
                for txi, tx in enumerate(self.scatter_funcs[0].interface)
            ]
        )

        # generate the code to select a number line then call a function using the number line
        s += r"""
        #define NUM_SCATTER_RATE_FUNCS {SUB_NUM_RATE_FUNCS}


        #define SCATTER_FUNC_MIN(x, y) ((x) < (y) ? (x) : (y))

        // function to pick a particular scatter function
        int apply_scatter_function(const double * cumsum_array, {SCATTER_DIM_ARGS}, int * sx, gsl_rng * rng, {WRAPPER_INTERFACE}){{

            {SCATTER_INDEX_CHECKING}

            {LININD}


            const double scatter_func_scale = cumsum_array[linind + NUM_SCATTER_RATE_FUNCS - 1];
            const double scatter_func_point = SCATTER_FUNC_MIN(scatter_func_scale * gsl_rng_uniform_pos(rng), scatter_func_scale);

            for(int fx=0 ; fx<NUM_SCATTER_RATE_FUNCS ; fx++){{
                if (scatter_func_point <= cumsum_array[linind + fx]) {{
                    //printf("linind=%d fx=%d | point %f boundary %f\n", linind, fx, scatter_func_point, cumsum_array[linind + fx]);
                    *sx = fx;
                    return SCATTER_FUNCTIONS[fx](rng, {CALLING_INTERFACE});
                }}
            }}

            printf("Error: Failed to find a scatter function to apply.");
            return 1;
        }}
        """.format(
            SUB_NUM_RATE_FUNCS=len(self.scatter_funcs),
            SCATTER_DIM_ARGS=",".join(["const int s" + str(dx) for dx in range(Ndim)]),
            SCATTER_INDEX_CHECKING=scatter_index_checking,
            LININD=linind,
            WRAPPER_INTERFACE=wrapper_interface,
            CALLING_INTERFACE=calling_interface,
        )

        self.header = write_header(s)
        self._construct_rates_array()

    def _construct_rates_array(self):
        num_rate_funcs = len(self.scatter_funcs)
        rates = [sx.rate for sx in self.scatter_funcs]
        if self.padding:
            rates[-1] = np.zeros(rates[0].shape)

        cumsum_rates = np.zeros(list(rates[0].shape) + [num_rate_funcs], c_double)
        # sacrifice something to the numpy slicing gods
        for rxi, rx in enumerate(rates):
            s = tuple(
                [slice(None, None, None) for x in rates[0].shape]
                + [slice(rxi, rxi + 1, 1)]
            )
            v = cumsum_rates[s].view()
            v[:] = rx.copy().reshape(v.shape)

        cumsum_rates[:] = np.cumsum(cumsum_rates, -1)

        maxrate = np.max(cumsum_rates.ravel().view())
        if self.padding:
            s = tuple(
                [slice(None, None, None) for x in rates[0].shape]
                + [slice(num_rate_funcs - 1, num_rate_funcs, 1)]
            )
            v = cumsum_rates[s].view()
            v[:] = maxrate

        self.maxrate = maxrate
        self.cumsum_rates = cumsum_rates
        self.cumsum_scalararray = ScalarArray(ncomp=cumsum_rates.size, dtype=c_double)
        self.cumsum_scalararray[:] = cumsum_rates.ravel().copy()

    def get_dat_dict(self):
        """
        Returns the `dat_dict` required to
        """
        return {"SCATTER_RATES_ARRAY": self.cumsum_scalararray(READ)}

    def get_header_code(self):
        """
        Returns a `ppmd.kernel.Header` that must be used with any kernel that calls the scatter functions.
        """

        return self.header
