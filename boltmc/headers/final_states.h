int final_state_isotropic(double fin_en, double cpol, double azi, double *kx, double *ky, double *kz, int m, int pt, int v){
    /*
    * Computes the final state of a particle in the isotropic case by modifying kx, ky and kz.
    */
    double modkf;
    double cazi, sazi, spol;
    double r_azi;

    modkf = calc_modk(fin_en, m, pt, v);

    if(cpol > 1) cpol = 1;
    if(cpol <-1) cpol =-1;

    cazi = cos(azi);
    sazi = sin(azi);
    spol = sqrt(1.0-cpol*cpol);

    // Update particle data:
    *kx = modkf*spol*cazi;
    *ky = modkf*spol*sazi;
    *kz = modkf*cpol;

    if(!isnormal(*kx) || !isnormal(*ky) || !isnormal(*kz)){
        printf("\n\n(acoustic) Error: k vector not valid.");
        printf("\nk: %e\t%e\t%e", *kx, *ky, *kz);
        printf("\ncpol: %f", cpol);
        return 1;
    }

    return 0;
}


int final_state_anisotropic(double fin_en, double cpol, double azi, double *p_kx, double *p_ky, double *p_kz, int m, int pt, int v){
    /*
    * Computes the final state of a particle in the anisotropic case by modifying kx, ky and kz.
    */
    double modki, modkf;
    double cazi, sazi, spol;
    double kx, ky, kz, skk, x1, x2, x3;
    double smat[3][3];

    kx = *p_kx;
    ky = *p_ky;
    kz = *p_kz;

    modki = sqrt(kx*kx + ky*ky + kz*kz);
    modkf = calc_modk(fin_en, m, pt, v);

    if(cpol > 1) cpol = 1;
    if(cpol <-1) cpol =-1;

    cazi = cos(azi);
    sazi = sin(azi);
    spol = sqrt(1.0 - cpol*cpol);

    x1 = modkf*spol*cazi;
    x2 = modkf*spol*sazi;
    x3 = modkf*cpol;

    if(1 - kz/modki < 1e-15){
        printf("Warning: particle momentum parallel to z axis\n");
        printf("\nmodki = %e", modki);
        printf("\nkz = %e", kz);
        printf("\nmodkf = %e", modkf);
        *p_kx = x1;
        *p_ky = x2;
        *p_kz = x3;
        return 1;
    }
    else{
        skk = sqrt(kx*kx + ky*ky);
        smat[0][0] = ky / skk;
        smat[0][1] = kx*kz / (modki*skk);
        smat[0][2] = kx / modki;
        smat[1][0] = -kx / skk;
        smat[1][1] = ky*kz / (modki*skk);
        smat[1][2] = ky / modki;
        smat[2][0] = 0.0;
        smat[2][1] = -skk / modki;
        smat[2][2] = kz / modki;

        *p_kx = smat[0][0]*x1 + smat[0][1]*x2 + smat[0][2]*x3;
        *p_ky = smat[1][0]*x1 + smat[1][1]*x2 + smat[1][2]*x3;
        *p_kz = smat[2][0]*x1 + smat[2][1]*x2 + smat[2][2]*x3;
    }

    if(!isnormal(kx) || !isnormal(ky) || !isnormal(kz)){
        printf("\n\n(acoustic) Error: k vector not valid.");
        printf("\nk: %e\t%e\t%e", kx, ky, kz);
        printf("\ncpol: %f", cpol);
        return 1;
    }

    return 0;
}


int track_cpol(int *icpol, double cpol){
    /*
    * Takes the cosine of the polar angle (cpol) and computes the correct bin to put it in, then saves it. Used for event tracking.
    */
    *icpol = ceil((cpol+1)*N_CPOL/2.0) - 1; // -1 because indexing starts at 0

    if(*icpol < 0){
        printf("icpol should not be below 0 but is %d. cpol = %f.\n", *icpol, cpol);
        return 1;
    }
    if(*icpol >= N_CPOL){
        printf("icpol is out of bounds, should not exceed %d but is %d. cpol = %f.\n", N_CPOL-1, *icpol, cpol);
        return 1;
    }

    return 0;
}