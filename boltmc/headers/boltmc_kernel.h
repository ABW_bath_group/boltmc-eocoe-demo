// ALL BRACES THAT WILL NOT BE REPLACED BY OTHER CODE IN BOLTMC SHOULD BE DOUBLE BRACKETS, OTHERWISE PYTHON FORMATTING WILL BREAK

{GSL_RNG_INIT} // gets replaced with code needed to initialise gsl rng instance
double init_en, t_rem;
int ie, choice, track_ie, track_idx, state_idx, icpol=0;
int pid = id.i[0];
int m = mat.i[0];
int pt = ptype.i[0];
int v = val.i[0];
int event_accepted = 0;

t_rem = DT;

for(int it = 0; ts.i[0] <= t_rem; it++){{

    drift(ts.i[0], charge.i[0], &k.i[0], &k.i[1], &k.i[2], &pos.i[0], &pos.i[1], &pos.i[2], m, pt, v);

    init_en = calc_energy(k.i[0], k.i[1], k.i[2], m, pt, v);
    ie = ceil(init_en/DE/Q);

    if(ie >= NE){{
        printf("Warning: electron energy higher than max. of scattering table\n");
        ie = NE-1;
    }}
    if(ie < 1){{
        printf("\nError, ie = %d", ie);
        ERR[0] += 1;
    }}

    if(TRACK_EVENTS){{
        track_ie = ceil(init_en/TRACKING_ENERGY_STEP/Q);
        track_ie = (track_ie < TRACKING_ENERGY_N) ? track_ie : TRACKING_ENERGY_N-1;
        state_idx = track_ie + TRACKING_ENERGY_N*(v + N_VALS*(pt + N_PTYPES*m)); // for tracking events in EVENT_TRACKER, which needs initial state
    }}

    // create ScatterData struct, which contains everything that scattering rates may want to know
    event_accepted = 1;
    ScatterData sd = {{&k.i[0], &k.i[1], &k.i[2], &m, &pt, &v, &icpol, &ALL_K[0], &ALL_PT[0], SCREENLEN, SCREENLEN_MIN, pid, &event_accepted}};

    // pick and execute a scattering event
    ERR[0] += apply_scatter_function(SCATTER_RATES_ARRAY, m, pt, v, ie, &choice, rng, &sd);
    if(event_accepted == 0){{
        choice = N_MECHS - 1; // the scattering event chose not to happen, so treat as self-scattering
    }}

    if(!isnormal(k.i[0]) || !isnormal(k.i[1]) || !isnormal(k.i[2])){{
        printf("\nError: k vector not valid: %e\t%e\t%e, ie: %d", k.i[0], k.i[1], k.i[2], ie);
        ERR[0] += 1;
    }}

    if(TRACK_EVENTS){{
        track_idx = choice + N_MECHS*(icpol + N_CPOL*state_idx);
        EVENT_TRACKER[track_idx] += 1;
    }}

    t_rem -= ts.i[0];
    ts.i[0]=-log(gsl_rng_uniform_pos(rng))/GAMMA;

    if(it > MAX_INTERNAL_LOOPS){{
        printf("\nError: Maximum number of loops (%d) reached.", MAX_INTERNAL_LOOPS);
        ERR[0] += 1;
    }}
    if(ERR[0] > 0){{
        break;
    }}
}}

drift(t_rem, charge.i[0], &k.i[0], &k.i[1], &k.i[2], &pos.i[0], &pos.i[1], &pos.i[2], m, pt, v);
ts.i[0] -= t_rem;

mat.i[0] = m;
ptype.i[0] = pt;
val.i[0] = v;
