double calc_band_energy(double kx, double ky, double kz, int m, int pt, int v){
    double ksquared, gammak, discrim, energy;
    ksquared = kx*kx + ky*ky + kz*kz;
    gammak = HBAR*HBAR*ksquared / (2.0*mstar[m][pt][v]);
    discrim = sqrt(1.0 + 4.0*alpha[m][pt][v]*gammak);
    energy = 2.0*gammak / (1.0+discrim);
    return energy;
}

double calc_polaron_energy(double kx, double ky, double kz, int m, int pt, int v){
    double ksquared, gammak, discrim, energy;
    ksquared = kx*kx + ky*ky + kz*kz;
    gammak = HBAR*HBAR*ksquared / (2.0*masspol[m][pt][v]);
    discrim = sqrt(1.0 + 4.0*alpha[m][pt][v]*gammak);
    energy = 2.0*gammak / (1.0+discrim);
    return energy;
}

double calc_modk(double energy, int m, int pt, int v){
    double modk;
    if(POLARON_MODE){
        modk = sqrt(2.0*masspol[m][pt][v]*energy*(1.0+alpha[m][pt][v]*energy)) / HBAR;
    }
    else{
        modk = sqrt(2.0*mstar[m][pt][v]*energy*(1.0+alpha[m][pt][v]*energy)) / HBAR;
    }
    return modk;
}

double calc_energy(double kx, double ky, double kz, int m, int pt, int v){
    double energy;
    if(POLARON_MODE){
        energy = calc_polaron_energy(kx, ky, kz, m, pt, v);
    }
    else{
        energy = calc_band_energy(kx, ky, kz, m, pt, v);
    }
    return energy;
}