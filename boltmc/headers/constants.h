// Sets physical and mathematical constants
const double PI = 3.1415926535897931159979635e+00; // Pi number
const double KB = 1.380649000000000092152161e-23;          // Boltzmann constant (J/K)
const double Q = 1.60217663399999989375623e-19;        // Electron charge (C)
const double H = 6.626070149999999829724907e-34;     // Planck constant (J*s)
const double HBAR = 1.0545718176461564719995744e-34; // Reduced Planck constant (J*s)
const double EPS0 = 8.854187812800000604855354e-12;    // Permittivity of free space (F/m)
const double M0 = 9.109383701500000790034398e-31;      // Electron Mass (kg)