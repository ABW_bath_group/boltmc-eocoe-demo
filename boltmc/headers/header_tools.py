import numpy as np
import os
from boltmc import boltmcdir
from ppmd.lib import build


def _build_headers(self):
    """
    Build all the headers needed for the simulation.
    """
    # Header files that the generated c code needs to use. Make sure they're in the right order.
    hfiles = (
        "constants.h",
        "checks.h",
        "drift_pk.h",
        "calc_energy.h",
        "final_states.h",
    )

    hpath = os.path.join(boltmcdir, "headers")
    self.h_headers = build_header_from_h(hfiles, hpath)
    self.mat_header = build_material_header(
        self.mats, self.max_ptypes, self.max_valleys
    )

    # Simulation constants to go in the constants header. Choice of dict specifies type.
    int_sim_constants = {
        "NE": self.rate_table_energy_n,
        "N_PART": self.n_particles,
        "N_MATS": self.n_mats,
        "N_PTYPES": self.max_ptypes,
        "N_VALS": self.max_valleys,
        "N_MECHS": len(self.smh.scatter_funcs),
        "N_CPOL": self.tracking_n_cpol,
        "MAX_INTERNAL_LOOPS": self.max_internal_loops,
        "TRACKING_ENERGY_N": self.tracking_energy_n,
    }
    float_sim_constants = {
        "TEMP": self.lattice_temperature,
        "FIELD": self.field,
        "DT": self.timestep,
        "DE": self.rate_table_energy_step,
        "GAMMA": self.gamma,
        "TRACKING_ENERGY_STEP": self.tracking_energy_step,
    }
    bool_sim_constants = {
        "TRACK_EVENTS": self.track_events,
        "POLARON_MODE": self.polaron_mode,
    }

    self.sim_header = build_emc_param_header(
        floats=float_sim_constants, ints=int_sim_constants, bools=bool_sim_constants
    )


def file_to_str(filename, folder=None):
    """
    Take a str representing a path to a file and return a str containing the contents of that file.
    """
    filepath = os.path.join(folder, filename) if folder else filename
    if not os.path.exists(filepath):
        raise RuntimeError("Cannot find file {}".format(filepath))

    with open(filepath) as fo:
        filestr = fo.read()
    return filestr


def build_header_from_h(fnames, folder=None):
    """
    Returns a ppmd header object from a list of .h files.
    """
    hstr = ""
    for f in fnames:
        hstr = "\n".join([hstr, file_to_str(f, folder)])
    return build.write_header(hstr)


def build_emc_param_header(floats=None, ints=None, bools=None):
    """
    Constructs a header with all the simulation parameters needed for the simulation.
    """
    s = ""

    if ints:
        for name, val in ints.items():
            type_str = "const int"
            line = type_str + " " + name + " = " + str(int(val)) + ";\n"
            s += line

    if floats:
        for name, val in floats.items():
            type_str = "const double"
            line = type_str + " " + name + " = " + str(val) + ";\n"
            s += line

    if bools:
        type_str = "const bool"
        for name, val in bools.items():
            val_str = bool_py_to_c(val)
            line = type_str + " " + name + " = " + val_str + ";\n"
            s += line

    return build.write_header(s)


def build_material_header(mats, max_ptypes, max_valleys):
    """
    Constructs a header with all the material constants needed for the simulation.
    """
    if len(mats) == 0:
        raise RuntimeError("Must supply at least one material to header file.")

    s = ""
    header_dict = {}

    for label in mats[0].labels:
        header_dict.update({label: np.stack([getattr(m, label) for m in mats])})

    for name, value in header_dict.items():

        first_value = value.flatten()[0]
        # TODO: check if all other values are same type

        if isinstance(first_value, float):
            type_str = "const double"
        elif isinstance(first_value, (int, np.integer)):
            type_str = "const int"
        else:
            raise RuntimeError(
                "Parameter type not recongised when writing material header. Parameter is {} = {} of type {}".format(
                    name, value, type(first_value)
                )
            )

        ndims = len(value.shape)

        bracket_str = "[{}]" * ndims
        list_limits = (len(mats),) + mats[0].list_limits
        bracket_str = bracket_str.format(*list_limits[:ndims])
        array_str = c_nd_array_snippet(value)

        line = type_str + " " + name + bracket_str + " = " + array_str + ";\n"

        s += line

    return build.write_header(s)


def c_1d_array_snippet(array):
    """
    Given a 1d array, returns a string representing the c code declaration of the elements of the array. Fails if passed an array that has more than one dimension; use c_nd_array_snippet instead.
    """
    if len(array.shape) == 1:
        return "{" + ",".join(str(i) for i in array) + "}"
    else:
        raise RuntimeError(
            "Wrong shape, expecting 1 dimension but got {}.".format(len(array.shape))
        )


def c_nd_array_snippet(array):
    """
    Returns a string representing the c code declaration of the elements of an n-dimensional numpy array. To handle the n dimensions, this function calls itself recursively to deal with the n-dimensional arrays within. When a 1d array is found, c_1d_array_snippet is called to start inputting the values in the array.
    """
    if len(array.shape) == 1:
        # array has one dimension, so don't bother with the rest
        return c_1d_array_snippet(array)

    c_array_items = []
    for i in array:
        if len(i.shape) == 1:
            c_array_items.append(c_1d_array_snippet(i))
        else:
            c_array_items.append(c_nd_array_snippet(i))
    return "{" + ",".join(c_array_items) + "}"


def bool_py_to_c(b):
    """
    Converts a python True or False boolean to the string 'true' or 'false', for writing the c declaration.
    """
    return str(b).lower()
