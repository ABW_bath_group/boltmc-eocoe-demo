//Changes a particle wavevector and position according to the drifting time, the applied field and the particle's charge.

void
drift(double tau, double charge, double *kx, double *ky, double *kz, double *px, double *py, double *pz, int m, int pt, int v){
    double mass;
    if(POLARON_MODE){
        mass = masspol[m][pt][v];
    }

    else{
        mass = mstar[m][pt][v];
    }

    double dkx, dky, dkz;

    dkx = charge*Q*FIELD*tau/HBAR;
    dky = 0;
    dkz = 0;

    *px += HBAR/mass * (*kx + 0.5*dkx) * tau;
    *py += HBAR/mass * (*ky + 0.5*dky) * tau;
    *pz += HBAR/mass * (*kz + 0.5*dkz) * tau;

    *kx += dkx;
    *ky += dky;
    *kz += dkz;
}