// Changes a wavenumber component according to the drifting time, the applied field and the particle's charge.
void 
drift(double *kx, double tau, double charge){
    *kx += charge*Q*FIELD*tau/HBAR;
}
