int check_particle_indexes(int m, int pt, int v){
    // This currently isn't being used, but is a good check to put somewhere. '10' needs to be a more meaningful value though.
    if(m<0 || pt<0 || v<0 || m>10 || pt>10 || v>10){{
        printf("Error: particle indexes not valid. m = %d\tpt = %d\tv = %d", m, pt, v);
        return 1;
    }}
    return 0;
}