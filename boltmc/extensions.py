import os
import importlib
from boltmc import boltmcdir


def add_extensions(self, extensions):
    """Add extension class instances to the list of extensions to be triggered before, during and after the simulation run.

    An extension class instance should contain one or more of the following methods:
    - initialise: to be executed before the simulation begins
    - execute: to be executed during each timestep, before the Monte Carlo drift/scatter loop is run
    - finalise: to be execute after the simulation finishes

    :param extensions: A list of extension class instances.
    :type extensions: list
    """
    for ext in extensions:
        self.extensions.append(ext)


def _initialise_extensions(self):
    """
    Run initialise() for all functions defined in the list self.extensions that have that method. Gets called just before simulation starts.
    """
    for ext in self.extensions:
        if callable(getattr(ext, "initialise", None)):
            ext.initialise()


def _execute_extensions(self):
    """
    Run execute() for all functions defined in the list self.extensions that have that method. Gets called every timestep in main loop.
    """
    for ext in self.extensions:
        if callable(getattr(ext, "execute", None)):
            ext.execute()


def _finalise_extensions(self):
    """
    Run finalise() for all functions defined in the list self.extensions that have that method. Gets called just after simulation ends.
    """
    for ext in self.extensions:
        if callable(getattr(ext, "finalise", None)):
            ext.finalise()


def _parse_extensions_from_input_file(self):
    """Take the "custom_extensions" input in the input file (if it exists) and use it to add an arbitrary number of custom extensions. To be compatible with this interface, the ``__init__()`` method of the extension class must take a Simulation class instance as its first argument.

    Extensions are specified by a nested list of dicts. Each entry's key should be the name of the extension class. Each extension can then be given the following keys:
        - ``path``: the path to the file that contains the extension class (including the .py suffix). Defaults to recorders.py in the BoltMC osurce directory.
        - ``args``: a dict of arguments to pass to the extension class instance upon initialisation, expect the Simulation class instance which should be the first argument. Key should be the name of the argument, value should be the argument value.

    For example, an extension class called ``MyRecorder`` has been specified in the file /path/to/myrecorders.py with the following specification:

    ::
        class MyRecorder:
            def __init__(self, sim, foo, bar):
                self.foo = foo
                self.bar = bar
                pass

        def execute(self):
            print(self.foo, self.bar)

    then the following entry will add it to the Simulation class:

    ::
        custom_extensions:
            MyRecorder:
                path: /path/to/myrecorders.py
                args:
                    foo: hello world
                    bar: 42
    """
    if not hasattr(self, "custom_extensions"):
        return
    if self.custom_extensions:
        for extlabel, extspec in self.custom_extensions.items():
            if "path" in extspec:
                extpath = extspec["path"]
            else:
                extpath = os.path.join(boltmcdir, "recorders.py")
            extargs = extspec["args"] if "args" in extspec else {}
            spec = importlib.util.spec_from_file_location("", extpath)
            extmodule = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(extmodule)
            ext = getattr(extmodule, extlabel)(self, **extargs)

            if self.verbose:
                print(
                    "Adding extension {} from file {} with args:".format(
                        extlabel, extpath
                    )
                )
                for arglabel, argval in extargs.items():
                    print("\t{} = {}".format(arglabel, argval))

            add_extensions(self, [ext])
