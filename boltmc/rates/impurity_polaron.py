import numpy as np
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct
from scipy.special import expi


class Impurity(BaseScatterMechStruct):
    def __init__(self, sim):
        self.rate = self.rate_func(sim)
        self.c = r"""
        int %(NAME)s(gsl_rng *rng, ScatterData * SD) {

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *v = SD->v;
            int *icpol = SD->icpol;

            double init_en, fin_en, modki, ksquared;
            double cpol, azi, r_pol1, r_pol2, r_azi, QD2;
            double v_prefact, v_theta1, v_theta2, prob_r1, prob_r2;
            int found_cpol;
            int err = 0;

            ksquared = *kx * *kx + *ky * *ky + *kz * *kz;
            modki = sqrt(ksquared);
            init_en = calc_energy(*kx, *ky, *kz, *m, *pt, *v);
            fin_en = init_en;

            QD2 = Q*Q*impdens[*m] / (eps_s[*m]*KB*TEMP);
            v_prefact = HBAR*mupol[*m][*pt][*v]/(2*mstar[*m][*pt][*v]*mstar[*m][*pt][*v]*wpol[*m][*pt][*v]);

            auto u = [v_prefact, ksquared, QD2](double cospol){{
                return (v_prefact*(2*ksquared*(1.0-cospol)+QD2));
            }};

            // the expression for the scattering probability density given a value of costheta
            auto prob_dens = [](double v_theta){{
                return 1/(v_theta*v_theta) * exp(-v_theta);
            }};

            // Tracks whether the trial value of costheta has been accepted
            found_cpol = 0;
            while(found_cpol==0){{

                // Generate new random numbers
                r_pol1 = gsl_rng_uniform_pos(rng);
                r_pol2 = gsl_rng_uniform_pos(rng);

                // Pick a trial value of costheta between -1 and 1
                cpol = r_pol1 * 2.0 - 1.0;
                v_theta1 = u(cpol);

                // Scattering prob at trial costheta
                prob_r1 = prob_dens(v_theta1);

                // r2 scaled by maximum scattering prob (occurs at costheta = 1)
                v_theta2 = u(1.0);
                prob_r2 = r_pol2*prob_dens(v_theta2);

                if(prob_r2 < prob_r1){{
                    // Accepted trial value of cpol! Otherwise, try again.
                    found_cpol = 1;
                }}
            }}
            r_azi = gsl_rng_uniform_pos(rng);
            azi = 2.0*PI*r_azi;

            err = final_state_anisotropic(fin_en, cpol, azi, kx, ky, kz, *m, *pt, *v);
            err = track_cpol(icpol, cpol);

            return err;
        }
        """

    def rate_func(self, sim):
        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, v, e in idx_iter:
            if e > 0 and v < sim.mats[m].n_vals[p]:

                init_en = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                impdens = sim.impdens[m]
                eps_s = sim.eps_s[m]
                mstar = sim.mstar[m, p, v]
                alpha = sim.alpha[m, p, v]
                masspol = sim.masspol[m, p, v]
                mupol = sim.mupol[m, p, v]
                wpol = sim.wpol[m, p, v]

                init_K = np.sqrt(init_en * 2 * masspol) / hbar
                qd2 = q * q * impdens / (eps_s * k_B * lattice_temperature)
                pre_fact = (
                    impdens
                    * q ** 4
                    * masspol
                    * mupol
                    / (8 * pi * hbar ** 2 * eps_s ** 2 * mstar ** 2 * wpol)
                )
                v_0 = hbar * mupol / (2 * mstar ** 2 * wpol) * qd2
                v_pi = hbar * mupol / (2 * mstar ** 2 * wpol) * (4 * init_K ** 2 + qd2)

                rates[m, p, v, e] = (
                    pre_fact
                    * (1 / init_K)
                    * np.exp(v_0)
                    * (
                        (np.exp(-v_0) / v_0 + expi(-v_0))
                        - (np.exp(-v_pi) / v_pi + expi(-v_pi))
                    )
                )

        return rates
