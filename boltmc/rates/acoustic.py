import numpy as np
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct


class Acoustic(BaseScatterMechStruct):
    def __init__(self, sim):
        self.rate = self.rate_func(sim)
        self.c = r"""
        int %(NAME)s(gsl_rng *rng, ScatterData * SD) {{

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *v = SD->v;
            int *icpol = SD->icpol;

            double init_en, fin_en;
            double cpol, azi, cazi, sazi, spol;
            int err = 0;

            init_en = calc_energy(*kx, *ky, *kz, *m, *pt, *v);
            fin_en = init_en;
            cpol = 1.0 - 2.0*gsl_rng_uniform_pos(rng);
            azi = 2.0*PI*gsl_rng_uniform_pos(rng);

            err = final_state_isotropic(fin_en, cpol, azi, kx, ky, kz, *m, *pt, *v);
            err = track_cpol(icpol, cpol);

            return err;
        }}
        """

    def rate_func(self, sim):
        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, v, e in idx_iter:
            if e > 0 and v < sim.mats[m].n_vals[p]:

                init_en = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                el_const = sim.el_const[m]
                acodefpot = sim.acodefpot[m, p]
                mstar = sim.mstar[m, p, v]
                alpha = sim.alpha[m, p, v]

                dosfact = (2 * mstar) ** 1.5 / (4 * pi ** 2 * hbar ** 3)
                gammak = init_en * (1 + alpha * init_en)
                dos = dosfact * np.sqrt(gammak) * (1 + 2 * alpha * init_en)
                rates[m, p, v, e] = (
                    2
                    * pi
                    * acodefpot ** 2
                    * k_B
                    * lattice_temperature
                    / (el_const * hbar)
                    * dos
                )

        return rates
