import numpy as np
import math
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct


class NpopticalBase(BaseScatterMechStruct):

    c_base = r"""
    int %(NAME)s(gsl_rng *rng, ScatterData * SD) {{

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *vi = SD->v;
            int *icpol = SD->icpol;

            double init_en, fin_en, modkf;
            double cpol, azi, cazi, sazi, spol;
            double deltaVM, hw;
            int vf, err = 0;

            init_en = calc_energy(*kx, *ky, *kz, *m, *pt, *vi);

            vf = {FINALVAL};
            deltaVM = valmin[*m][*pt][vf] - valmin[*m][*pt][*vi];
            hw = hwij[*m][*pt][*vi][vf];

            fin_en = init_en {PLUSMINUS} hw - deltaVM;

            if(fin_en > 0){{

                cpol = 1.0 - 2.0*gsl_rng_uniform_pos(rng);
                azi = 2.0*PI*gsl_rng_uniform_pos(rng);

                err = final_state_isotropic(fin_en, cpol, azi, kx, ky, kz, *m, *pt, vf);
                *vi = vf;

                err = track_cpol(icpol, cpol);

                return err;
            }}
            else{{
                // particle got rounded to bin with non-zero rate but fin_en < 0, so do nothing
                return err;
            }}
        }}
        """

    def rate_func(self, sim, mode, fv):

        if mode == "ab":
            pm_one = 1
        elif mode == "em":
            pm_one = -1
        else:
            raise RuntimeError("mode should be ab or em, input was {}.".format(mode))
        mp_one = -pm_one

        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, iv, e in idx_iter:
            if e > 0 and iv < sim.mats[m].n_vals[p] and fv < sim.mats[m].n_vals[p]:

                init_en = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                massdens = sim.massdens[m]
                mstar = sim.mstar[m, p, fv]
                alpha = sim.alpha[m, p, fv]
                hw = sim.hwij[m, p, iv, fv]
                dtk = sim.dtk[m, p, iv, fv]
                n_fin_vals = sim.numeqvals[m, p, fv]

                if fv == iv:
                    # moving to an equivalent valley, so correct for the one we're currently in
                    n_fin_vals -= 1

                deltavm = sim.valmin[m, p, fv] - sim.valmin[m, p, iv]
                fin_en = init_en + pm_one * hw - deltavm

                if fin_en > 0 and hw > 0 and dtk > 0 and n_fin_vals > 0:
                    gammak = fin_en * (1 + alpha * fin_en)
                    npopt = pi * hbar * dtk ** 2 / (massdens * hw)
                    n_ph = 1 / (math.exp(hw / k_B / lattice_temperature) - 1)
                    dos = (
                        (2 * mstar) ** 1.5
                        / (4 * pi ** 2 * hbar ** 3)
                        * math.sqrt(gammak)
                        * (1 + 2 * alpha * fin_en)
                    )

                    rates[m, p, iv, e] = (
                        npopt * n_fin_vals * dos * (n_ph + 0.5 + mp_one * 0.5)
                    )

        return rates


class Npoptical(NpopticalBase):
    def __init__(self, sim, mode, final_val):
        self.mode = mode
        self.final_val = final_val
        self.mode_dict = {"ab": "+", "em": "-"}
        self.rate = self.rate_func(sim, mode, final_val)
        self.c = self.c_base.format(
            PLUSMINUS=self.mode_dict[mode], FINALVAL=str(int(final_val))
        )

    def name(self):
        """
        Returns the name of the scattering function. This overwrites name() in the definition of BaseScatterMechStruct.
        """
        return (
            "ScatterMech_"
            + self.__class__.__name__
            + "_"
            + self.mode
            + "_{}".format(self.final_val)
        )


def generate_npoptical(sim):
    """
    Generates absorption and emission npoptical scattering rates based on the number of valleys.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    npoptical_instances = []

    for final_val in range(sim.max_valleys):
        for mode in ["ab", "em"]:
            npoptical_instances.append(Npoptical(sim, mode, final_val))

    return npoptical_instances


def all(sim):
    """
    A wrapper for generate_npoptical.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return generate_npoptical(sim)