import numpy as np
import math
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct


class PopticalBase(BaseScatterMechStruct):
    """The base class that defines a polar optical phonon scattering mechanism. Inherits from BaseScatterMechStruct which is defined in scatter_mech_gen.py and contains some extra methods that get used in the background."""

    # This is a string that represents the C (or C++) code that gets run if this scattering mechanism gets selected. The rng pointer represents the random number generator. ScatterData is a struct that contains a lot of useful data that are needed to determine the final states, including the particle wavevectors, indices and some tracking variables.
    c_base = r"""
    int %(NAME)s(gsl_rng *rng, ScatterData * SD) {{

            double *kx = SD->kx; // wavevector x component
            double *ky = SD->ky; // wavevector y component
            double *kz = SD->kz; // wavevector z component
            int *m = SD->m; // material index
            int *pt = SD->pt; // particle type index
            int *v = SD->v; // valley index
            int *icpol = SD->icpol; // index corresponding to the final scattering angle, for tracking

            double init_en, fin_en;
            double cpol, azi, r_pol, r_azi, f;
            int err = 0; // if not zero, interpreted as an error.

            init_en = calc_energy(*kx, *ky, *kz, *m, *pt, *v); // calculate current particle energy (see calc_energy.h)
            fin_en = init_en {PLUSMINUS} {HWO}; // add / subtract phonon energy (using python string formatting for plus/minus depending on abs/em)
            if(fin_en > 0){{

                f = 2.0*sqrt(init_en*fin_en) / pow((sqrt(init_en)-sqrt(fin_en)), 2);
                r_pol = gsl_rng_uniform_pos(rng);
                cpol = (1.0 + f - pow(1.0 + 2.0*f, r_pol)) / f; // cos of polar scattering angle
                r_azi = gsl_rng_uniform_pos(rng);
                azi = 2.0*PI*r_azi; // azimuthal angle, which is completely random

                err = final_state_anisotropic(fin_en, cpol, azi, kx, ky, kz, *m, *pt, *v); // calculate and set final state wavevectors, given cos(theta) and azimuthal angle (see final_states.h)
                err = track_cpol(icpol, cpol); // given scattering angles, convert this into an index which can be used for tracking.

                return err;
            }}
            else{{
                // particle got rounded to bin with non-zero rate but fin_en < 0, so do nothing
                // This will artificially raise the number of events that get tracked, and increase the count at icpol=0. Importantly, should only affect my recording and not the overall kinetics of the simulation.
                return err;
            }}
        }}
        """

    def rate_func(self, sim, mode, hwo, coupling_const):
        """Generate an array of scattering rates as a function of the initial particle energy. This needs re-writing to ignore non-parabolicity and to include coupling constants.

        :param sim: an instance of the BoltMC simulation class.
        :type sim: Simulation class instance
        :param mode: if 'ab', calculate the phonon absorption rate. If 'em', calculate the phonon emission rate.
        :type mode: str
        """
        # convert mode to a plus/minus, used later
        if mode == "ab":
            pm_one = 1
        elif mode == "em":
            pm_one = -1
        else:
            raise RuntimeError("mode should be ab or em, input was {}.".format(mode))
        mp_one = -pm_one  # just the negative of pm_one for convenience

        # defines the basic shape of the rate array. In order:
        # - number of materials
        # - number of particle types (i.e. electron/hole)
        # - (maximum) number of band valleys
        # - number of energy steps
        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        # create a big list of indices to loop over, using itertools
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )
        # m, p, v, e loop over their ranges set in the previous line
        for m, p, v, e in idx_iter:
            # if energy isn't zero and the valley number exists for this particle:
            if e > 0 and v < sim.mats[m].n_vals[p] and hwo > 0:

                init_en = sim.energies[e]  # initial energy (in J)
                lattice_temperature = sim.lattice_temperature  # lattice temperature
                mstar = sim.mstar[m, p, v]  # effective mass
                alpha = sim.alpha[m, p, v]  # non-parabolicity factor (parabolic if 0)
                unit_cell_vol = sim.unit_cell_vol[m]  # volume of the unit cell (in m^3)

                n_ph = 1 / (
                    math.exp(hwo / k_B / lattice_temperature) - 1
                )  # Bose-Einstein population
                fin_en = (
                    init_en + pm_one * hwo
                )  # add / subtract phonon energy depending on mode

                if fin_en > 0:
                    sqrt_ie = np.sqrt(init_en)
                    sqrt_fe = np.sqrt(fin_en)

                    gammak = init_en * (1 + alpha * init_en)
                    cell_const = unit_cell_vol / (4 * pi * hbar)
                    # non-parabolic calculation of wavevector
                    ki = np.sqrt(2 * mstar * gammak) / hbar

                    energy_frac = np.log(
                        np.abs(np.divide(sqrt_fe + sqrt_ie, sqrt_fe - sqrt_ie))
                    )
                    pm_phonon = n_ph + 0.5 + (mp_one * 0.5)

                    rates[m, p, v, e] = (
                        coupling_const
                        * cell_const
                        * ki
                        / init_en
                        * pm_phonon
                        * energy_frac
                    )

        return rates


class Poptical(PopticalBase):
    def __init__(self, hwx, hwo_i, coupling_const, mode, sim):
        """A class that generates a scattering mechanism corresponding to polar optical phonon scattering. This can now take a mode index, which is used when generating scattering mechanisms for multiple phonon modes.

        Inherits from the PopticalBase class (see above), which is where self.rate_func and self.c_base come from.

        :param hwx: the mode index.
        :type hwx: int
        :param hwo_i: the phonon mode frequency (in J)
        :type hwo_i: float
        :param mode: 'ab' for phonon absorption, 'em' for phonon emission
        :type mode: str
        :param sim: an instance of the Boltmc Simulation class
        :type sim: Simulation class instance
        """
        self.hwx = hwx  # hwx is the mode index
        self.hwo_i = hwo_i  # the phonon frequency in J of this mode
        self.coupling_const = coupling_const  # the electron-phonon coupling strength
        self.mode = mode  # either 'ab' for absorption or 'em' for emission (sorry for confusing nomenclature of 'mode', this should be changed)
        self.mode_dict = {
            "ab": "+",
            "em": "-",
        }  # used for modifying c code in base class
        self.rate = self.rate_func(sim, mode, hwo_i, coupling_const)
        self.c = self.c_base.format(PLUSMINUS=self.mode_dict[mode], HWO=self.hwo_i)

    def name(self):
        """
        Returns the name of the scattering function.

        This overwrites name() in the definition of BaseScatterMechStruct, and adds an index for the current phonon mode.
        """
        return (
            "ScatterMech_"
            + self.__class__.__name__
            + "_"
            + self.mode
            + "_{}".format(self.hwx)
        )


def generate_multi_poptical(sim):
    """
    *IN BETA*

    Should generate a list of Poptical class instances, each with a different value of hwo.
    """
    # generate an empty list of scattering mechanisms
    poptical_instances = []

    # loop over phonon modes (frequencies):
    for hwx, hwo_i in enumerate(sim.mats[0].hwo):
        coupling_const = sim.mats[0].coupling_const[hwx]
        # loop over absorption / emission modes:
        for mode in ["ab", "em"]:
            # add a mechanism with correct frequency / mode
            poptical_instances.append(Poptical(hwx, hwo_i, coupling_const, mode, sim))

    return poptical_instances


def all(sim):
    """A wrapper for generate_multi_poptical.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return generate_multi_poptical(sim)
