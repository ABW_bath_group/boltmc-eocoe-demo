import numpy as np
import math
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct
from scipy.special import expi


class PopticalPolaronParabolicBase(BaseScatterMechStruct):
    c_base = r"""
    int %(NAME)s(gsl_rng *rng, ScatterData * SD) {{

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *v = SD->v;
            int *icpol = SD->icpol;
            int *event_accepted = SD->event_accepted;

            double en_i, en_f;
            double cpol, azi, r_azi, f;
            double ki, prefact, ke_sq, ke;
            double u_theta1, u_theta2;
            double r_pol1, r_pol2, prob_r1, prob_r2;
            int found_cpol;
            int err = 0;

            en_i = calc_energy(*kx, *ky, *kz, *m, *pt, *v);
            en_f = en_i {PLUSMINUS} hwo[*m][*pt];
            if(en_f < 0){{
                // particle got rounded to bin with non-zero rate but en_f < 0, so do nothing
                return err;
            }}

            // define some convenient functions
            ki = sqrt(*kx * *kx + *ky * *ky + *kz * *kz);
            prefact = HBAR*mupol[*m][*pt][*v]/(2.0*mstar[*m][*pt][*v]*mstar[*m][*pt][*v]*wpol[*m][*pt][*v]);
            ke_sq = ki*ki {PLUSMINUS} 2*masspol[*m][*pt][*v]/(HBAR*HBAR)*hwo[*m][*pt];
            if(ke_sq<0){{
                return err;
            }}
            ke = sqrt(ke_sq);

            auto u = [prefact, ke, ki](double cospol){{
                return prefact*(ke*ke + ki*ki - 2.0*ke*ki*cospol);
            }};

            // the expression for the scattering probability density given a value of costheta
            auto prob_dens = [ke, prefact](double u_theta){{
                return ke*prefact*(1/u_theta)*exp(-u_theta);
            }};

            // Tracks whether the trial value of costheta has been accepted
            found_cpol = 0;
            while(found_cpol==0){{

                // Generate new random numbers
                r_pol1 = gsl_rng_uniform_pos(rng);
                r_pol2 = gsl_rng_uniform_pos(rng);

                // Pick a trial value of costheta between -1 and 1
                cpol = r_pol1 * 2.0 - 1.0;
                u_theta1 = u(cpol);

                // Scattering prob at trial costheta
                prob_r1 = prob_dens(u_theta1);

                // r2 scaled by maximum scattering prob (occurs at costheta = 1)
                u_theta2 = u(1.0);
                prob_r2 = r_pol2 * prob_dens(u_theta2);

                if(prob_r2 < prob_r1){{
                    // Accepted trial value of cpol! Otherwise, try again.
                    found_cpol = 1;
                }}
            }}

            r_azi = gsl_rng_uniform_pos(rng);
            azi = 2.0*PI*r_azi;
            err = final_state_anisotropic(en_f, cpol, azi, kx, ky, kz, *m, *pt, *v);
            err = track_cpol(icpol, cpol);
            return err;
        }}
        """

    def rate_func(self, sim, mode):

        if mode == "ab":
            pm_one = 1
        elif mode == "em":
            pm_one = -1
        else:
            raise RuntimeError("mode should be ab or em, input was {}.".format(mode))
        mp_one = -pm_one

        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, v, e in idx_iter:
            if e > 0 and v < sim.mats[m].n_vals[p]:

                init_en = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                eps_s = sim.eps_s[m]
                eps_inf = sim.eps_inf[m]
                hwo = sim.hwo[m, p]
                mstar = sim.mstar[m, p, v]
                alpha = sim.alpha[m, p, v]
                masspol = sim.masspol[m, p, v]
                mupol = sim.mupol[m, p, v]
                wpol = sim.wpol[m, p, v]

                eps_p = 1 / (1 / eps_inf - 1 / eps_s)
                n_ph = 1 / (math.exp(hwo / k_B / lattice_temperature) - 1)
                fin_en = init_en + pm_one * hwo

                if fin_en > 0:
                    sqrt_ie = np.sqrt(init_en)
                    sqrt_fe = np.sqrt(fin_en)

                    init_K = np.sqrt(init_en * 2 * masspol) / hbar
                    pre_fact = (
                        q * q * (hwo / hbar) * masspol / (8 * pi * hbar ** 2 * init_K)
                    )
                    K_pm_sq = init_K ** 2 + 2 * masspol / (hbar ** 2) * pm_one * hwo
                    if K_pm_sq > 0:
                        K_pm = np.sqrt(K_pm_sq)
                        u_0 = (
                            hbar
                            * mupol
                            / (2 * mstar ** 2 * wpol)
                            * (K_pm - init_K) ** 2
                        )
                        u_pi = (
                            hbar
                            * mupol
                            / (2 * mstar ** 2 * wpol)
                            * (K_pm + init_K) ** 2
                        )
                        rates[m, p, v, e] = (
                            pre_fact
                            * (1 / eps_p)
                            * (n_ph + 0.5 + mp_one * 0.5)
                            * (expi(-u_pi) - expi(-u_0))
                        )

        return rates


class Poptical(PopticalPolaronParabolicBase):
    def __init__(self, sim, mode):
        """A class that generates a scattering mechanism corresponding to polar optical phonon scattering. This can now take a mode index, which is used when generating scattering mechanisms for multiple phonon modes.

        Inherits from the PopticalBase class (see above), which is where self.rate_func and self.c_base come from.

        :param hwx: the mode index.
        :type hwx: int
        :param hwo_i: the phonon mode frequency (in J)
        :type hwo_i: float
        :param mode: 'ab' for phonon absorption, 'em' for phonon emission
        :type mode: str
        :param sim: an instance of the Boltmc Simulation class
        :type sim: Simulation class instance
        """
        self.mode = mode  # either 'ab' for absorption or 'em' for emission
        self.mode_dict = {
            "ab": "+",
            "em": "-",
        }  # used for modifying c code in base class
        self.rate = self.rate_func(sim, mode)
        self.c = self.c_base.format(PLUSMINUS=self.mode_dict[mode])

    def name(self):
        """
        Returns the name of the scattering function. This overwrites name() in the definition of BaseScatterMechStruct.
        """
        return "ScatterMech_" + self.__class__.__name__ + "_" + self.mode


def generate_poptical_modes(sim):
    """Returns two Poptical class instances corresponding to the emission  / absorption of polar optical phonons.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return [Poptical(sim, mode) for mode in ["ab", "em"]]


def all(sim):
    """A wrapper for generate_poptical_modes.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return generate_poptical_modes(sim)
