import numpy as np
import math
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct


class PopticalParabolicBase(BaseScatterMechStruct):
    c_base = r"""
    int %(NAME)s(gsl_rng *rng, ScatterData * SD) {{

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *v = SD->v;
            int *icpol = SD->icpol;
            int *event_accepted = SD->event_accepted;

            double init_en, fin_en;
            double cpol, azi, r_pol, r_azi, f;
            int err = 0;

            init_en = calc_energy(*kx, *ky, *kz, *m, *pt, *v);
            fin_en = init_en {PLUSMINUS} hwo[*m][*pt];
            if(fin_en > 0){{

                f = 2.0*sqrt(init_en*fin_en) / pow((sqrt(init_en)-sqrt(fin_en)), 2);
                r_pol = gsl_rng_uniform_pos(rng);
                cpol = (1.0 + f - pow(1.0 + 2.0*f, r_pol)) / f;
                r_azi = gsl_rng_uniform_pos(rng);
                azi = 2.0*PI*r_azi;

                err = final_state_anisotropic(fin_en, cpol, azi, kx, ky, kz, *m, *pt, *v);
                err = track_cpol(icpol, cpol);

                return err;
            }}
            else{{
                // particle got rounded to bin with non-zero rate but fin_en < 0, so do nothing
                // This might not be the best idea; this will artificially raise the number of events that get tracked, and increase the count at icpol=0. Importantly, should only affect my recording and not the overall kinetics of the simulation.
                return err;
            }}
        }}
        """

    def rate_func(self, sim, mode):

        if mode == "ab":
            pm_one = 1
        elif mode == "em":
            pm_one = -1
        else:
            raise RuntimeError("mode should be ab or em, input was {}.".format(mode))
        mp_one = -pm_one

        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, v, e in idx_iter:
            if e > 0 and v < sim.mats[m].n_vals[p]:

                init_en = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                eps_s = sim.eps_s[m]
                eps_inf = sim.eps_inf[m]
                hwo = sim.hwo[m, p]
                mstar = sim.mstar[m, p, v]
                alpha = sim.alpha[m, p, v]

                eps_p = 1 / (1 / eps_inf - 1 / eps_s)
                n_ph = 1 / (math.exp(hwo / k_B / lattice_temperature) - 1)
                fin_en = init_en + pm_one * hwo

                if fin_en > 0:
                    sqrt_ie = np.sqrt(init_en)
                    sqrt_fe = np.sqrt(fin_en)

                    gammak = init_en * (1 + alpha * init_en)
                    popt = q * q * (hwo / hbar) / (8 * pi * eps_p)
                    ki = np.sqrt(2 * mstar * gammak) / hbar

                    rates[m, p, v, e] = (
                        popt
                        * ki
                        / init_en
                        * (n_ph + 0.5 + mp_one * 0.5)
                        * np.log(
                            np.abs(np.divide(sqrt_fe + sqrt_ie, sqrt_fe - sqrt_ie))
                        )
                    )

        return rates


class PopticalParabolic(PopticalParabolicBase):
    def __init__(self, sim, mode):
        """A class that generates a scattering mechanism corresponding to polar optical phonon scattering. This can now take a mode index, which is used when generating scattering mechanisms for multiple phonon modes.

        Inherits from the PopticalBase class (see above), which is where self.rate_func and self.c_base come from.

        :param hwx: the mode index.
        :type hwx: int
        :param hwo_i: the phonon mode frequency (in J)
        :type hwo_i: float
        :param mode: 'ab' for phonon absorption, 'em' for phonon emission
        :type mode: str
        :param sim: an instance of the Boltmc Simulation class
        :type sim: Simulation class instance
        """
        self.mode = mode  # either 'ab' for absorption or 'em' for emission
        self.mode_dict = {
            "ab": "+",
            "em": "-",
        }  # used for modifying c code in base class
        self.rate = self.rate_func(sim, mode)
        self.c = self.c_base.format(PLUSMINUS=self.mode_dict[mode])

    def name(self):
        """
        Returns the name of the scattering function. This overwrites name() in the definition of BaseScatterMechStruct.
        """
        return "ScatterMech_" + self.__class__.__name__ + "_" + self.mode


def generate_poptical_modes(sim):
    """Returns two Poptical class instances corresponding to the emission  / absorption of polar optical phonons.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return [PopticalParabolic(sim, mode) for mode in ["ab", "em"]]


def all(sim):
    """A wrapper for generate_poptical_modes.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return generate_poptical_modes(sim)
