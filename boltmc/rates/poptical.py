import numpy as np
import math
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct


class PopticalBase(BaseScatterMechStruct):
    c_base = r"""
    int %(NAME)s(gsl_rng *rng, ScatterData * SD) {{

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *v = SD->v;
            int *icpol = SD->icpol;
            int *event_accepted = SD->event_accepted;

            double alp, en_i, en_f, gam_i, gam_f, a_i, a_f, c_i, c_f;
            double cpol, azi, r_pol1, r_pol2, r_azi;
            double prob_r1, prob_r2;
            int found_cpol;
            int err = 0;

            en_i = calc_energy(*kx, *ky, *kz, *m, *pt, *v);
            en_f = en_i {PLUSMINUS} hwo[*m][*pt];
            if(en_f < 0){{
                // particle got rounded to bin with non-zero rate but en_f < 0, so do nothing
                return err;
            }}

            // define some convenient functions
            alp = alpha[*m][*pt][*v];
            gam_i = en_i * (1 + alp * en_i);
            gam_f = en_f * (1 + alp * en_f);
            a_i = sqrt((1 + alp * en_i) / (1 + 2 * alp * en_i));
            a_f = sqrt((1 + alp * en_f) / (1 + 2 * alp * en_f));
            c_i = sqrt(alp * en_i / (1 + 2 * alp * en_i));
            c_f = sqrt(alp * en_f / (1 + 2 * alp * en_f));

            // the expression for the scattering probability density given a value of costheta
            auto prob_dens = [a_i, a_f, c_i, c_f, gam_i, gam_f](double cospol){{
                return pow(a_i * a_f + c_i * c_f * cospol, 2.0) /
                            (gam_i + gam_f - 2.0 * pow(gam_i * gam_f, 0.5) * cospol);
            }};

            // Tracks whether the trial value of costheta has been accepted
            found_cpol = 0;
            while(found_cpol==0){{

                // Generate new random numbers
                r_pol1 = gsl_rng_uniform_pos(rng);
                r_pol2 = gsl_rng_uniform_pos(rng);

                // Pick a trial value of costheta between -1 and 1
                cpol = r_pol1 * 2.0 - 1.0;

                // Scattering prob at trial costheta
                prob_r1 = prob_dens(cpol);
                // r2 scaled by maximum scattering prob (occurs at costheta = 1)
                prob_r2 = r_pol2 * prob_dens(1.0);

                if(prob_r2 < prob_r1){{
                    // Accepted trial value of cpol! Otherwise, try again.
                    found_cpol = 1;
                }}
            }}

            r_azi = gsl_rng_uniform_pos(rng);
            azi = 2.0*PI*r_azi;
            err = final_state_anisotropic(en_f, cpol, azi, kx, ky, kz, *m, *pt, *v);
            err = track_cpol(icpol, cpol);
            return err;
        }}
        """

    def rate_func(self, sim, mode):
        """
        See Fawcett et al (1970): https://doi.org/10.1016/0022-3697(70)90001-6
        """
        if mode == "ab":
            pm_one = 1
        elif mode == "em":
            pm_one = -1
        else:
            raise RuntimeError("mode should be ab or em, input was {}.".format(mode))
        mp_one = -pm_one

        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, v, e in idx_iter:
            if e > 0 and v < sim.mats[m].n_vals[p]:

                en_i = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                eps_s = sim.eps_s[m]
                eps_inf = sim.eps_inf[m]
                hwo = sim.hwo[m, p]
                mstar = sim.mstar[m, p, v]
                alpha = sim.alpha[m, p, v]

                eps_p = 1 / (1 / eps_inf - 1 / eps_s)
                n_ph = 1 / (math.exp(hwo / k_B / lattice_temperature) - 1)
                en_f = en_i + pm_one * hwo

                if en_f > 0:
                    gamma_i = en_i * (1 + alpha * en_i)
                    gamma_f = en_f * (1 + alpha * en_f)

                    # common brackets for convenience
                    ae_i = 1 + alpha * en_i
                    ae2_i = 1 + 2 * alpha * en_i
                    ae_f = 1 + alpha * en_f
                    ae2_f = 1 + 2 * alpha * en_f

                    A = (2 * ae_i * ae_f + alpha * (gamma_i + gamma_f)) ** 2
                    B = (
                        -2
                        * alpha
                        * gamma_i ** 0.5
                        * gamma_f ** 0.5
                        * (4 * ae_i * ae_f + alpha * (gamma_i + gamma_f))
                    )
                    C = 4 * ae_i * ae_f * ae2_i * ae2_f

                    band_shape = np.sqrt(2 * mstar) / hbar * ae2_f / (gamma_i ** 0.5)
                    popt = q * q * (hwo / hbar) / (8 * pi * eps_p)

                    ln_gamma = np.log(
                        np.abs(
                            (gamma_i ** 0.5 + gamma_f ** 0.5)
                            / (gamma_i ** 0.5 - gamma_f ** 0.5)
                        )
                    )

                    # Put it all together
                    rates[m, p, v, e] = (
                        popt
                        * (n_ph + 0.5 + mp_one * 0.5)
                        * band_shape
                        / C
                        * (A * ln_gamma + B)
                    )
        return rates


class Poptical(PopticalBase):
    def __init__(self, sim, mode):
        """A class that generates a scattering mechanism corresponding to polar optical phonon scattering. This can now take a mode index, which is used when generating scattering mechanisms for multiple phonon modes.

        Inherits from the PopticalBase class (see above), which is where self.rate_func and self.c_base come from.

        :param hwx: the mode index.
        :type hwx: int
        :param hwo_i: the phonon mode frequency (in J)
        :type hwo_i: float
        :param mode: 'ab' for phonon absorption, 'em' for phonon emission
        :type mode: str
        :param sim: an instance of the Boltmc Simulation class
        :type sim: Simulation class instance
        """
        self.mode = mode  # either 'ab' for absorption or 'em' for emission
        self.mode_dict = {
            "ab": "+",
            "em": "-",
        }  # used for modifying c code in base class
        self.rate = self.rate_func(sim, mode)
        self.c = self.c_base.format(PLUSMINUS=self.mode_dict[mode])

    def name(self):
        """
        Returns the name of the scattering function. This overwrites name() in the definition of BaseScatterMechStruct.
        """
        return "ScatterMech_" + self.__class__.__name__ + "_" + self.mode


def generate_poptical_modes(sim):
    """Returns two Poptical class instances corresponding to the emission  / absorption of polar optical phonons.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return [Poptical(sim, mode) for mode in ["ab", "em"]]


def all(sim):
    """A wrapper for generate_poptical_modes.

    :param sim: the BoltMC Simulation class instance.
    :type sim: Simulation class
    """
    return generate_poptical_modes(sim)
