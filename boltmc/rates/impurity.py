import numpy as np
from itertools import product
from boltmc.utilities.boltmc_constants import *
from boltmc.scatter_mech_gen import BaseScatterMechStruct


class Impurity(BaseScatterMechStruct):
    def __init__(self, sim):
        self.rate = self.rate_func(sim)
        self.c = r"""
        int %(NAME)s(gsl_rng *rng, ScatterData * SD) {

            double *kx = SD->kx;
            double *ky = SD->ky;
            double *kz = SD->kz;
            int *m = SD->m;
            int *pt = SD->pt;
            int *v = SD->v;
            int *icpol = SD->icpol;

            double init_en, fin_en, modki, ksquared;
            double cpol, azi, r_pol, r_azi, QD2;
            int err = 0;

            ksquared = *kx * *kx + *ky * *ky + *kz * *kz;
            modki = sqrt(ksquared);
            init_en = calc_energy(*kx, *ky, *kz, *m, *pt, *v);
            fin_en = init_en;

            QD2 = Q*Q*impdens[*m] / (eps_s[*m]*KB*TEMP);
            r_pol = gsl_rng_uniform_pos(rng);
            cpol = 1.0 - 2*r_pol / (1.0 + (1.0 - r_pol)*(4*modki*modki/QD2));
            r_azi = gsl_rng_uniform_pos(rng);
            azi = 2.0*PI*r_azi;

            err = final_state_anisotropic(fin_en, cpol, azi, kx, ky, kz, *m, *pt, *v);
            err = track_cpol(icpol, cpol);

            return err;
        }
        """

    def rate_func(self, sim):
        rates = np.zeros(
            (sim.n_mats, sim.max_ptypes, sim.max_valleys, sim.rate_table_energy_n)
        )
        idx_iter = product(
            range(sim.n_mats),
            range(sim.max_ptypes),
            range(sim.max_valleys),
            range(sim.rate_table_energy_n),
        )

        for m, p, v, e in idx_iter:
            if e > 0 and v < sim.mats[m].n_vals[p]:

                init_en = sim.energies[e]
                lattice_temperature = sim.lattice_temperature
                impdens = sim.impdens[m]
                eps_s = sim.eps_s[m]
                mstar = sim.mstar[m, p, v]
                alpha = sim.alpha[m, p, v]

                qd2 = q * q * impdens / (eps_s * k_B * lattice_temperature)

                dosfact = (2 * mstar) ** 1.5 / (4 * pi ** 2 * hbar ** 3)
                imp = 2 * pi * impdens * (q * q / eps_s) ** 2 / hbar
                gammak = init_en * (1 + alpha * init_en)
                dos = dosfact * np.sqrt(gammak) * (1 + 2 * alpha * init_en)
                modk2 = 2 * mstar * gammak / (hbar ** 2)
                qq = qd2 * (4 * modk2 + qd2)

                rates[m, p, v, e] = imp * dos / qq

        return rates
