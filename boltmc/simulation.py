import numpy as np
import os
import importlib
import yaml
import warnings

from ppmd import *
from ctypes import c_double, c_int64

from boltmc import boltmcdir, polaron
from boltmc.utilities.boltmc_constants import *
from boltmc.utilities import kernel_gsl_rng, geometries
from boltmc.scatter_mech_gen import ScatterMechHandler
from boltmc.headers.header_tools import file_to_str


class Simulation:
    """A class that contains the entire BoltMC simulation.

    The Simulation class is first initialised, which involves setting up all particles, the scattering rate table, the PPMD state and any function handles. Once it has been initialised, the ``run()`` method can be called which runs the ensemble Monte Carlo simulation.

    The input file is used to generate attributes of the simulation class instance.

    :param infile: The path to the input file for the simulation. If None, a default set is used, found at /path/to/boltmc/inputs/defaults.yaml. Defaults to None.
    :type infile: str, optional
    :param params: A dict containing extra inputs to be given to the simulation class. Any inputs with the same name as one found in the input file will be overwritten. Defaults to None.
    :type params: dict, optional
    :param setup: Immediately setup the simulation. Defaults to True.
    :type params: bool, optional
    :param run: Immediately run the simulation. Defaults to False.
    :type params: bool, optional
    """

    # these methods are found in other files but are treated as methods of the Simulation class.
    from boltmc.printing import _print_preamble, _print_inputs, _print_progress
    from boltmc.inputs import _load_inputs
    from boltmc.material import _load_materials
    from boltmc.headers.header_tools import _build_headers
    from boltmc.thermaliser import _setup_thermaliser
    from boltmc.extensions import (
        add_extensions,
        _initialise_extensions,
        _execute_extensions,
        _finalise_extensions,
        _parse_extensions_from_input_file,
    )
    from boltmc.kstates import (
        _band_ks_from_energy,
        _polaron_ks_from_energy,
        _get_maxwell_boltzmann_ks,
        _get_initial_maxwell_boltzmann_ks,
        _get_initial_delta_ks,
        _get_gaussian_ks,
        _get_ks_from_file,
        _get_initial_ks,
        set_maxwell_boltzmann_ks,
    )
    from boltmc import polaron

    def __init__(self, infile=None, params=None, setup=True, run=False):
        """Initialises the Simulation class.

        :param infile: The path to the input file for the simulation. If None, a default set is used, found at /path/to/boltmc/inputs/defaults.yaml. Defaults to None.
        :type infile: str, optional
        :param params: A dict containing extra inputs to be given to the simulation class. Any inputs with the same name as one found in the input file will be overwritten. Defaults to None.
        :type params: dict, optional
        :param setup: Immediately setup the simulation. Defaults to True.
        :type params: bool, optional
        :param run: Immediately run the simulation. Defaults to False.
        :type params: bool, optional
        """
        if setup:
            self.setup_simulation(infile, params)
        if run:
            self.run()

    def setup_simulation(self, infile=None, params=None):
        """Sets up the simulation, ready for run() to be executed. Involves setting up all particles, the scattering rate table, the PPMD state and any function handles.

        :param infile: The path to the input file for the simulation. If None, a default set is used, found at /path/to/boltmc/inputs/defaults.yaml. Defaults to None.
        :type infile: str, optional
        :param params: A dict containing extra inputs to be given to the simulation class. Any inputs with the same name as one found in the input file will be overwritten. Defaults to None.
        :type params: dict, optional
        """
        # No parameters specified? Use defaults.
        if not infile:
            infile = os.path.join(boltmcdir, "inputs/defaults.yaml")
        # Load simulation parameters from .yaml file
        self._load_inputs(infile, params)
        # Print preamble (if silent = False)
        self._print_preamble()
        # Print a list of all inputs (if verbose = True)
        self._print_inputs()
        # List of functions to be executed in main loop. Empty for now but can be added using self.add_extensions()
        self.extensions = []
        # If instant_thermalisation is set to true, add a Thermaliser to the list of extensions.
        self._setup_thermaliser()
        # Random number generator used in python framework
        self.rng = np.random.RandomState(self.rngseed)
        # Generate a seed for gsl rng (initialised later, used in kernel)
        self.gslrngseed = self.rng.randint(0, 2 ** 32 - 1)
        # Make energy scale used in scattering table (in J)
        self._set_energy_rate_table()
        # Particle types (for example, electron and hole)
        self.ptypes = np.arange(self.max_ptypes)
        # Conduction / valance band valleys
        self.vals = np.arange(self.max_valleys)
        # Materials
        self.mats = self._load_materials()
        # Generate some useful parameter arrays for calculating rates etc.
        self._build_param_arrays()
        # Set up polarons if needed
        self._setup_polarons()
        # Set up PPMD State
        self._setup_ppmd_state()
        # Load scattering mechanisms, calculate rates and generate scattering table
        self._load_scattering_mechs()
        # Setup event tracker to record how many events of each type occured during simulation
        self._setup_event_tracker()
        # Make array with all wavevectors, useful for carrier-carrier scattering
        self._make_all_k()
        self._make_all_pt()
        # Setup the initial properties of particles, and assign them to the PPMD state
        self._set_initial_state()
        # Generate timesteps for simulation
        self._set_timesteps()
        # Generate savetimes for simulation
        self._set_saveids()
        # dict containing particle data
        self.dat_dict = {
            "id": self.State.id(access.READ),
            "pos": self.State.pos(access.WRITE),
            "k": self.State.k(access.WRITE),
            "mat": self.State.mat(access.WRITE),
            "ptype": self.State.ptype(access.WRITE),
            "val": self.State.val(access.WRITE),
            "ts": self.State.ts(access.WRITE),
            "charge": self.State.charge(access.WRITE),
            "EVENT_TRACKER": self.State.EVENT_TRACKER(access.INC),
            "ERR": self.State.err(access.INC_ZERO),
            "ALL_K": self.State.ALL_K(access.READ),
            "ALL_PT": self.State.ALL_PT(access.READ),
            "SCREENLEN": self.State.SCREENLEN(access.READ),
            "SCREENLEN_MIN": self.State.SCREENLEN_MIN(access.READ),
        }
        # Setup the GSL rng module for use in the kernel, and add to dat_dict.
        self._setup_gsl_rng()
        # Update dat_dict to include scattering rates
        self.dat_dict.update(self.smh.get_dat_dict())
        # Build the headers needed for the kernel
        self._build_headers()
        # Main kernel, to be looped over all particles
        kernel_str = file_to_str(os.path.join(boltmcdir, "headers", "boltmc_kernel.h"))
        self.emc_loop = loop.ParticleLoopOMP(
            kernel.Kernel(
                "boltmc_kernel",
                kernel_str.format(GSL_RNG_INIT=self.gsl_rng_module.get_kernel_code()),
                headers=(
                    kernel.Header("stdio.h"),
                    kernel.Header("math.h"),
                    kernel.Header("string.h"),
                    kernel.Header("gsl/gsl_rng.h"),
                    kernel.Header("gsl/gsl_sf_expint.h"),
                    self.sim_header,
                    self.mat_header,
                    self.h_headers,
                    self.smh.get_header_code(),
                ),
            ),
            self.dat_dict,
        )

        self._parse_extensions_from_input_file()

    def run(self):
        """Runs the simulation, executing any custom function handles before, during and after the main loop."""
        if self.verbose:
            print("Running simulation...")

        # Set time to 0, in case any extensions need it
        self.current_time = 0

        # Initialise any function handles
        self._initialise_extensions()

        # Main loop
        for self.it, self.current_time in enumerate(self.timesteps):

            # Print current progress (if not silent and save_check() returns True)
            self._print_progress()

            # Execute any function handles
            self._execute_extensions()

            # Execute the ensemble Monte Carlo loop
            self.emc_loop.execute()

            # Check if the main kernel loop raised an error
            if self.State.err[0] != 0:
                raise RuntimeError(
                    "Kernel raised error, value = {}.".format(self.State.err[0])
                )

        # Finalise any function handles
        self._finalise_extensions()

        if self.verbose:
            print("Simulation has finished!")

    def save_check(self):
        """Checks whether the criteria for saving data has been met, based on the current timestep and the specified savetimes.

        :return: True if the current timestep index is within the list of indexes that trigger a save event, False otherwise
        :rtype: bool
        """
        return self.it in self.saveids

    def _load_scattering_mechs(self):
        """
        Load all of the specified scattering mechanisms into a ScatterMechHandler object.

        The user can specify the location and class name of the custom rate class.
        """
        if not self.silent:
            print("Loading scattering mechanisms...")

        self.mechs = []
        self.scattering_mech_filenames = []
        self.scattering_mech_classnames = []

        for filename, classnames in self.scattering_mechs.items():
            if isinstance(classnames, str):  # just one mechanism, no list
                self._add_scattering_mech(filename, classnames)

            elif isinstance(classnames, list):  # a list of mechanisms in one file
                for classname in classnames:
                    self._add_scattering_mech(filename, classname)

            else:
                raise RuntimeError(
                    "Mechanisms in module '{}' have been specified in an unknown format: {}".format(
                        filename, classnames
                    )
                )

        if not self.silent:
            print("Scattering Mechanisms:")
            for mech in self.mechs:
                print("- {}".format(mech.name()))
        # TODO: check if they have the same names
        self.smh = ScatterMechHandler(self.mechs, padding=True)

        self.gamma = self.smh.maxrate
        if self.verbose:
            print("Loaded scattering mechanisms.")
            print("Maximum Rate (Gamma): {:.3e} s-1".format(self.gamma))

    def _add_scattering_mech(self, filename, classname):
        """Adds a scattering mechanism, or a list of scattering mechanisms, to self.mechs, given the name of a class or function that returns a list of classes.

        :param filename: the path to the file that contains the scattering mechanism generator. If filename does not exist, the default path (boltmc/rates) will be checked.
        :type filename: str
        :param classname: the name of the class / function to use when generating scattering mechanism(s). If classname represents a class, an instance of that class will be added to self.mechs. If classname represents a function, that function should return a list of class instances, or a single class instance.
        :type classname: str
        """
        # if filename is a complete path, use it
        if os.path.exists(filename):
            mechpath = filename
        # if not, assume it can be found within boltmc/rates.
        else:
            default_path = os.path.join(boltmcdir, "rates")
            mechpath = os.path.join(default_path, filename)
        # raise error if mechpath does not exist.
        if not os.path.exists(mechpath):
            raise RuntimeError(
                "Cannot find scattering mechanism module at path {}".format(mechpath)
            )
        # code to import scattering mechanism generator as a module.
        spec = importlib.util.spec_from_file_location("", mechpath)
        mechmodule = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mechmodule)
        # mechanism takes this simulation class instance (self) as argument
        mech = getattr(mechmodule, classname)(self)
        # call mechanism. Can be a Class or a function that returns class instances.
        mech_iter = mech if isinstance(mech, list) else [mech]
        # add class instance(s) to list of mechanisms
        self.mechs.extend(mech_iter)
        self.scattering_mech_filenames.extend(
            filename if isinstance(filename, list) else [filename]
        )
        self.scattering_mech_classnames.extend(
            classname if isinstance(classname, list) else [classname]
        )

    def _generate_scattering_times(self, n=None):
        """Generate initial times until scattering using the total scattering rate, gamma.

        :param n: length of array, defaults to self.n_particles
        :type n: int, optional
        :return: a length n numpy array containing randomly generated scattering times.
        :rtype: float
        """
        if not n:
            n = self.n_particles
        return -np.log(self.rng.rand(n, 1)) / self.gamma

    def _build_param_arrays(self):
        """
        Build arrays for all parameters found in the material. Works by stacking the parameter array for each material on top of each other. Assumes that all materials have the same number of particle types, band valleys etc. so that the parameter arrays are the same shape.
        """
        for label in self.mats[0].labels:
            param_array = np.stack([getattr(m, label) for m in self.mats])
            setattr(self, label, param_array)

    def _setup_polarons(self):
        """
        If the input polarons is True, calculate polaron properties for use in scattering rates and energy calculations, and set polaron_mode to True to get correct scattering rates. If False (or missing), set polaron_mode to False and don't do anything.
        """
        self.polaron_mode = False
        if hasattr(self, "polarons"):
            if self.polarons:
                self.polaron_mode = True
                self.Pol = self.polaron.PolaronLib(self)
                self.Pol.initialise_polarons()

        if not self.polaron_mode:
            # Create (unused but required) masspol material object and set to zero.
            polaron.set_null_masspol(self)

    def get_parameter_per_particle(self, param, init_mode=False):
        """
        Return a 1D numpy array of length N (number of particles), where element i is the material parameter for particle i, given its material and/or particle type and/or valley.

        Assumes that material parameters are indexed in the following order: (material, particle type, valley, valley). Indexing can stop at any point; for example a parameter can be defined for only (material, particle type).

        :param param: an attribute of a Simulation class instance that is specified for a particular material, particle type, initial band valley and final band valley, or any subset of that list provided it starts with material and follows the same order. For example, if ``sim`` is a Simulation class instance, ``sim.mstar`` is a valid input.
        :type param: list
        :return: a 1D numpy array with length equal to the number of particles, where element i is the material parameter for particle i, given its material and/or particle type and/or valley.
        :rtype: ndarray
        """
        ndims = len(np.shape(param))
        dim_indices = []  # TODO: change to tuple, find alternative to append

        # This is the version that can be used without a PPMD State, which is normally used to get particle indexes. Instead, the arrays that define the initial particle properties are used.
        if ndims <= 0:
            raise RuntimeError(
                "Parameter {} has ndims = {} which is invalid. Must be > 0.".format(
                    param, ndims
                )
            )
        if init_mode:
            dim_indices.append(self.mat_init[:, 0])
            if ndims > 1:
                dim_indices.append(self.ptype_init[:, 0])
                if ndims > 2:
                    dim_indices.append(self.val_init[:, 0])
                    if ndims > 3:
                        dim_indices.append(self.val_init[:, 0])
                        if ndims > 4:
                            raise RuntimeError(
                                "Parameter {} has ndims = {} which is invalid. Must be < 4.".format(
                                    param, ndims
                                )
                            )
        else:
            dim_indices.append(self.State.mat.view[:, 0])
            if ndims > 1:
                dim_indices.append(self.State.ptype.view[:, 0])
                if ndims > 2:
                    dim_indices.append(self.State.val.view[:, 0])
                    if ndims > 3:
                        dim_indices.append(self.State.val.view[:, 0])
                        if ndims > 4:
                            raise RuntimeError(
                                "Parameter {} has ndims = {} which is invalid. Must be < 4.".format(
                                    param, ndims
                                )
                            )
        return param[tuple(dim_indices)]

    def _setup_gsl_rng(self):
        """
        Set up self.rng module, then adds the rng module to the ppmd dat_dict.
        """
        self.gsl_rng_module = kernel_gsl_rng.PerThreadGSLRNG(self.gslrngseed)
        if hasattr(self, "dat_dict"):
            self.dat_dict.update(self.gsl_rng_module.get_dat_dict())
        else:
            raise RuntimeError(
                "Trying to set up GSL rng module but cannot find a dat_dict to update."
            )

    def _setup_event_tracker(self):
        """
        Set up an array that tracks how many events of each type occured during the simulation. Or make it an empty array if tracking isn't required.

        Tracks events as a function of material, particle type, band valley, energy, final polar scattering angle and scattering mechanism.
        """
        if self.track_events:
            if (
                hasattr(self, "tracking_energy_max")
                or hasattr(self, "tracking_energy_step")
                or hasattr(self, "tracking_energy_n")
            ):
                # User has specified a different energy scale for tracking
                self.tracking_bins = q * self.get_arange_from_inputs(
                    "tracking_energy_max", "tracking_energy_step", "tracking_energy_n"
                )
            else:
                self.tracking_bins = self.energies
                self.tracking_energy_step = self.rate_table_energy_step
                self.tracking_energy_n = self.rate_table_energy_n
                self.tracking_energy_max = self.rate_table_energy_max

            self.tracker_shape = (
                self.n_mats,
                self.max_ptypes,
                self.max_valleys,
                self.tracking_energy_n,
                self.tracking_n_cpol,
                len(self.smh.scatter_funcs),
            )
            blank_event_tracker = np.zeros(self.tracker_shape)
            self.event_tracker_flat = blank_event_tracker.flatten()

            tracker_len = len(self.event_tracker_flat)
            self.State.EVENT_TRACKER = data.GlobalArray(
                ncomp=tracker_len, dtype=c_int64
            )

        else:
            self.tracking_bins = self.energies
            self.tracking_energy_step = self.rate_table_energy_step
            self.tracking_energy_n = self.rate_table_energy_n
            self.tracking_energy_max = self.rate_table_energy_max
            self.State.EVENT_TRACKER = data.GlobalArray(ncomp=1, dtype=c_int64)

    def kinetic_energy_band(self, ks, mstars, alphas):
        """
        Calculate the kinetic energy of a list of particles given particle wavevectors, masses and Kane parameters. Uses the Kane model for non-parabolicity, which reduces to the parabolic approximation when alpha = 0.
        """
        ksquared = np.square(np.linalg.norm(ks, axis=1))
        gammak = hbar ** 2 * ksquared / (2 * mstars)
        discrim = np.sqrt(1 + 4 * alphas * gammak)
        return 2 * gammak / (1 + discrim)

    def kinetic_energy_polaron(self, ks, masspols, alphas):
        """
        Calculate the kinetic energy of a list of particles given particle wavevectors, masses and Kane parameters. Uses the Kane model for non-parabolicity, which reduces to the parabolic approximation when alpha = 0.
        """
        ksquared = np.square(np.linalg.norm(ks, axis=1))
        gammak = hbar ** 2 * ksquared / (2 * masspols)
        discrim = np.sqrt(1 + 4 * alphas * gammak)
        return 2 * gammak / (1 + discrim)

    def calculate_kinetic_energies(self):
        """
        Return the kinetic energy (in SI) of every particle in the simulation, not including potential energy sources.
        """
        ks = self.State.k.view
        mstars = self.get_parameter_per_particle(self.mstar)
        if self.polaron_mode:
            masspols = self.get_parameter_per_particle(self.masspol)
        alphas = self.get_parameter_per_particle(self.alpha)
        if self.polaron_mode:
            kinetic_energy = self.kinetic_energy_polaron(ks, masspols, alphas)
        else:
            kinetic_energy = self.kinetic_energy_band(ks, mstars, alphas)
        return kinetic_energy

    def calculate_energies(self):
        """
        Return the energy (in SI) of every particle in the simulation, including kinetic (Kane model) and potential (valley minima).
        """
        ks = self.State.k.view
        mstars = self.get_parameter_per_particle(self.mstar)
        if self.polaron_mode:
            masspols = self.get_parameter_per_particle(self.masspol)
        alphas = self.get_parameter_per_particle(self.alpha)
        if self.polaron_mode:
            kinetic_energy = self.kinetic_energy_polaron(ks, masspols, alphas)
        else:
            kinetic_energy = self.kinetic_energy_band(ks, mstars, alphas)
        valmins = self.get_parameter_per_particle(self.valmin)
        return kinetic_energy + valmins

    def _setup_ppmd_state(self):
        """
        Initialise PPMD state, domain and ParticleDats / GlobalArrays / ScalarArrays
        """
        # Set up PPMD state
        self.State = state.State()
        self.State.domain = domain.BaseDomainHalo(extent=(1e-9, 1e-9, 1e-9))
        self.State.domain.boundary_condition = domain.BoundaryTypePeriodic()

        # Intialise ParticleDats / PositionDats
        self.State.pos = data.PositionDat()  # Positions
        self.State.k = data.ParticleDat(ncomp=3, dtype=c_double)  # Momenta
        self.State.id = data.ParticleDat(ncomp=1, dtype=c_int64)  # Particle indexes
        self.State.mat = data.ParticleDat(ncomp=1, dtype=c_int64)  # Particle materials
        self.State.ptype = data.ParticleDat(ncomp=1, dtype=c_int64)  # Particle types
        self.State.val = data.ParticleDat(ncomp=1, dtype=c_int64)  # Valleys
        self.State.charge = data.ParticleDat(
            ncomp=1, dtype=c_double
        )  # Particle charges (in units of q)
        self.State.ts = data.ParticleDat(ncomp=1, dtype=c_double)  # Scattering times

        # Initialise GlobalArrays
        self.State.err = data.GlobalArray(
            ncomp=1, dtype=c_int64
        )  # Error handler. If not 0 then there's a problem

        # Initialise ScalarArrays
        self.State.SCREENLEN = data.ScalarArray(ncomp=1, dtype=c_double)
        self.State.SCREENLEN_MIN = data.ScalarArray(ncomp=1, dtype=c_double)

    def _get_initial_state_parameter(self, input_param, dict_key=None):
        """
        Get a value per particle, either by giving every particle the same integer value or by reading an array from a .yaml file. Works by looking for the value of ``input_param``, which should normally be an attribute of the Simulation class instance that comes from the input file. The value of ``input_param`` should either be of type ``str`` or ``int``.

        If the value of ``input_param`` is of type ``str``, it is interpreted as a path to a .yaml file containing an entry with a key equal to ``dict_key``. The value of that entry should be a list of length ``n_particles``. The function will return an N x 1 array containing these values, preserving order so that the ith value is attributed to the particle with index i.

        If the value of ``input_param`` is of type ``int``, it is interpreted as a single value that should be broadcasted across all particles i.e. all particles will have the same attribute. The function will return an N x 1 array with each element having this value.

        This function is currently used in BoltMC for initial materials, particle types, band valleys and charges.
        """
        # if entry is a string, assume it's the name of the state file that contains the correct array
        if isinstance(input_param, str):
            with open(input_param) as f:
                fdata = yaml.safe_load(f)
            if not dict_key:
                raise RuntimeError("dict_key must not be None")
            ptypes = np.array([[pt] for pt in fdata[dict_key]])
        # if entry is an integer, assume it's the value to be given to every particle
        elif isinstance(input_param, int):
            ptypes = np.full((self.n_particles, 1), input_param)
        # function only supports str and int right now, else error
        else:
            raise RuntimeError(
                "{} should be either a str to a yaml file or a single integer.".format(
                    dict_key
                )
            )
        return ptypes

    def _set_initial_state(self):
        """
        Initialise values of particle properties according to inputs, or some other function. Then assign them to the PPMD state.
        """
        self.id_init = np.arange(self.n_particles).reshape((self.n_particles, 1))
        self.mat_init = self._get_initial_state_parameter(self.init_mats, "init_mats")
        self.ptype_init = self._get_initial_state_parameter(
            self.init_ptypes, "init_ptypes"
        )
        self.val_init = self._get_initial_state_parameter(
            self.init_valleys, "init_valleys"
        )
        self.charge_init = self._get_initial_state_parameter(
            self.init_charges, "init_charges"
        )
        self.ts_init = self._generate_scattering_times()
        self.pos_init = self._get_initial_positions()
        self.k_init = self._get_initial_ks()

        # Assign all particle data
        with self.State.modify() as m:
            m.add(
                {
                    self.State.id: self.id_init,
                    self.State.mat: self.mat_init,
                    self.State.ptype: self.ptype_init,
                    self.State.val: self.val_init,
                    self.State.charge: self.charge_init,
                    self.State.ts: self.ts_init,
                    self.State.pos: self.pos_init,
                    self.State.k: self.k_init,
                }
            )

    def _get_initial_positions(self):
        """
        Get an N x 3 numpy array of particle positions, specified by the function self.initial_position_func.

        Defaults to an N x 3 array of zeros.
        """
        if not hasattr(self, "initial_positions"):
            return geometries.zero_positions(self.n_particles)

        if self.initial_positions == "random":
            return geometries.random_positions(
                self.n_particles, (1.0, 1.0, 1.0), self.rng
            )
        elif self.initial_positions == "zeros":
            return geometries.zero_positions(self.n_particles)

    def _make_all_k(self):
        """
        Set up an array that contains all particle momenta. Useful for carrier-carrier scattering.

        Spec: N x 3 array. Element 3n + x gives particle n's momentum in dimension x (x = 0, 1, 2)
        """
        allk_flatlen = self.n_particles * 3
        self.State.ALL_K = data.ScalarArray(ncomp=allk_flatlen, dtype=c_double)

    def _make_all_pt(self):
        """
        Set up an array that contains all particle types. Useful for carrier-carrier scattering.

        Spec: N x 1 array. Element n gives particle n's particle type index.
        """
        self.State.ALL_PT = data.ScalarArray(ncomp=self.n_particles, dtype=c_double)

    def _update_all_k(self):
        """
        Add all particle momenta to ALL_K.
        """
        ks_flat = self.State.k.view.flatten()
        self.State.ALL_K[:] = ks_flat

    def _update_all_pt(self):
        """
        Add all particle types to ALL_PT.
        """
        pt_flat = self.State.ptype.view.flatten()
        self.State.ALL_PT[:] = pt_flat

    def _set_timesteps(self):
        """
        Generate a numpy array of timesteps for the simulation to loop over. The array is determined primarily by the input timestep_mode.

        if 'timestep_mode' == 'fixed':
            - if 'max_sim_time' and 'timestep' are specified, the timescale will increment in steps of timestep until the time equals max_sim_time.
            - if 'max_sim_time' and 'n_timesteps' are specified but 'rate_table_energy_step' is not, the timescale will have n_timesteps equal intervals between 0 and max_sim_time.
            - if 'max_sim_time' is not specified but 'n_timesteps' and 'timestep' are specified, the timescale will increment in steps of timestep until n_timesteps energies have been generated.

        if 'timestep_mode' == 'gamma':
            - if 'max_sim_time_gamma_ratio' and 'timestep_gamma_ratio' are specified, the timescale will increment in steps of timestep_gamma_ratio divided by gamma until the time equals max_sim_time_gamma_ratio divided by gamma, where gamma is the maximum scattering rate found in the rate table.
            - if 'max_sim_time_gamma_ratio' and 'n_timesteps' are specified but 'rate_table_energy_step' is not, the timescale will have n_timesteps equal intervals between 0 and max_sim_time_gamma_ratio divided by gamma.
            - if 'max_sim_time' is not specified but 'n_timesteps' and 'timestep_gamma_ratio' are specified, the timescale will increment in steps of timestep_gamma_ratio divided by gamma until n_timesteps times have been generated.

        """
        if not hasattr(self, "timestep_mode"):
            raise RuntimeError(
                "Must specify timestep_mode. Options are 'fixed' or 'gamma'."
            )

        if self.timestep_mode == "fixed":
            self.timesteps = self.get_arange_from_inputs(
                "max_sim_time", "timestep", "n_timesteps"
            )

        elif self.timestep_mode == "gamma":
            if hasattr(self, "max_sim_time_gamma_ratio"):
                self.max_sim_time = self.max_sim_time_gamma_ratio / self.gamma
            if hasattr(self, "timestep_gamma_ratio"):
                self.timestep = self.timestep_gamma_ratio / self.gamma

            self.timesteps = self.get_arange_from_inputs(
                "max_sim_time", "timestep", "n_timesteps"
            )
            if not hasattr(self, "timestep"):
                self.timestep = self.timesteps[1] - self.timesteps[0]

        else:
            raise RuntimeError(
                "Timesteps were not set due to unknown value of timestep_mode: {}".format(
                    self.timestep_mode
                )
            )

    def _set_saveids(self):
        """
        Generate a numpy array of saveids, used by save_check() in most recorder functions to determine when to save data. Also creates an array of the savetimes themselves.

        Relevant inputs:
        - saveid_step: the number of timesteps between data saves (default 1)
        - saveid_max: the last timestep to record data (defaults to last timestep)
        - saveid_min: the first timestep index to start recording (default 0)

        """
        if not hasattr(self, "timesteps"):
            raise RuntimeError(
                "Simulation must have timesteps before saveids are assigned."
            )
        if not hasattr(self, "saveid_step"):
            self.saveid_step = 1
        if not hasattr(self, "saveid_min"):
            self.saveid_min = 0
        if not hasattr(self, "saveid_max"):
            self.saveid_max = len(self.timesteps)

        self.saveids = np.arange(self.saveid_min, self.saveid_max, self.saveid_step)
        self.savetimes = self.timesteps[
            self.saveids
        ]  # this array should not be used in simulation, but useful for plotting later

    def _set_energy_rate_table(self):
        """
        Generate the energy scale used to calculate scattering rates (in J). Two of three of rate_table_energy_max, rate_table_energy_step and rate_table_energy_n must be specified to generate the scattering rate table:

        - if 'rate_table_energy_max' and 'rate_table_energy_step' are specified, the energy scale will increment in steps of rate_table_energy_step until the energy equals rate_table_energy_max.
        - if 'rate_table_energy_max' and 'rate_table_energy_n' are specified but 'rate_table_energy_step' is not, the energy scale will have rate_table_energy_n equal intervals between 0 and rate_table_energy_max.
        - if 'rate_table_energy_max' is not specified but 'rate_table_energy_n' and 'rate_table_energy_step' are specified, the energy scale will increment in steps of rate_table_energy_step until rate_table_energy_n energies have been generated.

        """

        self.energies = q * self.get_arange_from_inputs(
            "rate_table_energy_max", "rate_table_energy_step", "rate_table_energy_n"
        )

    def get_arange_from_inputs(self, max_inp, step_inp, num_inp):
        """
        Return a numpy arange array, given strings that represent the inputs relating to the maximum value, the step value and the number of steps. A heirarchy of inputs is used in case all three are specified, although a warning is given in this case.

        If one of the three inputs is missing, or if all three are specified, the missing/extra attribute will be determined self-consistently using the other two.
        """
        if (
            hasattr(self, max_inp)
            and hasattr(self, step_inp)
            and hasattr(self, num_inp)
        ):
            warnings.warn(
                "Array has been overspecified (all three of {max_inp}, {step_inp} and {num_inp} are in input file). Array will be generated using {max_inp} and {step_inp}, and {num_inp} will be overwritten for consistency. This may not be the desired effect.".format(
                    max_inp=max_inp, step_inp=step_inp, num_inp=num_inp
                )
            )

        # Get variables if they exist
        if hasattr(self, max_inp):
            max_val = getattr(self, max_inp)
        if hasattr(self, num_inp):
            num_val = getattr(self, num_inp)
        if hasattr(self, step_inp):
            step_val = getattr(self, step_inp)

        # Fill in the rest if the other two have been specified
        if hasattr(self, max_inp) and hasattr(self, step_inp):
            num_val = int(max_val / step_val)
            setattr(self, num_inp, num_val)
        elif hasattr(self, max_inp) and hasattr(self, num_inp):
            step_val = max_val / num_val
            setattr(self, step_inp, step_val)
        elif hasattr(self, num_inp) and hasattr(self, step_inp):
            max_val = step_val * num_val
            setattr(self, max_inp, max_val)
        else:
            raise RuntimeError(
                "Two of three of {max_inp}, {step_inp} and {num_inp} must be specified to generate the desired array.".format(
                    max_inp=max_inp, step_inp=step_inp, num_inp=num_inp
                )
            )

        return np.arange(0, max_val, step_val)
