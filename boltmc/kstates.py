import numpy as np
import yaml
from boltmc.utilities.boltmc_constants import *


def _band_ks_from_energy(self, en, mstars=None, alphas=None):
    """
    Return N by 3 momentum vectors, given a set of energies (en). The energy input can be a numpy array of length n_particles, or a single value. Assumes a completely isotropic distribution of momentum vectors.
    """
    modks = np.sqrt(2 * mstars * en * (1 + alphas * en)) / hbar
    return isotropic_ks(modks, self.rng)


def _polaron_ks_from_energy(self, en, masspols=None, alphas=None):
    """
    Return N by 3 momentum vectors, given a set of energies (en). The energy input can be a numpy array of length n_particles, or a single value. Assumes a completely isotropic distribution of momentum vectors.
    """
    modks = np.sqrt(2 * masspols * en * (1 + alphas * en)) / hbar
    return isotropic_ks(modks, self.rng)


def isotropic_ks(modks, rng):
    """
    Return N by 3 momenum vectors, given a list of
    """
    len_k = len(modks)  # should equal number of particles
    cpols = 1 - 2 * rng.uniform(0, 1, len_k)
    azis = rng.uniform(0, 2 * np.pi, len_k)
    spols = np.sqrt(1 - np.square(cpols))

    kx = modks * spols * np.cos(azis)
    ky = modks * spols * np.sin(azis)
    kz = modks * cpols
    return np.stack((kx, ky, kz), axis=-1)


def _get_maxwell_boltzmann_ks(
    self, temperature=None, ke_total=None, mstars=None, masspols=None, alphas=None
):
    """
    Return three dimensional wavevector components with a standard deviation according to Maxwell-Boltzmann distribution.
    """
    gamma_shape = 3 / 2

    if mstars is None:
        mstars = self.get_parameter_per_particle(self.mstar)
    if alphas is None:
        alphas = self.get_parameter_per_particle(self.alpha)
    if self.polaron_mode:
        if masspols is None:
            masspols = self.get_parameter_per_particle(self.masspol)
    if temperature is not None:
        ke_total = self.n_particles * 3 / 2 * k_B * temperature

    unnorm_energies = self.rng.gamma(gamma_shape, 1, (self.n_particles, 1))
    energies = ke_total / np.sum(unnorm_energies) * unnorm_energies
    en_flat = energies[:, 0]
    if self.polaron_mode:
        ks_from_en = self._polaron_ks_from_energy(en_flat, masspols, alphas)
    else:
        ks_from_en = self._band_ks_from_energy(en_flat, mstars, alphas)
    return ks_from_en


def _get_initial_maxwell_boltzmann_ks(self):
    """
    Give all particles an initial three dimensional wavevector components with a standard deviation according to Maxwell-Boltzmann distribution. Assumes that there is no ppmd state yet, so doesn't use self.get_parameter_per_particle to get effective masses.
    """
    # There's no ppmd state yet, so must use init_mode
    mstars = self.get_parameter_per_particle(self.mstar, init_mode=True)
    alphas = self.get_parameter_per_particle(self.alpha, init_mode=True)
    if hasattr(self, "initial_temperature"):
        mb_temp = self.initial_temperature
    else:
        mb_temp = self.lattice_temperature
    if self.polaron_mode:
        masspols = self.get_parameter_per_particle(self.masspol, init_mode=True)
        ks = self._get_maxwell_boltzmann_ks(
            mb_temp, mstars=mstars, masspols=masspols, alphas=alphas
        )
    else:
        masspols = None
        ks = self._get_maxwell_boltzmann_ks(
            mb_temp, mstars=mstars, masspols=masspols, alphas=alphas
        )
    return ks


def _get_initial_delta_ks(self):
    """
    Return a set of N by 3 momentum vectors that all have the same energy. The energy is set by the attribute initial_delta_energy (in electron volts, which is converted to J automatically).
    """
    # There's no ppmd state yet, so must use init_mode
    mstars = self.get_parameter_per_particle(self.mstar, init_mode=True)
    alphas = self.get_parameter_per_particle(self.alpha, init_mode=True)

    delta_energy = self.initial_delta_energy * q
    return self._ks_from_energy(delta_energy, mstars, alphas)


def _get_gaussian_ks(self, central_energy, width, init_mode=False, n=None):
    """
    Return a set of N by 3 momentum vectors that have a Gaussian energy distribution, with FWHM equal to `width` and mean energy equal to `central_energy` (default 0).
    """
    if not n:
        n = self.n_particles
    mstars = self.get_parameter_per_particle(self.mstar, init_mode=init_mode)
    alphas = self.get_parameter_per_particle(self.alpha, init_mode=init_mode)

    # get energies:
    gaussian_energies = self.rng.normal(loc=central_energy, scale=width, size=n)
    if np.all(gaussian_energies < 0):
        raise RuntimeError("All energies are less than zero.")
    # make sure they're all > 0:
    while len(gaussian_energies[gaussian_energies < 0]) != 0:
        gaussian_energies[gaussian_energies < 0] = self.rng.normal(
            loc=central_energy,
            scale=width,
            size=len(gaussian_energies[gaussian_energies < 0]),
        )
    return self._ks_from_energy(gaussian_energies, mstars=mstars, alphas=alphas)


def _get_ks_from_file(self):
    """Return a set of N by 3 momentum vectors from an input file.

    The input file should be in a YAML format, and the k-vectors should be an N by 3 list of floats in SI units. This function is executed if "initial_distribution" is set to "from_file". The file is specified by the input "kfile" and the key that the k-vectors are saved under is set by the input "kkey", or defaults to "wavevectors".

    :return: A numpy array of shape self.n_particles by 3 containing wavevector components.
    :rtype: ndarray
    """

    if not hasattr(self, "kfile"):
        raise RuntimeError("kfile must be specified")
    if not hasattr(self, "kkey"):
        self.kkey = "wavevectors"

    with open(self.kfile) as f:
        kfiledict = yaml.safe_load(f)
    if self.kkey not in kfiledict:
        raise RuntimeError(
            "'{}' key not found in k input file {}".format(self.kkey, self.kfile)
        )
    ks = np.array(kfiledict[self.kkey])

    expected_shape = (self.n_particles, 3)
    if np.shape(ks) != expected_shape:
        raise RuntimeError(
            "input ks should have shape {}, but has shape {}.".format(
                expected_shape, np.shape(ks)
            )
        )
    return ks


def _get_initial_ks(self):
    """
    Return a set of N by 3 momentum vectors, with a distribution set according to the input 'initial_distribution'. The options are:

    - 'thermal' (Default): A distribution following Maxwell-Boltzmann statistics
        (at the lattice temperature, unless another is set by 'initial_temperature')
    - 'delta': All particles at a certain energy specified by 'initial_delta_energy' (in eV),
        with corresponding momentum vectors isotropically distributed.
    - 'gaussian': A Gaussian (normal) distribution, with a mean and width specified by 'initial_gaussian_centre' and 'initial_gaussian_width' (both in eV).
    """
    if hasattr(self, "initial_distribution"):
        if self.initial_distribution == "thermal":
            ks = self._get_initial_maxwell_boltzmann_ks()
        elif self.initial_distribution == "delta":
            ks = self._get_initial_delta_ks()
        elif self.initial_distribution == "gaussian":
            if hasattr(self, "initial_gaussian_centre"):
                central_energy = q * self.initial_gaussian_centre
            else:
                raise RuntimeError(
                    "Must specify a value of initial_gaussian_centre, the mean initial energy (in eV)."
                )
            if hasattr(self, "initial_gaussian_width"):
                width = q * self.initial_gaussian_width
            else:
                raise RuntimeError(
                    "Must specify a value of initial_gaussian_width, the width of the initial Gaussian energy distribution (in eV)."
                )
            ks = self._get_gaussian_ks(central_energy, width, init_mode=True)
        elif self.initial_distribution == "from_file":
            ks = self._get_ks_from_file()
        else:
            raise RuntimeError(
                "Unexpected value of initial_distribution received: {}".format(
                    self.initial_distribution
                )
            )
    else:
        # Default to thermal / Maxwell-Boltzmann distribution
        ks = self._get_initial_maxwell_boltzmann_ks()
    return ks


def set_maxwell_boltzmann_ks(self, temperature=None, ke_total=None):
    """Give all particles new three dimensional wavevector components according to a Maxwell-Boltzmann distribution.

    The shape of the M-B distribution can be described by one (and only one) of the following:
    - a temperature; if this is calculated from the particle dsitribution then conservation of energy should be conserved.
    - a total kinetic energy, in which case conservation of energy is enforced

    :param temperature: a temperature (in Kelvin), defaults to None
    :type temperature: float, optional
    :param ke_total: a total kinetic energy (in J) of the system, summed across all particles. Defaults to None.
    :type ke_total: float, optional
    """
    ks = self._get_maxwell_boltzmann_ks(temperature=temperature, ke_total=ke_total)
    with self.State.k.modify_view() as m:
        m[:, :] = ks
