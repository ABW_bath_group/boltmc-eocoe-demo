import yaml
import os


def _load_inputs(self, filename, extra_params):
    """Reads an input .yaml file and saves parameters to the Simulation class instance.

    Contains functionality for overriding or extending inputs from the .yaml file by specifying an extra_params dict.

    :param filename: path to the input file. Must end in .yaml.
    :type filename: str
    :param extra_params: a dict of extra parameters. If any key is already an input from the input file, the value from extra_params overwrites it.
    :type extra_params: dict
    """
    if not os.path.exists(filename):
        raise RuntimeError("Cannot find input file {}.".format(filename))

    # Get inputs from YAML file as dict
    with open(filename) as f:
        self.inputs = yaml.safe_load(f)

    # If custom inputs have been specified using the params argument in __init__, add to or overwrite the values that were read from input file.
    if extra_params:
        self.inputs.update(extra_params)

    # Set all input parameters as attributes of the Simulation class.
    for name, value in self.inputs.items():
        setattr(self, name, value)

    # Save inputs to a yaml file
    if self.inputs["save_inputs"]:
        _save_inputs(self)

    if not self.inputs["silent"]:
        print("Loaded input file from {}".format(filename))


def _save_inputs(self):
    """
    Save input params to a .yaml file for safekeeping.
    """
    sfile = self.save_file if "save_file" in self.inputs else "saved_inputs.yaml"
    with open(sfile, "w") as f:
        yaml.dump(self.inputs, f)