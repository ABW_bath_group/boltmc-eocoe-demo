"""
Python and C implementations of Ei^{-1}(x).
"""

from scipy.special import expi
from math import exp


def inverse_Ei(y, tol=10.0 ** -13):
    """
    Use Newton's method to compute x such that abs(Ei(x) - y) < tol.
    """

    x = 2 ** (-1022)

    ymin = expi(x)
    if y < ymin:
        raise RuntimeError("Input y value outside accepted range.")

    # better starting value for larger y values.
    # could be improved with some binning for initial values
    if y > 0.455:
        x = 0.5

    for ix in range(100000):
        residual = expi(x) - y
        if abs(residual) < tol:
            return x

        # x = x * (1.0 - (expi(x) - y) * exp(-1.0 * x))
        x = x * (1.0 - residual * exp(-1.0 * x))

    raise RuntimeError(
        "Maximum number of iterations reached: abs(residual)={}, x={}, y={}.".format(
            abs(residual), x, y
        )
    )


INVERSE_EI_IMPLEMENTATION = r"""
#include <float.h>
#include <gsl/gsl_sf.h>
#include <stdio.h>
#include <math.h>

#define Ei gsl_sf_expint_Ei
#define RABS(x) (((x) > 0) ? (x) : (-1.0 * (x)))
#define INVERSE_EI_DEFAULT_TOL 1E-13


static inline int inverse_Ei(
    const double y,
    const double tol,
    double * x
){

    double xn = DBL_MIN;
    const double ymin = Ei(xn);

    if (y < ymin) {
        printf("inverse_Ei domain error: input y too low a value.\n");
        return 1;
    }
    if (y > 0.455) {
        xn = 0.5;
    }

    double residual;
    for(int ix=0 ; ix<100000 ; ix++){

        residual = Ei(xn) - y;

        if (RABS(residual) < tol){
            *x = xn;
            return 0;
        } else {
            xn = xn * (1.0 - residual * exp(-1.0 * xn));
        }

    }

    printf(
        "(inverse_Ei) Maximum number of iterations reached: abs(residual)=%f, x=%f, y=%f.\n",
        RABS(residual), xn, y
    );
    return 1;
}
"""
"""C implementation of inverse Ei. Should be placed in a header file."""
