import numpy as np
import math
from boltmc.utilities.boltmc_constants import *
from boltmc.material import Material


def mobility(temperature, material_file):
    """
    The charge carrier mobility under acoustic phonon scattering. Takes as input a temperature (in Kelvin); if a numpy array is passed, a numpy array of equal length will be returned.

    Taken from equation 11.46 of Jacoboni's textbook 'Theory of Electron Transport in Semiconductors: A Pathway from Elementary Physics to Nonequilibrium Green Functions'
    """
    max_ptypes = 3
    max_valleys = 3

    m = Material(max_ptypes, max_valleys, filename=material_file, verbose=False)
    return (
        q
        * 4
        * math.sqrt(pi)
        * hbar ** 4
        * m.el_const
        / (3 * math.sqrt(2) * m.mstar[0, 0] ** (5 / 2) * m.acodefpot[0] ** 2)
        * (k_B * temperature) ** -1.5
    )


if __name__ == "__main__":
    temps = np.geomspace(1, 1000, 100)
    matfile = "/home/li219/boltmc_space/boltmc/boltmc/materials/mapbi3_05.yaml"
    mob = mobility(temps, matfile)
    print(mob)
