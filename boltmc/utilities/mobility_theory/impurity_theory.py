import numpy as np
import math
from boltmc.utilities.boltmc_constants import *
from boltmc.material import Material

def mobility(temperature, material_file):
    """
    The charge carrier mobility under ionised impurity scattering. Takes as input a temperature (in Kelvin); if a numpy array is passed, a numpy array of equal length will be returned.

    Taken from equation 11.49 of Jacoboni's textbook 'Theory of Electron Transport in Semiconductors: A Pathway from Elementary Physics to Nonequilibrium Green Functions'
    """
    max_ptypes = 3
    max_valleys = 3
    m = Material(max_ptypes, max_valleys, filename=material_file, verbose=False)
    qo = np.sqrt(q ** 2 * m.impdens / (m.eps_s * k_B * temperature))
    eo = hbar ** 2 * qo ** 2 / (2 * m.mstar[0, 0])
    fbh = np.log(1 + 12 * k_B * temperature / eo) - 12 * k_B * temperature / eo / (
        1 + 12 * k_B * temperature / eo
    )
    return (
        64
        / math.sqrt(m.mstar[0, 0])
        * math.sqrt(pi)
        * m.eps_s ** 2
        / (m.impdens * q ** 3 * fbh)
        * (2 * k_B * temperature) ** (1.5)
    )


def mobility_transfer_report(temperature, material_file):
    max_ptypes = 1
    max_valleys = 1
    m = Material(max_ptypes, max_valleys, filename=material_file, verbose=False)
    return (
        128
        * math.sqrt(2 * pi)
        * m.eps_s ** 2
        * (k_B * temperature) ** 1.5
        / (q ** 3 * math.sqrt(m.mstar[0, 0]) * m.impdens)
        / np.log(
            24
            * m.mstar[0, 0]
            * m.eps_s
            * (k_B * temperature) ** 2
            / (q ** 2 * hbar ** 2 * m.impdens)
        )
    )
