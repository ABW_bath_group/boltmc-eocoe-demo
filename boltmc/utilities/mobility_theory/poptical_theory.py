import numpy as np
import math
from scipy.special import k1
from boltmc.utilities.boltmc_constants import *
from boltmc.material import Material


def mobility(temperature, material_file, custom_params=None):
    """
    The charge carrier mobility under polar optical phonon scattering. Takes as input a temperature (in Kelvin); if a numpy array is passed, a numpy array of equal length will be returned.

    Taken from equation 11.49 of Jacoboni's textbook 'Theory of Electron Transport in Semiconductors: A Pathway from Elementary Physics to Nonequilibrium Green Functions'
    """
    max_ptypes = 3
    max_valleys = 3
    m = Material(max_ptypes, max_valleys, filename=material_file, verbose=False)

    if custom_params:
        for name, value in custom_params.items():
            setattr(m, name, value)

    eo = (
        m.mstar[0, 0]
        * q
        * m.hwo[0]
        / (4 * pi * hbar ** 2)
        * (1 / m.eps_inf - 1 / m.eps_s)
    )
    xo = m.hwo[0] / (k_B * temperature)
    n_ph = 1 / (np.exp(m.hwo[0] / (k_B * temperature)) - 1)
    return (
        3
        * math.sqrt(2 * pi * m.hwo[0])
        / (
            4
            * m.mstar[0, 0] ** 0.5
            * eo
            * n_ph
            * xo ** 1.5
            * np.exp(xo / 2)
            * k1(xo / 2)
        )
    )


def mob_transfer_report(temperature, material_file, zlimit):
    """
    poptical mobility as found in my transfer report.
    """
    max_ptypes = 3
    max_valleys = 3
    m = Material(max_ptypes, max_valleys, filename=material_file, verbose=False)

    z = m.hwo[0] / (k_B * temperature)
    if zlimit == "high":
        chi_z = 3 / 8 * np.sqrt(pi * z)
    elif zlimit == "low":
        chi_z = 1
    else:
        raise RuntimeError(
            "zlimit must be either high or low, you entered {}.".format(zlimit)
        )
    return (
        4
        * pi
        * 2 ** (5 / 2)
        * hbar ** 2
        / (3 * q * m.mstar[0, 0] ** (3 / 2) * m.hwo[0] ** 0.5)
        * 1
        / (1 / m.eps_inf - 1 / m.eps_s)
        * chi_z
        * (np.exp(z) - 1)
        / np.sqrt(z)
    )