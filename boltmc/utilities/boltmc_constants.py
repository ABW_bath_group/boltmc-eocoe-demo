from scipy import constants

pi = constants.pi
h = constants.h
hbar = constants.hbar
c = constants.speed_of_light
k_B = constants.Boltzmann
m_e = constants.electron_mass
eps_0 = constants.epsilon_0

q = constants.electron_volt
mev = 1e-3 * q

ns = 1e-9
ps = 1e-12
fs = 1e-15