import os
import numpy as np
import yaml
import itertools


def setup_boltmc(
    varnames,
    varlists,
    infile,
    varformat=int,
    foldernames=None,
    folder=None,
    extra_varnames=None,
    extra_varlists=None,
    extra_varformats=None,
    verbose=True,
):
    """
    A script that can be used to automatically create and set up folders that loop over a list of parameters in BoltMC's input file. Each folder will be nested and named after the corresponding parameters and values and will contain a copy of infile with the corresponding parameters changed.

    :arg varnames: (List of) string representing inputs to BoltMC, usually found in an input (.yaml) file.
    :arg varlists: (List of) list of values corresponding to each argument varname.
    :arg infile: Input file (.yaml) to use as the settings that the BoltMC simulation class will otherwise take. It may have an entry for varname, but it will be overwritten.
    :arg varformat: (List of) formatting function(s) (default float) that should be applied to each entry in varlist before being written to the .yaml file.
    :arg foldernames: String(s) to use when creating directory names. If None, the entry in the varlist will be used (determined by varformat and the entry value itself). If a list, should be the same length as varnames.
    :arg folder: Location of the folder that the newly created folders will be placed inside. If None (the default), the folders will be placed inside the current folder.
    :arg verbose: Whether to print the names of the created folders. Default True.
    """

    vpaths = []

    if not folder:
        folder = os.getcwd()

    iteridxs = [range(len(x)) for x in varlists]

    for idxs in itertools.product(*iteridxs):
        folders = []
        for vx, v in enumerate(idxs):
            name = varnames[vx]
            if foldernames and foldernames[vx] is not None:
                print(foldernames[vx][v])
                value = foldernames[vx][v]
            else:
                value = varformat[vx](varlists[vx][v])
            folders.append("{}_{}".format(name, value))

        vpath = os.path.join(folder, "/".join(folders))
        if verbose:
            print(vpath)
        os.makedirs(vpath, exist_ok=True)

        # Make correct parameter files
        with open(infile) as f:
            params = yaml.full_load(f)

        for vx, varname in enumerate(varnames):
            if varname not in params:
                raise RuntimeWarning("varname {} is not in params".format(varname))
            params.update({varname: varformat[vx](varlists[vx][idxs[vx]])})

            # Update any extra parameters
            if extra_varnames and extra_varnames[vx]:
                for evx, evarname in enumerate(extra_varnames[vx]):
                    if evarname not in params:
                        raise RuntimeWarning(
                            "varname {} is not in params".format(evarname)
                        )

                    params.update(
                        {
                            evarname: extra_varformats[vx][evx](
                                extra_varlists[vx][evx][idxs[vx]]
                            )
                        }
                    )

        with open(os.path.join(vpath, infile), "w") as f:
            yaml.dump(params, f)

        vpaths.append(vpath)

    if verbose:
        print("Total number of folders created: {}".format(len(vpaths)))
    return vpaths
