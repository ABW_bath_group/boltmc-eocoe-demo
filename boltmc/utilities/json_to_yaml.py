import json, yaml
import argparse
import os

parser = argparse.ArgumentParser(description="Process some filenames.")
parser.add_argument("infile", type=str, help="Input file to be converted")
parser.add_argument("outfile", type=str, help="Output file to be created")

args = parser.parse_args()


def json_to_yaml(infile, outfile):
    if not os.path.exists(infile):
        raise RuntimeError("can't find file {}".format(infile))

    with open(infile, "r") as f:
        contents = json.load(f)
    print(contents)
    with open(outfile, "w") as f:
        documents = yaml.dump(contents, f)


if __name__ == "__main__":
    json_to_yaml(args.infile, args.outfile)
