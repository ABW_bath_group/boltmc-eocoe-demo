import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.use("Agg")
from scipy import constants
from plotting import boltmc_plotter
from boltmc.utilities.setup_boltmc import setup_boltmc
from boltmc.utilities.mobility_theory import poptical_theory


def calc_autocorr(workfolder, t_idx_min, t_idx_max, t0_idx_min, t0_idx_max):
    simfiles = ["saved_inputs.yaml", "simulation.json", "vel_autocorr.npz"]
    simpaths = [os.path.join(workfolder, f) for f in simfiles]

    if not all(os.path.exists(f) for f in simpaths):
        print("files not in {}".format(workfolder))
        return

    p = boltmc_plotter.BoltMCPlotter(simpaths, verbose=False)

    txs = np.arange(t_idx_min, t_idx_max)
    t0xs = np.arange(t0_idx_min, t0_idx_max)

    p.autocorr = np.zeros(len(txs))

    for tx in txs:
        sum_t = sum(
            np.tensordot(
                p.all_velocities[t0x],
                p.all_velocities[tx + t0x],
                axes=([1, 0], [1, 0]),
            )
            for t0x in t0xs
        )


        sum_t /= p.n_particles * len(t0xs)
        p.autocorr[tx] = sum_t

    return p


def autocorrelation_mobility(folders, ax=None, **plot_kwargs):
    if not ax:
        save_fig = True
        fig, ax = plt.subplots()
    else:
        save_fig = False

    ps = []
    autocorrs = []
    for workfolder in folders:
        print(workfolder)
        ps.append(calc_autocorr(workfolder, 0, 500, 0, 400))
        autocorrs.append(ps[-1].autocorr)

        p = ps[-1]

        p.int_autocorr = p.saveid_step * p.timestep * np.sum(p.autocorr)
        p.mob_int = (
            constants.electron_volt
            / (constants.Boltzmann * p.lattice_temperature)
            * p.int_autocorr
        )
        np.savetxt(
            os.path.join(workfolder, "mobility.dat"), [p.lattice_temperature, p.mob_int]
        )

        ax.plot(p.lattice_temperature, p.mob_int, marker=".", color="red")

    if save_fig:
        ax.set_yscale("log")
        ax.set_xscale("log")
        ax.set_ylabel("Mobility (m$^2$V$^{-1}$s$^{-1}$)")
        ax.set_xlabel("Temperature (K)")
        fig.savefig("autocorrelation_mobilities.png", dpi=1000)


def setup_autocorrelation_mobility(
    temperatures,
    material_file=None,
    input_file="inputs.yaml",
    workfolder=None,
):
    temperatures = np.array(temperatures)
    if workfolder is None:
        workfolder = os.getcwd()

    expected_pop_mobs = poptical_theory.mobility(
        temperatures, "/home/li219/boltmc_space/boltmc/boltmc/materials/mapbi3_05.yaml"
    )

    expected_autocorr_times = (
        expected_pop_mobs * 0.15 * constants.electron_mass / constants.electron_volt
    )
    simtimes = 100 * expected_autocorr_times

    varlists = [temperatures]
    varnames = ["lattice_temperature"]
    varformat = [int]
    extra_varnames = [["max_sim_time"]]
    extra_varlists = [[simtimes]]
    extra_varformats = [[float]]
    foldernames = [None]

    setup_boltmc(
        varnames,
        varlists,
        "inputs.yaml",
        varformat,
        foldernames,
        extra_varnames=extra_varnames,
        extra_varlists=extra_varlists,
        extra_varformats=extra_varformats,
        folder=workfolder,
    )