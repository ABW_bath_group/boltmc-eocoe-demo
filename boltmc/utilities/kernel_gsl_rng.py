from scipy.special import expi
from math import exp
import numpy as np

from ppmd import *
from ppmd.lib import build
import ctypes
import ctypes.util


class PerThreadGSLRNG:
    @staticmethod
    def setup(f, arg_types, result_type=ctypes.c_int):
        f.argtypes = arg_types
        f.restype = result_type
        return f

    def __init__(self, seed=None):
        """
        Class to create pointers to GSL RNG instances. Assumes the number of particles is constant and
        that there is only 1 MPI rank.
        """

        self.seed = np.random.randint(1, 2 ** 32 - 1) if seed is None else seed
        self.rng = np.random.RandomState(self.seed)

        gslblas = ctypes.util.find_library("gslcblas")
        assert type(gslblas) is str
        self.gslblas = ctypes.CDLL(gslblas, mode=ctypes.RTLD_GLOBAL)

        gsl = ctypes.util.find_library("gsl")
        assert type(gsl) is str
        self.gsl = ctypes.CDLL(gsl, mode=ctypes.RTLD_GLOBAL)

        self.gsl_rng_pointers = data.ScalarArray(
            ncomp=runtime.NUM_THREADS, dtype=ctypes.c_void_p
        )
        self.header = kernel.Header("gsl/gsl_rng.h")

        N = runtime.NUM_THREADS
        mt_rng_type = ctypes.c_void_p.in_dll(self.gsl, "gsl_rng_mt19937")
        self.rng_ptrs = np.zeros(N, dtype=ctypes.c_void_p)
        max_long = 2 ** (8 * ctypes.sizeof(ctypes.c_ulong) - 1) - 1

        for px in range(N):

            # make rng for this thread
            ptr = self.setup(
                self.gsl.gsl_rng_alloc, (ctypes.c_void_p,), ctypes.c_void_p
            )(mt_rng_type)
            self.rng_ptrs[px] = ptr

            # seed the rng
            tseed = int(self.rng.randint(0, max_long))
            self.setup(self.gsl.gsl_rng_set, (ctypes.c_void_p, ctypes.c_ulong), None)(
                ptr, tseed
            )

        self.gsl_rng_pointers[:] = self.rng_ptrs

    def __del__(self):
        if self.rng_ptrs is not None:
            for px in range(self.rng_ptrs.shape[0]):
                ptr = self.rng_ptrs[px]
                self.setup(self.gsl.gsl_rng_free, (ctypes.c_void_p,), None)(
                    ctypes.c_void_p(int(ptr))
                )

    def get_dat_dict(self):
        return {"GSL_RNG_POINTERS": self.gsl_rng_pointers(access._INTERNAL_RW)}

    def get_kernel_code(self):
        return """
        gsl_rng * rng = (gsl_rng*) GSL_RNG_POINTERS[omp_get_thread_num()];
        """
