import numpy as np


def random_positions(n_particles, dims, rng):
    """
    Generates a random x, y and z position for a number of particles.
    """
    lengths = np.array(dims)
    return lengths * rng.random((n_particles, 3))


def zero_positions(n_particles):
    """
    Generates an n_particles x 3 array of zeros.
    """
    return np.zeros((n_particles, 3))
