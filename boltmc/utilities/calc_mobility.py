import numpy as np
import os
import json
import glob
import matplotlib.pyplot as plt

from plotting.boltmc_plotter import BoltMCPlotter
from plotting.plot_variable import plot_variable_vs_time


def plot_vel_v_time(workfolder, fig=None, ax=None):
    """
    Plot the mean velocity vs time, to check whether equilibrium was reached.
    """
    if fig is None and ax is None:
        fig, ax = plt.subplots()

    simfile = os.path.join(workfolder, "saved_inputs.yaml")
    timefile = os.path.join(workfolder, "savetimes.json")
    velfile = os.path.join(workfolder, "mean_velocity.json")
    plotfile = os.path.join(workfolder, "vel_v_time.png")
    plot_variable_vs_time(
        "mean_velocity",
        velfile,
        simfile,
        timefile,
        plotfile,
        fig=fig,
        ax=ax,
        savefig=False,
    )

    ax.set_title(plotfile)
    fig.tight_layout()
    fig.savefig(plotfile)


def calc_mobility(workfolder, startidx, return_plotter=False):
    """
    Calculate mobility, given a folder that contains a 'savetimes.json', 'mean_vels.json' and 'saved_inputs.yaml' file.
    """
    simfile = os.path.join(workfolder, "simulation.json")
    velfile = os.path.join(workfolder, "mean_velocity.json")
    plotfiles = [simfile, velfile]

    if not all(os.path.exists(f) for f in plotfiles):
        print("skipping")
        return None

    p = BoltMCPlotter(plotfiles, verbose=False)

    mean_vel = np.mean(p.mean_velocity[startidx:, 0])
    mobility = -mean_vel / p.field

    if not return_plotter:
        return mobility

    p.mobility = mobility
    return p


def mobility_v_temp(tfolders, startidx, savefile, verbose=True):
    """
    Generates a file that contains the charge carrier mobility as a function of temperature. The argument tfolders should be a list of valid directories that all contain a 'savetimes.json', 'mean_vels.json' and 'saved_inputs.yaml' file.
    """
    temps = np.zeros(len(tfolders))
    mobilities = np.zeros(len(tfolders))

    for tx, folder in enumerate(tfolders):
        if verbose:
            print(folder)
        p = calc_mobility(folder, startidx, return_plotter=True)

        if p is not None:
            temps[tx] = p.lattice_temperature
            mobilities[tx] = p.mobility

    # order by temperature
    temps_ord = temps[np.argsort(temps)]
    mobs_ord = mobilities[np.argsort(temps)]

    mob_dict = {
        "temps": temps_ord.tolist(),
        "mobilities": mobs_ord.tolist(),
    }

    with open(savefile, "w") as f:
        json.dump(mob_dict, f)

    return mob_dict
