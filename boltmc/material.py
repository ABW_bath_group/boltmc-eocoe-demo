import numpy as np
import os
from scipy import constants
import yaml
from boltmc import boltmcdir


def _load_materials(self):
    """
    Read material files, return list of mats.
    """
    if not self.silent:
        print("Loading materials...")

    matlist = []
    if isinstance(self.matfilenames, str):
        self.matfilenames = [self.matfilenames]
    for fname in self.matfilenames:
        filepath = self.matfilepath or os.path.join(boltmcdir, "materials")
        filename = os.path.join(filepath, fname)

        matlist.append(
            Material(
                self.max_ptypes,
                self.max_valleys,
                filename=filename,
                verbose=self.verbose,
            )
        )

        # impurity density and particle density often aren't intrinsic properties of the material, so the user can specify them in the input file.
        if hasattr(self, "impdens"):
            matlist[-1].impdens = self.impdens
        if hasattr(self, "particle_dens"):
            matlist[-1].particle_dens = self.particle_dens

    self.n_mats = len(matlist)
    return matlist


class Material:
    def __init__(self, max_ptypes, max_valleys, filename=None, verbose=False):
        # TODO: can probably be more clever and work out maximum number of valleys / particles from the material files
        self.max_ptypes = max_ptypes  # electrons and holes
        self.max_valleys = max_valleys  # maximum number of valleys
        self.verbose = verbose

        if filename:
            self.read_file(filename)
        return

    def read_file(self, filename):
        """
        Reads a material property file and saves properties to the material class
        """
        # TODO: user should be able to specify only electron or hole properties
        if self.verbose:
            print("\nReading material properties from {}...".format(filename))
        with open(filename) as f:
            data = yaml.safe_load(f)

        self.n_vals = np.zeros(self.max_ptypes)
        self.labels = []
        self.list_limits = (
            self.max_ptypes,
            self.max_valleys,
            self.max_valleys,
        )  # the limits for each dimension. Format assumed for material parameters is 1) particle type, 2) valley, 3) valley again (for intervalley properties)

        for name, value in data.items():
            if isinstance(value, (float, int)):
                # for single-valued parameters across whole material
                setattr(self, name, value)
                self.labels.append(name)

            if isinstance(value, list):
                ndims = self.find_list_dims(
                    value
                )  # find the maximum number of levels in the list (assumes regular shape for now)
                if ndims > len(self.list_limits):
                    raise RuntimeError(
                        "Dimension of parameter is too high. Parameter name is {}, ndims = {}".format(
                            name, ndims
                        )
                    )

                value_arr = np.zeros(self.list_limits[:ndims])
                # self.check_list_shape(value) # TODO: add this
                self.assign_to_param_array(value_arr, value, name, ndims)
                setattr(self, name, value_arr)
                self.get_check_nvals(value, name, filename)
                self.labels.append(name)

        self.convert_params()

        if self.verbose:
            print("Finished reading material parameters from {}.".format(filename))

    def convert_params(self, conv_dict=None):
        """
        Performs material parameter unit conversions according to a conversion dictionary.
        """
        self.default_conv_dict = {
            "mstar": constants.electron_mass,
            "eps_s": constants.epsilon_0,
            "eps_inf": constants.epsilon_0,
            "acodefpot": constants.electron_volt,
            "hwo": constants.electron_volt,
            "hwij": constants.electron_volt,
            "dtk": constants.electron_volt,
            "valmin": constants.electron_volt,
            "alpha": 1 / constants.electron_volt,
            "coupling_const": (1.0e-3 ** 2)
            * (constants.electron_volt ** 2)
            / (constants.angstrom ** 2),
            "unit_cell_vol": (constants.angstrom ** 3),
        }

        if conv_dict is None:
            conv_dict = self.default_conv_dict

        for name, conv_value in conv_dict.items():
            if hasattr(self, name):
                tempval = getattr(self, name)
                setattr(self, name, tempval * conv_value)
            elif self.verbose:
                print(
                    "{} can't be converted as it hasn't been found in material file.".format(
                        name
                    )
                )

    def find_list_dims(self, l, ndims=1):
        """
        Returns the number of dimensions that a list has. Assumes that list has a regular shape. Works by calling itself recursively and stopping at the dimension where the first element is not a list.
        """
        if isinstance(l[0], list):
            return self.find_list_dims(l[0], ndims + 1)
        else:
            return ndims

    def assign_to_param_array(self, param_arr, param_list, name, ndims, index_tuple=()):
        """
        Takes a list that represents a parameter as a function of particle type, valley etc. and assigns it to the numpy array param_arr.

        Because the list does not have a regular shape (as, for example, electrons may have a different number of valleys compared with holes), this function calls itself recursively. Param_arr must initially be a numpy array full of zeros with a shape that accommodates the paramater list.
        """
        if ndims > 1:
            for l in range(len(param_list)):
                new_list = param_list[l]
                new_tuple = index_tuple + (l,)
                self.assign_to_param_array(
                    param_arr, new_list, name, ndims - 1, index_tuple=new_tuple
                )
        elif len(param_arr[index_tuple]) < len(param_list):
            raise RuntimeError(
                "Parameter {} has length {} which exceeds expected length of {}.".format(
                    name, len(param_list), len(param_arr[index_tuple])
                )
            )

        else:
            param_arr[index_tuple][: len(param_list)] = param_list

    def get_check_nvals(self, param, name, filename):
        """
        Gets the number of band valleys for each particle type from material paramters, and checks whether this is consistant with other parameters if this has already been called. Assumes that every parameter with multiple dimensions is first defined for electron/hole, then per valley.
        """
        if (
            self.find_list_dims(param) > 1
        ):  # some parameters only defined per electron / hole
            for pt, line in enumerate(param):
                if not self.n_vals[pt]:
                    self.n_vals[pt] = len(line)
                elif len(line) != self.n_vals[pt]:
                    raise RuntimeError(
                        "Inconsistent material parameter shape for {} in {}.".format(
                            name, filename
                        )
                    )
