import os
import pyfiglet
from boltmc import boltmcdir


def _print_preamble(self):
    """
    Print a header. Only prints if silent = False.
    """
    if self.silent:
        return
    # Print BoltMC Logo
    print("\n", "-" * 35)
    print(pyfiglet.figlet_format("BoltMC", font="slant"))
    print("-" * 35)
    # Print license
    with open(os.path.join(os.path.dirname(boltmcdir), "LICENSE")) as f:
        license_text = f.read()
    print("\n", license_text)


def _print_inputs(self):
    """
    Print all inputs. Only prints if verbose = True.
    """
    if self.inputs["verbose"]:
        print("Inputs:")
        for name, value in self.inputs.items():
            print("\t{}: {}".format(name, value))


def _print_progress(self):
    """
    Print the current progress of the simulation, based on the current timestep.

    Includes the iteration number, current time (in seconds), and the current percentage progress (based on the current time in seconds).

    If silent = True, don't print.
    If silent = False and verbose = False, print if save_check returns True.
    If silent = False and verbose = True, print every time.
    """
    if not self.silent:
        if not self.verbose and self.save_check():
            print(
                _format_progress_message(self.it, self.current_time, self.timesteps)
            )
        if self.verbose:
            print(_format_progress_message(self.it, self.current_time, self.timesteps))


def _format_progress_message(iteration, current_time, timesteps):
    """
    The basic formatting for the BoltMC progress printout.
    """
    return "Iteration: {} / {},\tTime: {:.3e} seconds,\t{:.2f} %".format(
        iteration,
        len(timesteps),
        current_time,
        100 * current_time / timesteps[-1],
    )