import numpy as np
import json
from scipy import constants


def _setup_thermaliser(self):
    """If the input instant_thermaliser is set to True, add an instance of the Thermaliser class to the list of extensions. The Thermaliser class periodically redistributes the ensemble momenta according to a Maxell-Boltzmann distribution while conserving total kinetic energy."""
    if hasattr(self, "instant_thermalisation") and self.instant_thermalisation:
        self.add_extensions([Thermaliser(self)])


class Thermaliser:
    """
    A class that periodically takes the mean particle energy and assigns all particles new k vector components according to a Maxwell-Boltzmann distribution at that corresponding mean energy.
    """

    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile
        if not hasattr(self.sim, "thermalise_every"):
            if self.sim.verbose:
                print(
                    "attempting instantaneous thermalisation, but thermalise_every not set. Instead, setting thermalise_every to save_every_timesteps, which equals {}".format(
                        self.sim.save_every_timesteps
                    )
                )
            self.sim.thermalise_every = self.sim.save_every_timesteps

    def execute(self):
        """
        Calculate mean particle energy and assign new k vectors according to a Maxwell-Boltzmann distribution at that corresponding mean energy.
        """
        if (
            self.sim.instant_thermalisation
            and self.sim.it % self.sim.thermalise_every == 0
        ):
            particle_energies = self.sim.calculate_kinetic_energies()
            ke_total = np.sum(particle_energies)
            self.sim.set_maxwell_boltzmann_ks(ke_total=ke_total)
