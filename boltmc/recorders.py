import numpy as np
import json
import yaml
import copy
from scipy import constants
from itertools import product
from boltmc.utilities.boltmc_constants import *


"""
Recorder classes can have the following optional methods:
- initialise(): gets called just before the main loop starts. Not to be confused with __init__().
- execute(): gets called every timestep in the main loop.
- finalise(): gets called just after main loop ends.
"""

# TODO: most of these could be given a base class, or at least share some methods


class SaveSimulation:
    """
    Save a copy of the Simulation class object in dict form. Only attributes of the Simulation class object with types that are found within the list allowed_types are saved; the default list is [int, float, np.ndarray]. np.ndarrays are converted to equivalent lists before saving.
    """

    def __init__(self, sim, outfile=None, allowed_types=None):
        self.sim = sim
        self.outfile = outfile
        self.allowed_types = allowed_types

    def finalise(self):
        if self.allowed_types is None:
            self.allowed_types = [int, float, str, np.ndarray]

        # get dict with all attributes of simulation class
        sim_dict = self.sim.__dict__
        save_dict = {}  # empty dict to be filled with saveable attributes

        for name, value in sim_dict.items():
            if type(value) in self.allowed_types:
                if type(value) is np.ndarray:
                    # numpy arrays need to be turned into lists
                    save_dict.update({name: value.tolist()})
                else:
                    # everything else is saved as is
                    save_dict.update({name: value})

        if self.outfile:
            with open(self.outfile, "w") as f:
                json.dump(save_dict, f)


class RecordMeanK:
    """
    Record the mean x, y and z wave vector magnitudes of all particles as a function of time. At the end of the simulation, save the trajectory to a .json file.
    """

    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile
        self.meankhistory = []

    def execute(self):
        if self.sim.save_check():
            ks = self.sim.State.k.view
            meanks = np.mean(ks, axis=0)
            self.meankhistory.append((meanks[0], meanks[1], meanks[2]))

    def finalise(self):
        if self.outfile:
            k_dict = {"mean_k": self.meankhistory}
            with open(self.outfile, "w") as f:
                json.dump(k_dict, f, indent=2, separators=(",", ": "))


class RecordMeanVelocity:
    """
    Record the mean velocities in the x, y and z directions of all particles as a function of time. At the end of the simulation, save the trajectory to a .json file.
    """

    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile
        self.meanvelhistory = []

    def execute(self):
        if self.sim.save_check():
            ks = self.sim.State.k.view
            mstars = self.sim.get_parameter_per_particle(self.sim.mstar)
            if self.sim.polaron_mode:
                masspols = self.sim.get_parameter_per_particle(self.sim.masspol)
            alphas = self.sim.get_parameter_per_particle(self.sim.alpha)
            ksquared = np.sum(ks ** 2, axis=-1)
            if self.sim.polaron_mode:
                gammak = constants.hbar ** 2 * ksquared / (2 * masspols)
            else:
                gammak = constants.hbar ** 2 * ksquared / (2 * mstars)
            discrim = np.sqrt(1 + 4 * alphas * gammak)
            if self.sim.polaron_mode:
                vels = ks * constants.hbar / masspols[:, None] / discrim[:, None]
            else:
                vels = ks * constants.hbar / mstars[:, None] / discrim[:, None]
            meanvels = np.mean(vels, axis=0)
            self.meanvelhistory.append((meanvels[0], meanvels[1], meanvels[2]))

    def finalise(self):
        if self.outfile:
            k_dict = {"mean_velocity": self.meanvelhistory}
            with open(self.outfile, "w") as f:
                json.dump(k_dict, f, indent=2, separators=(",", ": "))


class RecordMeanEnergy:
    """
    Record the mean particle energy as a function of time. At the end of the simulation, save the trajectory to a .json file.
    """

    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile
        self.meanenergyhistory = []
        self.meanenergyperindex = []

    def execute(self):
        if self.sim.save_check():
            energies = self.sim.calculate_energies()
            meanenergy = np.mean(energies)
            self.meanenergyhistory.append(meanenergy)
            self.meanenergyperindex.append(self.get_mean_energy_per_index(energies))

    def finalise(self):
        if self.outfile:
            e_dict = {
                "mean_energy": self.meanenergyhistory,
                "mean_energy_per_index": self.meanenergyperindex,
            }
            with open(self.outfile, "w") as f:
                json.dump(e_dict, f, indent=2, separators=(",", ": "))

    def get_mean_energy_per_index(self, energies):
        """
        Return the mean energy of the ensemble, split by their state indices i.e material, particle type and band valley.
        """
        energies = np.array(energies)
        mean_energies = np.zeros(
            (self.sim.n_mats, self.sim.max_ptypes, self.sim.max_valleys)
        )

        mats = np.array(self.sim.State.mat.view.tolist())[:, 0]
        ptypes = np.array(self.sim.State.ptype.view.tolist())[:, 0]
        vals = np.array(self.sim.State.val.view.tolist())[:, 0]

        idx_iter = product(
            range(self.sim.n_mats),
            range(self.sim.max_ptypes),
            range(self.sim.max_valleys),
        )
        for m, pt, v in idx_iter:
            m_idx = mats == m
            p_idx = ptypes == pt
            v_idx = vals == v
            valid_idx = m_idx * p_idx * v_idx
            mean_energies[m, pt, v] = np.mean(energies[valid_idx])

        return mean_energies.tolist()


class RecordSavetimes:
    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile

    def finalise(self):
        if self.outfile:
            t_dict = {"savetimes": self.sim.savetimes.tolist()}
            with open(self.outfile, "w") as f:
                json.dump(t_dict, f, indent=2, separators=(",", ": "))


class RecordMeanPos:
    """
    Record the mean x, y and z positions of all particles as a function of time. At the end of the simulation, save the trajectory to a .json file.
    """

    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile
        self.meanposhistory = []

    def execute(self):
        if self.sim.save_check():
            pos = self.sim.State.pos.view
            meanpos = np.mean(pos, axis=0)
            self.meanposhistory.append((meanpos[0], meanpos[1], meanpos[2]))

    def finalise(self):
        if self.outfile:
            pos_dict = {"mean_pos": self.meanposhistory}
            with open(self.outfile, "w") as f:
                json.dump(pos_dict, f, indent=2, separators=(",", ": "))


class RecordIniFinState:
    """
    Record the entire state of the system at the beginning and end of the simulation, and save to a .json file. Currently saves: current time, positions, wavevectors, charges, materials, particle types, band valleys, scattering times
    """

    def __init__(self, sim, inifile=None, finfile=None):
        self.sim = sim
        self.inifile = inifile
        self.finfile = finfile

    def initialise(self):
        self.record_state(self.inifile)

    def finalise(self):
        self.record_state(self.finfile)

    def record_state(self, filename):
        if self.sim.verbose:
            print("Saving state to file...")
        record_dict = {
            "time": self.sim.current_time,
            "positions": self.sim.State.pos.view.tolist(),
            "wavevectors": self.sim.State.k.view.tolist(),
            "charges": self.sim.State.charge.view.tolist(),
            "materials": self.sim.State.mat.view.tolist(),
            "ptypes": self.sim.State.ptype.view.tolist(),
            "valleys": self.sim.State.val.view.tolist(),
            "scattering_times": self.sim.State.ts.view.tolist(),
        }
        if filename:
            with open(filename, "w") as f:
                json.dump(record_dict, f, separators=(",", ": "))

        if self.sim.verbose:
            print("Saved state to file.")


class RecordScatteringRates:
    """
    Record the scattering rates for all mechanisms at the start of the simulation and save it to a .json file.
    """

    def __init__(self, sim, outfile):
        self.sim = sim
        self.outfile = outfile

    def initialise(self):
        if self.sim.verbose:
            print("Saving rate table...")
        mechs = self.sim.smh.scatter_funcs
        rates = np.stack([sx.rate for sx in mechs[:-1]], axis=-1)
        labels = [sx.name() for sx in mechs[:-1]]

        rate_dict = {
            "rate_labels": list(labels),
            "energies": self.sim.energies.tolist(),
            "rates": [i.tolist() for i in rates],
        }

        with open(self.outfile, "w") as f:
            json.dump(rate_dict, f)

        if self.sim.verbose:
            print("Saved rate table.")


class RecordScatteringEvents:
    """
    Record the number of scattering rates that occurred throughout the simulation by saving the reshaped EVENT_TRACKER array.
    """

    def __init__(self, sim, outfile, vs_time=False):
        self.sim = sim
        self.outfile = outfile
        self.vs_time = vs_time

    def initialise(self):
        if not hasattr(self.sim, "track_events"):
            raise RuntimeError(
                "If saving events, the input track_events should exist and be set to True."
            )
        if not self.sim.track_events:
            raise RuntimeError(
                "If saving  events, the input track_events should be set to True, not {}.".format(
                    self.sim.track_events
                )
            )
        if self.vs_time:
            # Set shape of tracking aray, now with time index
            self.events_v_time_shape = (
                len(self.sim.savetimes),
                *self.sim.tracker_shape,
            )
            self.events = np.zeros(self.events_v_time_shape)

    def execute(self):
        """
        Get total number of events so far, then subtract from previous total to get events during last timestep. If not saving vs time, this is ignored.
        """
        if self.vs_time and self.sim.save_check():
            cumulative_events = self.get_events_from_event_tracker()
            saveid = (np.abs(self.sim.current_time - self.sim.savetimes)).argmin()
            if saveid > 0:
                self.events[saveid - 1] = (
                    cumulative_events - self.last_cumulative_events
                )
            self.last_cumulative_events = cumulative_events

    def finalise(self):
        """
        If tracking events vs time, get the final events. If not, get all the events. Then save events to file.
        """
        if self.sim.verbose:
            print("Saving events to file...")
        if not self.sim.track_events:
            return
        if not self.vs_time:
            self.events = self.get_events_from_event_tracker()
        else:
            self.events[-1] = (
                self.get_events_from_event_tracker() - self.last_cumulative_events
            )

        mechs = self.sim.smh.scatter_funcs
        labels = [sx.name() for sx in mechs[:-1]]
        d_cpol = 2 / self.sim.tracking_n_cpol
        # we add d_cpol to be consistent with the binning procedure in final_states.h
        cpol = np.arange(-1, 1, d_cpol) + d_cpol

        event_dict = {
            "rate_labels": list(labels),
            "tracking_energy_bins": self.sim.tracking_bins.tolist(),
            "tracking_cpol_bins": cpol.tolist(),
        }
        event_label = "events_v_time" if self.vs_time else "events"
        event_dict.update({event_label: self.events.tolist()})

        with open(self.outfile, "w") as f:
            json.dump(event_dict, f)

        if self.sim.verbose:
            print("Saved events to file.")

    def get_events_from_event_tracker(self):
        """
        Return an array containing the number of events that have occured so far in the simulation.
        """
        events_flat = np.array([i for i in self.sim.State.EVENT_TRACKER])
        return np.reshape(events_flat, self.sim.tracker_shape)


class RecordEnergyDistributions:
    """
    Record the particle energy distribution as a function of time.
    Useful inputs (use two of three):
    - distribution_energy_max: maximum energy (in eV)
    - distribution_energy_step: energy bin width (in eV)
    - distribution_energy_n: number of energy steps

    """

    def __init__(self, sim, outfile, distribution_energy_bins=None):
        self.sim = sim
        self.outfile = outfile
        self.energy_distributions = []

        if distribution_energy_bins:
            # getting bins from argument
            self.distribution_energy_bins = distribution_energy_bins
        elif any(
            hasattr(self.sim, attr)
            for attr in [
                "distribution_energy_max",
                "distribution_energy_step",
                "distribution_energy_n",
            ]
        ):
            # getting bins specified by input file
            self.bins = q * self.sim.get_arange_from_inputs(
                "distribution_energy_max",
                "distribution_energy_step",
                "distribution_energy_n",
            )
        else:
            # getting bins from scattering rate table
            self.bins = self.sim.energies

    def execute(self):
        if self.sim.save_check():
            self.save_energy_distribution()

    def finalise(self):
        if self.sim.verbose:
            print("Saving energy distribution to file...")

        np.savez(
            self.outfile,
            distribution_energies=self.bins,
            distribution=self.energy_distributions,
        )

        if self.sim.verbose:
            print("Saved energy distribution to file.")

    def save_energy_distribution(self):
        particle_energies = self.sim.calculate_energies()
        histogram = np.histogram(particle_energies, self.bins)[0]
        self.energy_distributions.append(histogram.tolist())


class RecordKDistributions:
    """
    Record the particle wavevector distribution as a function of time.
    """

    def __init__(self, sim, outfile):
        self.sim = sim
        self.outfile = outfile
        self.k_distributions = []
        self.k_dist_bins = np.linspace(
            -self.sim.k_dist_max, self.sim.k_dist_max, self.sim.k_dist_n
        )

    def execute(self):
        if self.sim.save_check():
            self.save_k_distribution()

    def finalise(self):
        if self.sim.verbose:
            print("Saving k distribution to file...")

        np.savez(
            self.outfile,
            k_distribution_bins=self.k_dist_bins,
            k_distribution=self.k_distributions,
        )

        if self.sim.verbose:
            print("Saved k distribution to file.")

    def save_k_distribution(self):
        particle_ks = self.sim.State.k.view
        histogram = np.zeros((len(self.k_dist_bins) - 1, 3))
        for d in range(3):
            histogram[:, d] = np.histogram(particle_ks[:, d], self.k_dist_bins)[0]
        self.k_distributions.append(histogram.tolist())


class RecordOccupation:
    def __init__(self, sim, outfile):
        self.sim = sim
        self.outfile = outfile
        self.occupations = []

    def execute(self):
        if self.sim.save_check():
            self.save_occupations()

    def finalise(self):
        if self.sim.verbose:
            print("Saving occupations to file...")

        occ_dict = {"occupations": self.occupations}
        with open(self.outfile, "w") as f:
            json.dump(occ_dict, f)

        if self.sim.verbose:
            print("Saved occupations to file.")

    def save_occupations(self):
        occ_idx = (
            self.sim.State.mat.view,
            self.sim.State.ptype.view,
            self.sim.State.val.view,
        )
        occs = np.zeros((self.sim.n_mats, self.sim.max_ptypes, self.sim.max_valleys))
        np.add.at(occs, occ_idx, 1)
        self.occupations.append(occs.tolist())


class RecordMeanSquareDisplacement:
    """
    Record the mean squared displacement of all particles in the simulation.
    """

    def __init__(self, sim, outfile=None):
        self.sim = sim
        self.outfile = outfile
        self.msd = []

    def initialise(self):
        # Record initial positions
        self.initial_pos = copy.deepcopy(self.sim.State.pos.view)

    def execute(self):
        if self.sim.save_check():
            pos = self.sim.State.pos.view
            displacement = pos - self.initial_pos
            sq_displacement = np.sum(np.square(displacement), axis=-1)
            # warning: assumes no change in particle order. Ok if they all start off the same but will be incorrect elsewhere
            self.msd.append(np.mean(sq_displacement))

    def finalise(self):
        if self.outfile:
            msd_dict = {"msd": self.msd}
            with open(self.outfile, "w") as f:
                json.dump(msd_dict, f)


class RecordScreeningLength:
    def __init__(self, sim, outfile="screening_length.json"):
        self.sim = sim
        self.outfile = outfile
        self.screening_lengths = []

    def execute(self):
        if self.sim.save_check():
            self.screening_lengths.append(self.sim.screenlen.tolist())

    def finalise(self):
        savedict = {"screening_length": self.screening_lengths}
        with open(self.outfile, "w") as f:
            json.dump(savedict, f)


class RecordVelAutocorr:
    """
    Save the velocity of every particle as a function of time.
    """

    def __init__(self, sim, outfile):
        self.sim = sim
        self.outfile = outfile
        self.vel_autocorr = np.zeros((len(self.sim.savetimes), self.sim.n_particles, 3))

    def execute(self):
        if self.sim.save_check():
            ks = self.sim.State.k.view
            mstars = self.sim.get_parameter_per_particle(self.sim.mstar)
            if self.sim.polaron_mode:
                masspols = self.sim.get_parameter_per_particle(self.sim.masspol)
            alphas = self.sim.get_parameter_per_particle(self.sim.alpha)
            ksquared = np.sum(ks ** 2, axis=-1)
            if self.sim.polaron_mode:
                gammak = constants.hbar ** 2 * ksquared / (2 * masspols)
            else:
                gammak = constants.hbar ** 2 * ksquared / (2 * mstars)
            discrim = np.sqrt(1 + 4 * alphas * gammak)
            if self.sim.polaron_mode:
                vels = ks * constants.hbar / masspols[:, None] / discrim[:, None]
            else:
                vels = ks * constants.hbar / mstars[:, None] / discrim[:, None]

            saveid = (np.abs(self.sim.current_time - self.sim.savetimes)).argmin()
            self.vel_autocorr[saveid, :, :] = vels

    def finalise(self):
        if self.sim.verbose:
            print("Saving autocorrelation data...")

        np.savez(self.outfile, all_velocities=self.vel_autocorr)

        if self.sim.verbose:
            print("Saved autocorrelation data.")
