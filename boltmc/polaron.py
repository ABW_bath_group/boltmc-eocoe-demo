import math
import numpy as np
from itertools import product
from scipy import constants, integrate, optimize


# Constants
pi = constants.pi
hbar = constants.hbar
q = constants.elementary_charge
k_B = constants.Boltzmann


class PolaronLib:
    def __init__(self, sim):
        self.sim = sim

    def initialise_polarons(self):
        self.set_polaron_parameters()

    def osaka_free_energy(self, params, args):
        """
        Calculates the polaron free energy according to Osaka.

        Arguments:
            params (float): [v, w], the polaron dimensionless parameters
            args (float): [alpha, beta], the Frohlich coupling constant and reduced phonon frequency

        Returns:
            free_energy (float): the Osaka free energy
        """
        v, w = params[0], params[1]
        alpha, beta = args[0], args[1]

        if v < 0:
            v = np.abs(v)
        if w < 0:
            w = np.abs(w)

        A = (
            3
            / beta
            * (
                math.log(v / w)
                - 0.5 * math.log(2 * pi * beta)
                - math.log(math.sinh(v * beta / 2) / math.sinh(w * beta / 2))
            )
        )
        y = (
            lambda x: 1
            / (1 - math.exp(-v * beta))
            * (1 + math.exp(-v * beta) - math.exp(-v * x) - math.exp(v * (x - beta)))
        )
        f = (
            lambda x: (math.exp(beta - x) + math.exp(x))
            / (w ** 2 * x * (1 - x / beta) + y(x) * (v ** 2 - w ** 2) / v) ** 0.5
        )
        intf = integrate.quad(f, 0, beta / 2)
        B = alpha * v / (math.sqrt(pi) * (math.exp(beta) - 1)) * intf[0]
        C = (
            0.75
            * (v ** 2 - w ** 2)
            / v
            * (1 / (math.tanh(v * beta / 2)) - 2 / (v * beta))
        )
        return -(A + B + C)

    def polaron_vw(self, temperature, mstar, hwo, eps_inf, eps_s, guess=None):
        """
        Calculates the dimensionless polaron parameters v and w by minimising the Osaka polaron free energy

        Arguments:
            temperature (float): the temperature in Kelvin
            guess = None (float): the initial guess of [v, w] when minimising the free energy. Gives math errors when chosen unwisely.

        Returns:
            v, w (float): the dimensionless polaron parameters
        """
        if guess is None:
            # Rule of thumb to help avoid math errors
            guess = [3 + temperature / 10, 2 + temperature / 10]

        froconst = (
            q ** 2
            / hbar
            * math.sqrt(mstar / (2 * hwo))
            * (1 / eps_inf - 1 / eps_s)
            / (4 * pi)
        )
        kT = k_B * temperature
        betared = hwo / kT

        res = optimize.minimize(
            self.osaka_free_energy,
            guess,
            args=[froconst, betared],
            method="Nelder-Mead",
            tol=1e-04,
        )
        v, w = res.x[0], res.x[1]

        return v, w

    def set_polaron_parameters(self, guess=None):
        """
        Calculates the polaron mass, reduced polaron mass and oscillator frequency by converting the dimensionless polaron parameters v and w.

        Arguments:
            guess = None (float): the initial guess of [v, w] when minimising the free energy. Gives math errors when chosen unwisely.

        Returns:
            Nothing
        """
        index_ranges = (
            range(self.sim.n_mats),
            range(self.sim.max_ptypes),
            range(self.sim.max_valleys),
        )
        index_lengths = [len(i) for i in index_ranges]
        idx_iter = product(*index_ranges)
        self.v_feyn = np.zeros(index_lengths)
        self.w_feyn = np.zeros(index_lengths)
        self.sim.masspol = np.zeros(index_lengths)
        self.sim.mupol = np.zeros(index_lengths)
        self.sim.wpol = np.zeros(index_lengths)
        for m in range(self.sim.n_mats):
            self.sim.mats[m].masspol = np.zeros(index_lengths[1:])
            self.sim.mats[m].mupol = np.zeros(index_lengths[1:])
            self.sim.mats[m].wpol = np.zeros(index_lengths[1:])

        for m, p, v in idx_iter:
            self.v_feyn[m, p, v], self.w_feyn[m, p, v] = self.polaron_vw(
                self.sim.lattice_temperature,
                self.sim.mstar[m, p, v],
                self.sim.hwo[m, p],
                self.sim.eps_inf[m],
                self.sim.eps_s[m],
                guess,
            )

            mtotfact = np.square(np.divide(self.v_feyn[m, p, v], self.w_feyn[m, p, v]))
            woscfact = self.v_feyn[m, p, v]
            self.sim.masspol[m, p, v] = self.sim.mstar[m, p, v] * mtotfact
            self.sim.mupol[m, p, v] = self.sim.mstar[m, p, v] * (
                1 - self.sim.mstar[m, p, v] / self.sim.masspol[m, p, v]
            )
            if np.isnan(self.sim.mupol[m, p, v]):
                self.sim.mupol[m, p, v] = np.nan_to_num(self.sim.mupol[m, p, v])
            self.sim.wpol[m, p, v] = self.sim.hwo[m, p] * woscfact / hbar

            # Polaron parameters need to be material attributesGJ13%
            self.sim.mats[m].masspol[p, v] = self.sim.mats[m].mstar[p, v] * mtotfact
            self.sim.mats[m].mupol[p, v] = self.sim.mstar[m, p, v] * (
                1 - self.sim.mstar[m, p, v] / self.sim.mats[m].masspol[p, v]
            )
            if np.isnan(self.sim.mats[m].mupol[p, v]):
                self.sim.mats[m].mupol[p, v] = np.nan_to_num(
                    self.sim.mats[m].mupol[p, v]
                )
            self.sim.mats[m].wpol[p, v] = self.sim.hwo[m, p] * woscfact / hbar

        for mat in self.sim.mats:
            mat.labels.extend(("masspol", "mupol", "wpol"))


def set_null_masspol(sim):
    """If polaron mode is switched off, masspol material attribute still needs to be created for PPMD library to be built."""
    for m in range(sim.n_mats):
        index_ranges = (
            range(sim.max_ptypes),
            range(sim.max_valleys),
        )
        index_lengths = [len(i) for i in index_ranges]
        sim.mats[m].masspol = np.zeros(index_lengths)

    for mat in sim.mats:
        mat.labels.extend(("masspol",))
