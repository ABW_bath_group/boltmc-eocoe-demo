# BoltMC EoCoE Demo

A version of BoltMC to demonstrate the calculation of charge carrier mobilities in semiconductors, in particular polaron mobilities in lead--halide perovskites.
