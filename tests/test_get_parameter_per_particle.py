import matplotlib.pyplot as plt
import numpy as np
from scipy import constants

from boltmc.simulation import Simulation


def run_emc():
    N1 = 5
    N = N1 ** 3

    s = Simulation(
        params={
            "N1": N1,
            "n_timesteps": 10,
            "max_ptypes": 4,
            "max_valleys": 7,
            "matfilenames": ["example_mat.yaml", "example_mat_2.yaml"],
        }
    )
    s.run()

    for label in s.mats[0].labels:
        particlearray = s.get_parameter_per_particle(getattr(s, label))
        assert len(particlearray) == N


if __name__ == "__main__":
    run_emc()
