import os
import boltmc
from boltmc.simulation import Simulation
from boltmc.recorders import *
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.use("Agg")

# Note that all simulation parameters are as found in defaults.yaml unless otherwise specified.
test_params = {
    # "verbose": False,
    "track_events": False,
    "save_inputs": False,
    "matfilenames": ["mapbi3_05.yaml"],
    "polarons": True,
    "max_valleys": 1,
}


def test_polaron():
    s = Simulation(params=test_params)
    print(s.masspol)
    print(s.wpol)
    print(s.mupol)
    print(s.Pol.v_feyn)
    print(s.Pol.w_feyn)


def polaron_params():
    temperatures = np.linspace(1, 500, 100)
    masspols = np.zeros(len(temperatures))
    wpols = np.zeros(len(temperatures))
    for tx, temperature in enumerate(temperatures):
        temp_params = {**test_params, "lattice_temperature": temperature}
        s = Simulation(params=temp_params)
        masspols[tx] = s.masspol[0, 0, 0]
        wpols[tx] = s.wpol[0, 0, 0]

    fig, ax = plt.subplots()
    ax.plot(temperatures, masspols / s.mstar[0, 0])
    fig.savefig("masspol.png", dpi=500)

    fig, ax = plt.subplots()
    ax.plot(temperatures, wpols / s.hwo[0, 0])
    fig.savefig("wpol.png", dpi=500)


if __name__ == "__main__":
    # test_polaron()
    polaron_params()