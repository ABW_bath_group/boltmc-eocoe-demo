import os
from boltmc import material, boltmcdir


def test_read_file():

    max_ptypes = 2
    max_valleys = 4

    material_path = os.path.join(boltmcdir, "materials")
    matfiles = ["example_mat.yaml", "example_mat_2.yaml"]
    mats = []

    for m in range(len(matfiles)):
        filename = os.path.join(material_path, matfiles[m])
        mats.append(material.Material(max_ptypes, max_valleys, filename=filename))
        mats[-1].impdens = 1e24

        assert mats[-1].impdens == 1e24


if __name__ == "__main__":
    test_read_file()
