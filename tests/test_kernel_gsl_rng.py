import numpy as np

from ppmd import *
import ctypes

from boltmc.utilities.kernel_gsl_rng import PerThreadGSLRNG


def test_gsl_rng():

    N1 = 50
    N = N1 ** 3
    rng = np.random.RandomState(12439237)

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.x = data.ParticleDat(ncomp=1, dtype=ctypes.c_double)
    A.y = data.ParticleDat(ncomp=1, dtype=ctypes.c_double)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG(1)

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "X": A.x(access.WRITE),
        "Y": A.y(access.READ),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_inverse_Ei",
            """
            {GSL_RNG_INIT}
            X.i[0] = gsl_rng_uniform(rng);
            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(kernel.Header("gsl/gsl_sf.h"), kernel.Header("gsl/gsl_rng.h")),
        ),
        dat_dict,
    )

    for ix in range(1):
        l.execute()

    nbins = 10
    h = np.histogram(A.x.view, nbins, range=(0.0, 1.0))[0] * (nbins / N)
    assert np.min(h) > 0.97
    assert np.max(h) < 1.03
