import os
import numpy as np
from boltmc import Simulation
from boltmc.recorders import RecordScatteringEvents, RecordScatteringRates
from plotting import BoltMCPlotter

# Note that all simulation parameters are as found in defaults.yaml unless otherwise specified.
test_params = {
    "verbose": False,
    "silent": True,
    "track_events": True,
    "save_inputs": True,
    "matfilenames": ["gaas_01.yaml"],
    "lattice_temperature": 500,
    "field": 1e5,
    "n_particles": int(1e2),
    "max_ptypes": 2,
    "rate_table_energy_step": 1e-2,
    "rate_table_energy_n": 200,
    "scattering_mechs": {
        "acoustic.py": "Acoustic",
        "impurity.py": "Impurity",
        "poptical.py": "all",
        "npoptical.py": "all",
    },
}

tol = 0.05  # maximum tolerance for difference between rates and effective rates from events (as a ratio of rates)
event_threshold = 1e4  # number of events before rates and events are compared


def test_event_stats():
    eventfile = "events.json"
    events_v_timefile = "events_v_time.json"
    ratefile = "rates.json"

    s = Simulation(params=test_params)
    s.add_extensions(
        [
            RecordScatteringEvents(s, eventfile),
            RecordScatteringEvents(s, events_v_timefile, vs_time=True),
            RecordScatteringRates(s, ratefile),
        ]
    )
    s.run()

    # Let BoltMCPlotter handle file reading
    filenames = [eventfile, events_v_timefile, ratefile, "saved_inputs.yaml"]
    p = BoltMCPlotter(filenames, verbose=False)

    # Were any events recorded?
    assert p.events.any()
    assert p.events_v_time.any()
    # Do the two methods of collecting events get the same sum?
    assert np.sum(p.events) == np.sum(p.events_v_time)
    assert (p.events == np.sum(p.events_v_time, axis=0)).all()

    n_mats = len(p.matfilenames)
    t_sim = p.n_timesteps * p.timestep

    effective_rates = convert_events_to_rates(p.events, p.n_particles, total_time=t_sim)

    for mx in range(n_mats):
        for px in range(p.max_ptypes):
            # returns true or false for each valley; true only if at least one event occurred in the valley
            check_vals = [p.events[mx, px, vx].any() for vx in range(p.max_valleys)]
            # returns list of valleys that passed the last test
            valid_vals = [i for i, x in enumerate(check_vals) if x]

            n_axes = sum(check_vals)

            if n_axes > 0:
                for vx in range(len(valid_vals)):
                    for sx in range(len(p.rate_labels)):
                        for ex in range(len(p.energies)):
                            events_here = p.events[mx, px, vx, ex, 0, sx]
                            eff_rate_here = effective_rates[mx, px, vx, ex, 0, sx]
                            rate_here = p.rates[mx, px, vx, ex, sx]
                            if events_here > event_threshold:
                                diff_norm = abs(eff_rate_here - rate_here) / rate_here

                                # Check that the number of events which happened correctly represents the expected scattering rate
                                if diff_norm > tol:
                                    print(
                                        "failed test, mx = {}, px = {}, vx = {}, sx = {}, ex = {}, diff = {}".format(
                                            mx, px, vx, sx, ex, diff_norm
                                        )
                                    )

                                assert diff_norm < tol

    # Clean up files
    for f in filenames:
        if os.path.exists(f):
            os.remove(f)


def convert_events_to_rates(
    events, n_particles, v_time=False, total_time=None, timestep=None
):
    events_shape = np.shape(events)

    if v_time:
        if not timestep:
            raise RuntimeError("If v_time is True, timestep must be specified.")

        effective_rates = np.zeros(events_shape)
        for tx in range(events_shape[0]):
            effective_rates[tx] = convert_events_to_rates(
                events[tx], n_particles, total_time=timestep, v_time=False
            )

    else:
        if not total_time:
            raise RuntimeError("If v_time is False, total_time must be specified.")

        total_events = np.sum(events)
        effective_rates = np.zeros(events_shape)

        for mx in range(events_shape[0]):  # material index
            for px in range(events_shape[1]):  # particle type index
                for vx in range(events_shape[2]):  # band valley index
                    available_events = np.sum(events[mx, px, vx], axis=-2)
                    events_all_mechs = np.sum(available_events, axis=-1)

                    effective_rates[mx, px, vx, :, 0, :] = (
                        available_events * total_events
                    ) / (events_all_mechs[:, None] * n_particles * total_time)

    effective_rates = np.nan_to_num(effective_rates)
    return effective_rates


if __name__ == "__main__":
    test_event_stats()