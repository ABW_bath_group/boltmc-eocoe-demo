from boltmc.scatter_mech_gen import (
    BaseScatterMech,
    ScatterMechHandler,
    Padding,
    ScatterMechInterface,
)
import numpy as np
from ppmd import *
from ppmd.lib import build
import ctypes
from test_kernel_gsl_rng import PerThreadGSLRNG
from functools import reduce
from itertools import product


def test_construct_c():
    class A(BaseScatterMech):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 1.0
            self.rate[0, 0, 1] = 2.0
            self.c = r"""
            int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {

                *kx = 1.0;
                *ky = 2.1;
                *kz = 3.1;

                return 0;
            }
            """

    class B(BaseScatterMech):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 2.0
            self.rate[0, 0, 1] = 1.0

            self.c = r"""
            int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {

                *kx = 2.0;
                *ky = 5.2;
                *kz = 6.2;

                return 0;
            }
            """

    # no padding example
    hf = [A(), B()]
    sfh = ScatterMechHandler(hf, padding=False)

    N1 = 100
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)
    A.COUNTS = data.GlobalArray(ncomp=4, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG()

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            double kx = K.i[0];
            double ky = K.i[1];
            double kz = K.i[2];
            int sx = 0;

            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 0, &sx, rng, &kx, &ky, &kz);

            if (kx == 1.0) {{
                COUNTS[0]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[1]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 1, &sx, rng, &kx, &ky, &kz);

            if (kx == 1.0) {{
                COUNTS[2]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[3]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
            ),
        ),
        dat_dict,
    )

    for tx in range(10):
        l.execute()

    # our rate of scatter funcs is 1:2 so we can test the ratio is near 0.5 for a large number of
    # executions.
    assert abs(0.5 - A.COUNTS[0] / A.COUNTS[1]) < 3 * 10.0 ** -3
    assert abs(2.0 - A.COUNTS[2] / A.COUNTS[3]) < 3 * 10.0 ** -3
    assert A.FAIL[0] == 0


def test_construct_c_2():
    class A(ScatterMechInterface(("int", "int *"))):
        def __init__(self):
            self.rate = np.ones((1, 1, 1))
            self.c = r"""
            int %(NAME)s(gsl_rng * rng, int a, int * b) {

                *b = a;

                return 0;
            }
            """

    # no padding example
    hf = [
        A(),
    ]
    sfh = ScatterMechHandler(hf, padding=True)

    N1 = 100
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)
    A.COUNTS = data.GlobalArray(ncomp=4, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG()

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            int sx = 0;
            int a = 4;
            int b;

            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 0, &sx, rng, a, &b);

            if (b != a) {{
                FAIL[0]++;
            }}
            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
            ),
        ),
        dat_dict,
    )

    l.execute()
    assert A.FAIL[0] == 0


def test_construct_scatter_padding():
    class A(BaseScatterMech):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 1.0
            self.rate[0, 0, 1] = 4.0
            self.c = r"""
            int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {

                *kx = 1.0;
                *ky = 2.1;
                *kz = 3.1;

                return 0;
            }
            """

    class B(BaseScatterMech):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 2.0
            self.rate[0, 0, 1] = 2.0

            self.c = r"""
            int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {

                *kx = 2.0;
                *ky = 5.2;
                *kz = 6.2;

                return 0;
            }
            """

    class TestPadding(BaseScatterMech):
        def __init__(self):
            self.rate = None

            self.c = r"""
            int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {

                *kx = 7.0;
                *ky = 8.2;
                *kz = 9.2;

                return 0;
            }
            """

    hf = [A(), B()]
    # To use the default padding_func we would just initialise the class with ScatterMechHandler(hf)
    sfh = ScatterMechHandler(hf, padding_func=TestPadding())
    assert abs(sfh.maxrate - 6.0) < 10.0 ** -15

    N1 = 100
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)
    A.COUNTS = data.GlobalArray(ncomp=6, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG()

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            double kx = K.i[0];
            double ky = K.i[1];
            double kz = K.i[2];
            int sx = 0;

            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 0, &sx, rng, &kx, &ky, &kz);

            if (kx == 1.0) {{
                COUNTS[0]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[1]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else if (kx == 7.0) {{
                COUNTS[2]++;
                if (!(ky == 8.2)) {{ FAIL[0]++; }}
                if (!(kz == 9.2)) {{ FAIL[0]++; }}
                if (sx != 2) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 1, &sx, rng, &kx, &ky, &kz);

            if (kx == 1.0) {{
                COUNTS[3]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[4]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else if (kx == 7.0) {{
                COUNTS[5]++;
                if (!(ky == 8.2)) {{ FAIL[0]++; }}
                if (!(kz == 9.2)) {{ FAIL[0]++; }}
                if (sx != 2) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
            ),
        ),
        dat_dict,
    )

    for tx in range(10):
        l.execute()

    # first call
    assert abs(0.5 - A.COUNTS[0] / A.COUNTS[1]) < 3 * 10.0 ** -3
    assert abs(1.0 / 3.0 - A.COUNTS[0] / A.COUNTS[2]) < 3 * 10.0 ** -3
    assert abs(2.0 / 3.0 - A.COUNTS[1] / A.COUNTS[2]) < 3 * 10.0 ** -3

    # second call
    assert abs(2.0 - A.COUNTS[3] / A.COUNTS[4]) < 3 * 10.0 ** -3
    assert A.COUNTS[5] == 0

    assert A.FAIL[0] == 0


def test_construct_scatter_4dims_padding():

    base_func = r"""
    int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz) {{
        *kx = {}.0;
        return 0;
    }}
    """
    base_shape = (2, 3, 4, 3)
    ndim = len(base_shape)
    rng = np.random.RandomState(134135)

    class A(BaseScatterMech):
        def __init__(self):
            self.rate = rng.uniform(low=0.2, high=1.0, size=base_shape)
            self.c = base_func.format(1)

    class B(BaseScatterMech):
        def __init__(self):
            self.rate = rng.uniform(low=0.2, high=1.0, size=base_shape)
            self.c = base_func.format(2)

    class C(BaseScatterMech):
        def __init__(self):
            self.rate = rng.uniform(low=0.2, high=1.0, size=base_shape)
            self.c = base_func.format(3)

    class D(BaseScatterMech):
        def __init__(self):
            self.rate = rng.uniform(low=0.2, high=1.0, size=base_shape)
            self.c = base_func.format(4)

    class TestPadding(BaseScatterMech):
        def __init__(self):
            self.rate = None
            self.c = base_func.format(5)

    a = A()
    b = B()
    c = C()
    d = D()

    hf = [a, b, c, d]
    # To use the default padding_func we would just initialise the class with ScatterMechHandler(hf)
    sfh = ScatterMechHandler(hf, padding_func=TestPadding())

    N1 = 100
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)

    A.COUNTS = data.GlobalArray(ncomp=len(hf) + 1, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)
    A.INDEXING = data.ScalarArray(ncomp=len(hf), dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG(1)

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
        "INDEXING": A.INDEXING(access.READ),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            double kx = K.i[0];
            double ky = K.i[1];
            double kz = K.i[2];
            int sx = -1;

            apply_scatter_function(SCATTER_RATES_ARRAY, INDEXING[0], INDEXING[1], INDEXING[2], INDEXING[3], &sx, rng, &kx, &ky, &kz);

            if (fabs(kx - 1.0) < 1E-15) {{
                COUNTS[0]++;
                if (sx != 0) {{FAIL[0]++;}}
            }} else if (fabs(kx - 2.0) < 1E-15) {{
                COUNTS[1]++;
                if (sx != 1) {{FAIL[0]++;}}
            }} else if (fabs(kx - 3.0) < 1E-15) {{
                COUNTS[2]++;
                if (sx != 2) {{FAIL[0]++;}}
            }} else if (fabs(kx - 4.0) < 1E-15) {{
                COUNTS[3]++;
                if (sx != 3) {{FAIL[0]++;}}
            }} else if (fabs(kx - 5.0) < 1E-15) {{
                COUNTS[4]++;
                if (sx != 4) {{FAIL[0]++;}}
            }} else {{
                FAIL[0]++;
            }}

            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
                kernel.Header("math.h"),
            ),
        ),
        dat_dict,
    )

    iterators = [range(dx) for dx in base_shape]
    iterset = product(*iterators)

    def rel_err(ca, cb, pa, pb):

        if pa == 0.0 or pb == 0.0:
            return 0.0

        rel = max(pa / pb, pb / pa)

        abs_err = abs(ca / cb - pa / pb)
        rel_err = abs_err / rel

        return rel_err

    for dimx in iterset:
        A.INDEXING[:] = dimx
        A.COUNTS.set(0)

        for tx in range(100):
            l.execute()

        assert (
            rel_err(hf[0].rate[dimx], hf[1].rate[dimx], A.COUNTS[0], A.COUNTS[1])
            < 1.0 * 10.0 ** -2
        )
        assert (
            rel_err(hf[1].rate[dimx], hf[2].rate[dimx], A.COUNTS[1], A.COUNTS[2])
            < 1.0 * 10.0 ** -2
        )
        assert (
            rel_err(hf[2].rate[dimx], hf[3].rate[dimx], A.COUNTS[2], A.COUNTS[3])
            < 1.0 * 10.0 ** -2
        )

        pad_rate = sfh.maxrate - (
            hf[0].rate[dimx] + hf[1].rate[dimx] + hf[2].rate[dimx] + hf[3].rate[dimx]
        )
        if pad_rate > 0:
            assert (
                rel_err(hf[3].rate[dimx], pad_rate, A.COUNTS[3], A.COUNTS[4])
                < 10.0 ** -2
            )

        assert A.FAIL[0] == 0


def test_construct_c_struct():

    TestStructInterface = ScatterMechInterface(
        "ScatterData *",
        """
        typedef struct ScatterData_ {
            double *kx;
            double *ky;
            double *kz;
        } ScatterData;
        """,
    )

    class A(TestStructInterface):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 1.0
            self.rate[0, 0, 1] = 2.0
            self.c = r"""
            int %(NAME)s(gsl_rng * rng, ScatterData * SD) {

                SD->kx[0] = 1.0;
                SD->ky[0] = 2.1;
                SD->kz[0] = 3.1;

                return 0;
            }
            """

    class B(TestStructInterface):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 2.0
            self.rate[0, 0, 1] = 1.0

            self.c = r"""
            int %(NAME)s(gsl_rng * rng, ScatterData * SD) {

                SD->kx[0] = 2.0;
                SD->ky[0] = 5.2;
                SD->kz[0] = 6.2;

                return 0;
            }
            """

    # no padding example
    hf = [A(), B()]
    sfh = ScatterMechHandler(hf, padding=False)

    N1 = 100
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)
    A.COUNTS = data.GlobalArray(ncomp=4, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG()

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            double kx = K.i[0];
            double ky = K.i[1];
            double kz = K.i[2];
            int sx = 0;

            
            ScatterData sd = {{&kx, &ky, &kz}};

            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 0, &sx, rng, &sd);

            if (kx == 1.0) {{
                COUNTS[0]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[1]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 1, &sx, rng, &sd);

            if (kx == 1.0) {{
                COUNTS[2]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[3]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
            ),
        ),
        dat_dict,
    )

    for tx in range(10):
        l.execute()

    # our rate of scatter funcs is 1:2 so we can test the ratio is near 0.5 for a large number of
    # executions.
    assert abs(0.5 - A.COUNTS[0] / A.COUNTS[1]) < 3 * 10.0 ** -3
    assert abs(2.0 - A.COUNTS[2] / A.COUNTS[3]) < 3 * 10.0 ** -3
    assert A.FAIL[0] == 0


def test_construct_c_struct_padding():

    TestStructInterface = ScatterMechInterface(
        "ScatterData *",
        """
        typedef struct ScatterData_ {
            double *kx;
            double *ky;
            double *kz;
        } ScatterData;
        """,
    )

    class A(TestStructInterface):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 1.0
            self.rate[0, 0, 1] = 2.0
            self.c = r"""
            int %(NAME)s(gsl_rng * rng, ScatterData * SD) {

                SD->kx[0] = 1.0;
                SD->ky[0] = 2.1;
                SD->kz[0] = 3.1;

                return 0;
            }
            """

    class B(TestStructInterface):
        def __init__(self):
            self.rate = np.ones((1, 1, 2))
            self.rate[0, 0, 0] = 2.0
            self.rate[0, 0, 1] = 1.0

            self.c = r"""
            int %(NAME)s(gsl_rng * rng, ScatterData * SD) {

                SD->kx[0] = 2.0;
                SD->ky[0] = 5.2;
                SD->kz[0] = 6.2;

                return 0;
            }
            """

    # no padding example
    hf = [A(), B()]
    sfh = ScatterMechHandler(hf)

    N1 = 100
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)
    A.COUNTS = data.GlobalArray(ncomp=4, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))

    gsl_rng_module = PerThreadGSLRNG()

    with A.modify() as m:
        m.add({A.P: pi})

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            double kx = K.i[0];
            double ky = K.i[1];
            double kz = K.i[2];
            int sx = 0;

            
            ScatterData sd = {{&kx, &ky, &kz}};

            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 0, &sx, rng, &sd);

            if (kx == 1.0) {{
                COUNTS[0]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[1]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            apply_scatter_function(SCATTER_RATES_ARRAY, 0, 0, 1, &sx, rng, &sd);

            if (kx == 1.0) {{
                COUNTS[2]++;
                if (!(ky == 2.1)) {{ FAIL[0]++; }}
                if (!(kz == 3.1)) {{ FAIL[0]++; }}
                if (sx != 0) {{ FAIL[0]++; }}
            }} else if (kx == 2.0) {{
                COUNTS[3]++;
                if (!(ky == 5.2)) {{ FAIL[0]++; }}
                if (!(kz == 6.2)) {{ FAIL[0]++; }}
                if (sx != 1) {{ FAIL[0]++; }}
            }} else {{
                FAIL[0]++;
            }}


            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
            ),
        ),
        dat_dict,
    )

    for tx in range(10):
        l.execute()

    # our rate of scatter funcs is 1:2 so we can test the ratio is near 0.5 for a large number of
    # executions.
    assert abs(0.5 - A.COUNTS[0] / A.COUNTS[1]) < 3 * 10.0 ** -3
    assert abs(2.0 - A.COUNTS[2] / A.COUNTS[3]) < 3 * 10.0 ** -3
    assert A.FAIL[0] == 0
