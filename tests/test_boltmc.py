import os
from boltmc import Simulation
from boltmc.recorders import *

# Note that all simulation parameters are as found in defaults.yaml unless otherwise specified.
test_params = {
    "verbose": False,
    "silent": True,
    "track_events": False,
    "save_inputs": False,
}


def test_boltmc_basic():
    """
    Run an extremely basic version of the simulation, with no recording.
    """
    s = Simulation(params=test_params)
    s.run()


def test_boltmc_with_recorders():
    """
    Run BoltMC with all of the recorder objects found in recorders.py.
    """
    record_params = test_params.copy()
    record_params.update({"track_events": True})
    s = Simulation(params=record_params)
    tmpfile = "tmp.json"
    tmpnpzfile = "tmp.npz"
    s.add_extensions(
        [
            RecordMeanK(s, tmpfile),
            RecordMeanVelocity(s, tmpfile),
            RecordMeanEnergy(s, tmpfile),
            RecordSavetimes(s, tmpfile),
            RecordMeanPos(s, tmpfile),
            RecordIniFinState(s, inifile=tmpfile, finfile=tmpfile),
            RecordScatteringRates(s, tmpfile),
            RecordScatteringEvents(s, tmpfile),
            RecordEnergyDistributions(s, tmpnpzfile),
            RecordOccupation(s, tmpfile),
            RecordMeanSquareDisplacement(s, tmpfile),
            SaveSimulation(s, tmpfile),
        ]
    )
    s.run()

    for delfile in [tmpfile, tmpnpzfile]:
        assert os.path.exists(delfile)
        if os.path.exists(delfile):
            os.remove(delfile)


def boltmc_custom_material(matfile):
    """
    Run BoltMC with parameters found in matfile.
    """
    mat_params = test_params.copy()
    mat_params.update(
        {
            "matfilenames": [matfile],
            "track_events": True,
            "max_valleys": 3,
        }
    )
    s = Simulation(params=mat_params)
    s.run()


def test_boltmc_mapbi3_05():
    """
    Run BoltMC with parameters found in matfile mapbi3_05.yaml.
    """
    boltmc_custom_material("mapbi3_05.yaml")


def test_boltmc_gaas_01():
    """
    Run BoltMC with parameters found in matfile gaas_01.yaml. Features multiple valleys.
    """
    boltmc_custom_material("gaas_01.yaml")


if __name__ == "__main__":
    test_boltmc_basic()
    test_boltmc_with_recorders()
    test_boltmc_mapbi3_05()
    test_boltmc_gaas_01()
