from scipy.special import expi
from math import exp
import numpy as np

from ppmd import *
from ppmd.lib import build
from ctypes import c_double

from boltmc.utilities.ei_newton import inverse_Ei, INVERSE_EI_IMPLEMENTATION


def test_inverse_Ei():
    """
    Test the Python implementation of the Newton iteration.
    """

    N = 1000
    tol = 10.0 ** -13
    xmax = 6.0
    xvalues = np.linspace(0.5 * xmax / N, xmax, N)

    for ix in xvalues:

        y = expi(ix)

        to_test = inverse_Ei(y, tol)

        err = abs(to_test - ix)

        assert err < tol


def test_kernel_Ei():
    """
    Test the C kernel call to the GSL implementation of Ei(x)
    """

    N1 = 10
    N = N1 ** 3
    rng = np.random.RandomState(12439237)

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.x = data.ParticleDat(ncomp=1, dtype=c_double)
    A.y = data.ParticleDat(ncomp=1, dtype=c_double)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))
    xi = rng.uniform(low=2 ** -1021, high=6.0, size=(N, 1))

    with A.modify() as m:
        m.add({A.P: pi, A.x: xi})

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_Ei",
            """
            Y.i[0] = gsl_sf_expint_Ei(X.i[0]);
            """,
            headers=(kernel.Header("gsl/gsl_sf.h"),),
        ),
        {
            "X": A.x(access.READ),
            "Y": A.y(access.WRITE),
        },
    )

    l.execute()

    for px in range(N):
        assert abs(A.y[px, 0] - expi(A.x[px, 0])) < 10.0 ** -13


def test_kernel_inverse_Ei():
    """
    Test the C implementation of the Newton interation.
    """

    N1 = 10
    N = N1 ** 3
    rng = np.random.RandomState(12439237)

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.x = data.ParticleDat(ncomp=1, dtype=c_double)
    A.y = data.ParticleDat(ncomp=1, dtype=c_double)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))
    yi = rng.uniform(low=-707.0, high=20.0, size=(N, 1))

    with A.modify() as m:
        m.add({A.P: pi, A.y: yi})

    # This takes the string that defines the C function "inverse_Ei" and returns a kernel.Header object that we can
    # use with a PPMD kernel such that the kernel code can call inverse_Ei.
    inverse_header = build.write_header(INVERSE_EI_IMPLEMENTATION)

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_inverse_Ei",
            """
            double xtmp = 0.0;
            inverse_Ei(Y.i[0], INVERSE_EI_DEFAULT_TOL, &xtmp);
            X.i[0] = xtmp;
            """,
            headers=(inverse_header,),
        ),
        {
            "X": A.x(access.WRITE),
            "Y": A.y(access.READ),
        },
    )

    l.execute()

    for px in range(N):
        assert abs(expi(A.x[px, 0]) - A.y[px, 0]) < 10.0 ** -13
        assert abs(A.x[px, 0] - inverse_Ei(A.y[px, 0])) < 10.0 ** -12
