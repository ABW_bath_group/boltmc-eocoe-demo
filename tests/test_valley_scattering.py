from boltmc.scatter_mech_gen import (
    BaseScatterMech,
    BaseScatterMechThreeInt,
    ScatterMechHandler,
    Padding,
    ScatterMechInterface,
)
import numpy as np
from ppmd import *
from ppmd.lib import build
import ctypes
from test_kernel_gsl_rng import PerThreadGSLRNG


def test_valley_scatter():
    class A(BaseScatterMechThreeInt):
        def __init__(self):
            self.rate = np.ones((1, 1, 3))
            self.rate[0, 0, 1] = 4.0
            self.c = r"""
            int %(NAME)s(gsl_rng * rng, double * kx, double * ky, double * kz, int * mati, int * ptypei, int * vali) {

            // double r = gsl_rng_uniform_pos(rng);
            //     if (r > 0.5) {
                    if (*vali < 2) {
                        *vali += 1;
                    }
            //     }

                *kx = 1.0;
                *ky = 2.1;
                *kz = 3.1;

                return 0;
            }
            """

    hf = [A()]
    # To use the default padding_func we would just initialise the class with ScatterMechHandler(hf)
    sfh = ScatterMechHandler(hf)
    assert abs(sfh.maxrate - 4.0) < 10.0 ** -15

    N1 = 10
    N = N1 ** 3

    A = state.State()
    A.domain = domain.BaseDomainHalo(extent=(1.0, 1.0, 1.0))
    A.domain.boundary_condition = domain.BoundaryTypePeriodic()

    A.P = data.PositionDat()
    A.K = data.ParticleDat(ncomp=3, dtype=ctypes.c_double)
    A.mat = data.ParticleDat(ncomp=1, dtype=ctypes.c_int64)  # Particle materials
    A.ptype = data.ParticleDat(ncomp=1, dtype=ctypes.c_int64)  # Particle types
    A.val = data.ParticleDat(ncomp=1, dtype=ctypes.c_int64)  # Valleys
    A.COUNTS = data.GlobalArray(ncomp=6, dtype=ctypes.c_int64)
    A.FAIL = data.GlobalArray(ncomp=1, dtype=ctypes.c_int64)

    # Here the positions do not matter but they do effect parallel decomposition
    pi = utility.lattice.cubic_lattice((N1, N1, N1), (1.0, 1.0, 1.0))
    mat_init = np.full((N, 1), 0)
    ptype_init = np.full((N, 1), 0)
    val_init = np.full((N, 1), 0)

    gsl_rng_module = PerThreadGSLRNG()

    with A.modify() as m:
        m.add(
            {
                A.P: pi,
                A.mat: mat_init,
                A.ptype: ptype_init,
                A.val: val_init,
            }
        )

    dat_dict = {
        "K": A.K(access.WRITE),
        "COUNTS": A.COUNTS(access.INC),
        "FAIL": A.FAIL(access.INC),
        "mat": A.mat(access.WRITE),
        "ptype": A.ptype(access.WRITE),
        "val": A.val(access.WRITE),
    }
    dat_dict.update(gsl_rng_module.get_dat_dict())
    dat_dict.update(sfh.get_dat_dict())

    l = loop.ParticleLoopOMP(
        kernel.Kernel(
            "test_scatter_funcs",
            """
            {GSL_RNG_INIT}

            double kx = K.i[0];
            double ky = K.i[1];
            double kz = K.i[2];
            int m = mat.i[0];
            int pt = ptype.i[0];
            int v = val.i[0];
            int sx = 0;

            apply_scatter_function(SCATTER_RATES_ARRAY, m, pt, v, &sx, rng, &kx, &ky, &kz, &m, &pt, &v);

            mat.i[0] = m;
            ptype.i[0] = pt;
            val.i[0] = v;

            """.format(
                GSL_RNG_INIT=gsl_rng_module.get_kernel_code()
            ),
            headers=(
                kernel.Header("gsl/gsl_sf.h"),
                kernel.Header("gsl/gsl_rng.h"),
                sfh.get_header_code(),
            ),
        ),
        dat_dict,
    )

    for tx in range(1000):
        l.execute()

    # particles should all be in valley #2
    assert np.any(A.val.view - 0)
    assert np.any(A.val.view - 1)
    assert not np.any(A.val.view - 2)
