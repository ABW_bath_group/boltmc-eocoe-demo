import numpy as np
from scipy.stats import norm
from scipy.stats import normaltest
import matplotlib as mpl
import matplotlib.pyplot as plt
from boltmc.utilities.boltmc_constants import *
from boltmc.simulation import Simulation
from boltmc.thermaliser import Thermaliser


mpl.use("Agg")

thermo_params = {
    "verbose": False,
    "silent": True,
    "instant_thermalisation": True,
    "thermalise_every": 1,
    "initial_temp": 1000,
    "n_particles": int(1e6),
    "timestep": 1e-14,
    "n_timesteps": 1e2,
    "field": 0,
    "matfilenames": ["mapbi3_05.yaml"],
    "save_inputs": False,
}


class EnergyChecker:
    def __init__(self, sim):
        self.sim = sim
        self.thermaliser = Thermaliser(self.sim)

    def execute(self):
        """
        Check whether total energy has been conserved after thermalisation step.
        """
        energy_before = np.sum(self.sim.calculate_kinetic_energies())
        self.thermaliser.execute()
        energy_after = np.sum(self.sim.calculate_kinetic_energies())
        assert (energy_before - energy_after) / energy_before < 1e-10


class DistributionChecker:
    def __init__(self, sim):
        self.sim = sim
        self.thermaliser = Thermaliser(self.sim)

    def execute(self):
        """
        Check if ks are normally distributed after thermalisation step.
        """
        self.thermaliser.execute()
        k = self.sim.State.k.view

        for d in range(3):
            k_d = k[:, d]

            # Check if Gaussian using D’Agostino and Pearson’s test.
            # Null hypothesis is that k_d comes from a Gaussian distriburion. If p > alpha, we fail to reject null hypothesis; k_d is therefore likely to be Gaussian.
            k2, p = normaltest(k_d)
            alpha = 0.001

            if p < alpha:
                # plot the distribution that fails the test
                fig, ax = plt.subplots()
                bins = np.linspace(np.round(np.min(k_d)), np.round(np.max(k_d)), 100)
                ax.hist(k_d, bins, normed=True)

                x = np.linspace(np.min(k_d), np.max(k_d), 100)
                mean, std = norm.fit(k_d)
                y = norm.pdf(x, mean, std)
                ax.plot(x, y)
                fig.savefig("anomalous_ks.png")

            assert p > alpha


def test_thermalise_energy_conservation():
    """
    Checks whether energy is conserved during an instantaneous thermalisation step.
    """
    s = Simulation(params=thermo_params)
    s.add_extensions(
        [
            EnergyChecker(s),
        ]
    )
    s.run()


def test_thermalise_distribution():
    s = Simulation(params=thermo_params)
    s.add_extensions(
        [
            DistributionChecker(s),
        ]
    )
    s.run()


if __name__ == "__main__":
    test_thermalise_distribution()
    test_thermalise_energy_conservation()
