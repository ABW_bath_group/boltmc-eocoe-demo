import os
import glob
from boltmc import material, boltmcdir
from boltmc.headers import header_tools


def test_h_header():
    header_path = os.path.join(boltmcdir, "headers/")
    h_files = glob.glob(os.path.join(header_path, "*.h"))
    header = header_tools.build_header_from_h(h_files)


def test_material_header():
    max_ptypes = 2
    max_valleys = 4

    material_path = os.path.join(boltmcdir, "materials/")
    matfiles = ["example_mat.yaml", "example_mat_2.yaml"]
    mats = []

    for m in range(len(matfiles)):
        filename = os.path.join(material_path, matfiles[m])
        mats.append(
            material.Material(max_ptypes, max_valleys, filename=filename, verbose=False)
        )
        mats[-1].impdens = 1e24

    mat_header = header_tools.build_material_header(mats, max_ptypes, max_valleys)


def test_emc_header():
    int_sim_constants = {
        "NT": 1000,
        "NE": 1e6,
    }
    float_sim_constants = {
        "TEMP": 200,
        "FIELD": 1e5,
        "DT": 1e-15,
        "DE": 2e-4,
        "GAMMA": 1.8645e15,
    }
    bool_sim_constants = {}
    header = header_tools.build_emc_param_header(
        floats=float_sim_constants, ints=int_sim_constants, bools=bool_sim_constants
    )


if __name__ == "__main__":
    test_h_header()
    test_material_header()
    test_emc_header()
