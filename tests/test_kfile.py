import os
from boltmc import Simulation
from boltmc.recorders import *

# Note that all simulation parameters are as found in defaults.yaml unless otherwise specified.
test_params = {
    "verbose": False,
    "silent": True,
    "track_events": False,
    "save_inputs": False,
}


def test_kfile():
    """
    Run BoltMC, save the final state to a file, then try to load the file again.
    """
    s = Simulation(params=test_params)
    tmpfile = "tmp.json"
    s.add_extensions(
        [
            RecordIniFinState(s, finfile=tmpfile),
        ]
    )
    s.run()

    params2 = test_params.copy()
    params2.update(
        {
            "initial_distribution": "from_file",
            "kfile": tmpfile,
        }
    )
    s2 = Simulation(params=params2)

    assert (s.State.k.view == s2.State.k.view).all()

    assert os.path.exists(tmpfile)
    if os.path.exists(tmpfile):
        os.remove(tmpfile)


if __name__ == "__main__":
    test_kfile()
